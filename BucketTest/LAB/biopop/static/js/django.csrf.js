// See https://docs.djangoproject.com/en/dev/ref/csrf/ for more information concerning CSRF in Django.

$(document).ready(function () {
    var csrfToken = $.cookie('csrftoken');

    function csrfSafeMethod(method) {
        // These HTTP methods do not require CSRF protection.
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrfToken);
            }
        }
    });
})