from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import url

from . import views

app_name = 'publication'
urlpatterns = [
	url(r'^', views.index, name='index'),
	url(r'^(?P<year>(d+))$', views.year, name='year'),
] 

###+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
