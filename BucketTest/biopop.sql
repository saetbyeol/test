drop table publication;
create table publication(
	p_no bigint(20) not null auto_increment primary key,
	authors varchar(200) not null,
	journal varchar(200) not null,
	year varchar(5) not null,
	title varchar(100) not null,
	link varchar(200),
	page varchar(50)
);