-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: PartBank
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Attachment`
--

DROP TABLE IF EXISTS `Attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Attachment` (
  `attachmentID` bigint(20) NOT NULL COMMENT '첨부 파일 ID',
  `name` varchar(100) NOT NULL COMMENT '첨부파일 이름',
  `extension` varchar(16) DEFAULT NULL COMMENT '첨부파일 확장자 이름',
  `contentType` varchar(20) DEFAULT NULL COMMENT '첨부파일 유형 (image, file, text, xml...)',
  `fileSize` bigint(100) NOT NULL COMMENT '첨부파일 크기',
  `path` varchar(100) DEFAULT NULL COMMENT '파일경로',
  `userID` bigint(100) NOT NULL COMMENT '첨부파일 등록 사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '생성일자',
  PRIMARY KEY (`attachmentID`),
  KEY `ERA_REFS_USERID` (`userID`),
  CONSTRAINT `ERA_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 결과 이미지 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Attachment`
--

LOCK TABLES `Attachment` WRITE;
/*!40000 ALTER TABLE `Attachment` DISABLE KEYS */;
INSERT INTO `Attachment` VALUES (50,'GESSisp_phe_200.csv','csv','csv',6228890,'/usr/local/tomcat6/webapps/partbank_v2/./attachments/44/GESSisp_phe_200.csv',22,'001406525389853'),(52,'GESSisp_phe_200.csv','csv','csv',6228890,'/usr/local/tomcat6/webapps/partbank_v2/./attachments/52/GESSisp_phe_200.csv',22,'001406525842865'),(72,'GESSisp_phe_200.csv','csv','csv',6228890,'/usr/local/tomcat6/webapps/partbank_v2/./attachments/72/GESSisp_phe_200.csv',1,'001406687676550'),(74,'GESSisp_phe_200.csv','csv','csv',6228890,'/usr/local/tomcat6/webapps/partbank_v2/./attachments/74/GESSisp_phe_200.csv',22,'001406702072710'),(242,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\242/victor-exp1.xls',31,'001438133484094'),(292,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\292/victor-exp1.xls',31,'001439356331828'),(302,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\302/victor-exp1.xls',31,'001439415556093'),(304,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\304/victor-exp1.xls',31,'001439415634411'),(306,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\306/victor-exp1.xls',31,'001439416361765'),(312,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\312/victor-exp1.xls',31,'001439416991422'),(322,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\322/victor-exp1.xls',31,'001441783848254'),(332,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\332/victor-exp1.xls',31,'001441847819748'),(342,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\342/victor-exp1.xls',31,'001441850543846'),(352,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\352/victor-exp1.xls',31,'001442306603030'),(361,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\292/victor-exp1.xls',31,'001442309050473'),(371,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\352/victor-exp1.xls',31,'001442309654487'),(372,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\292/victor-exp1.xls',31,'001442309817689'),(382,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\382/victor-exp1.xls',31,'001442551816165'),(392,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\392/victor-exp1.xls',31,'001442552288217'),(411,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\392/victor-exp1.xls',31,'001442801295613'),(413,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\413/victor-exp1.xls',31,'001442801959497'),(414,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\413/victor-exp1.xls',31,'001442801991780'),(415,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\413/victor-exp1.xls',31,'001442802028256'),(421,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\413/victor-exp1.xls',31,'001442806498841'),(431,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\413/victor-exp1.xls',31,'001442808346318'),(432,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\413/victor-exp1.xls',31,'001442809211841'),(441,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\413/victor-exp1.xls',31,'001442809518000'),(452,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\452/victor-exp1.xls',31,'001444023781364'),(462,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\462/victor-exp1.xls',31,'001444025199207'),(472,'victor-exp1.xls','xls','xls',60928,'F:\\folderNoTest\\partbank\\war\\attachments\\472/victor-exp1.xls',31,'001444098988459'),(592,'victor-exp1.xls','xls','xls',70656,'F:\\NewText\\partbank\\war\\attachments\\592/victor-exp1.xls',31,'001445387481580'),(601,'victor-exp1.xls','xls','xls',70656,'F:\\NewText\\partbank\\war\\attachments\\592/victor-exp1.xls',31,'001445488662104'),(622,'victor-exp1.xls','xls','xls',70656,'F:\\NewText\\partbank\\war\\attachments\\622/victor-exp1.xls',31,'001445574455249'),(632,'victor-exp1.xls','xls','xls',70656,'F:\\NewText\\partbank\\war\\attachments\\632/victor-exp1.xls',31,'001445838575706'),(682,'tecan-exp1.xlsx','xlsx','xlsx',267479,'F:\\NewText\\partbank\\war\\attachments\\682/tecan-exp1.xlsx',31,'001445936459994'),(692,'tecan-exp1.xlsx','xlsx','xlsx',267479,'F:\\NewText\\partbank\\war\\attachments\\692/tecan-exp1.xlsx',31,'001445936909877');
/*!40000 ALTER TABLE `Attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CitaterArticle`
--

DROP TABLE IF EXISTS `CitaterArticle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CitaterArticle` (
  `articleID` bigint(20) NOT NULL COMMENT '참고논문 ID',
  `alias` varchar(128) NOT NULL COMMENT '참고논문 약어이름',
  `title` varchar(128) NOT NULL COMMENT '참고논문 제목',
  `authors` varchar(1000) DEFAULT NULL COMMENT '저자명',
  `year` int(11) DEFAULT NULL COMMENT '저서 등록 년도',
  `comment` text COMMENT '저서에 대한 논평',
  `journal` varchar(128) DEFAULT NULL COMMENT '저널 이름',
  `publishDate` varchar(50) DEFAULT NULL COMMENT '출판일자',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  PRIMARY KEY (`articleID`),
  KEY `Article_Alias_IDX` (`alias`),
  KEY `Article_Title_IDX` (`title`),
  KEY `Article_Journal_IDX` (`journal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CitaterArticle`
--

LOCK TABLES `CitaterArticle` WRITE;
/*!40000 ALTER TABLE `CitaterArticle` DISABLE KEYS */;
INSERT INTO `CitaterArticle` VALUES (1,'reference test','reference titlte','kim, park, choi',2013,'reference comments..','unknown','2013/01/01','001377489379226','001377489379226'),(11,'aaa','aaa','aaa',0,'aaaa','aa',NULL,'001378109843161','001378109843161');
/*!40000 ALTER TABLE `CitaterArticle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Device`
--

DROP TABLE IF EXISTS `Device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Device` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `serial` varchar(16) NOT NULL COMMENT '고유번호',
  `name` varchar(64) NOT NULL COMMENT '이름이 없는 경우 고유번호 입력',
  `deviceType` bigint(20) DEFAULT NULL,
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`deviceID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `DEVICE_REFS_TYPE` (`deviceType`),
  KEY `DEVICE_REFS_USERID` (`userID`),
  KEY `Device_Serial_IDX` (`serial`),
  KEY `Device_Name_IDX` (`name`),
  CONSTRAINT `DEVICE_REFS_TYPE` FOREIGN KEY (`deviceType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `DEVICE_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비 관리테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Device`
--

LOCK TABLES `Device` WRITE;
/*!40000 ALTER TABLE `Device` DISABLE KEYS */;
INSERT INTO `Device` VALUES (21,'DEV021','GESSv4',32,'GESSv4',22,'001381712686078','001407595652163','YES'),(41,'DEV041','test1090',17,NULL,31,'001433812423365','001433812423365','NO'),(52,'DEV052','testest',17,NULL,31,'001440564353046','001440564353046','YES'),(61,'DEV061','TEST0901',17,'sfsdfsdfsdfsdf',31,'001441070580799','001441070580799','YES'),(62,'DEV062','TESTIO',17,NULL,31,'001441071280012','001441071280012','YES'),(81,'DEV081','test123456',17,NULL,31,'001441598256328','001441598256328','YES'),(91,'DEV091','test10101362',NULL,'sdasdasd',31,'001444113330599','001444113330599','YES'),(101,'DEV101','DeviceTest',NULL,NULL,31,'001444122127766','001444122127766','YES'),(111,'DEV111','tsdfwerType',NULL,NULL,31,'001444195203463','001444195203463','YES'),(121,'DEV121','Device2',NULL,NULL,31,'001446083673757','001446083673757','YES'),(122,'DEV122','Device2323',NULL,NULL,31,'001446083917246','001446083917246','YES'),(123,'DEV123','Device2323',NULL,NULL,31,'001446083917247','001446083917247','YES');
/*!40000 ALTER TABLE `Device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DeviceComponent`
--

DROP TABLE IF EXISTS `DeviceComponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceComponent` (
  `componentID` bigint(20) NOT NULL COMMENT '장비 내 부품 ID',
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `sourceID` bigint(20) NOT NULL COMMENT '부품의 원본 객체 ID (Part, IOUnit, Module, ModulePart, ModuleIOUnit, Logic)',
  `sourceType` varchar(20) NOT NULL DEFAULT 'DeviceModule' COMMENT '부품의 원본 객체의 종류',
  `positionX` double NOT NULL DEFAULT '0' COMMENT '부품의 x 좌표',
  `positionY` double NOT NULL DEFAULT '0' COMMENT '부품의 y 좌표',
  `width` double NOT NULL DEFAULT '30' COMMENT '부품의 넓이',
  `height` double NOT NULL DEFAULT '30' COMMENT '부품의 높이',
  `inputDirections` varchar(45) DEFAULT NULL COMMENT '입력 Anchor 방향, comma 로 구분(UP,DOWN,LEFT,RIGHT)',
  `outputDirections` varchar(45) DEFAULT NULL COMMENT '출력 Anchor 방향, comma 로 구분(UP,DOWN,LEFT,RIGHT)',
  `isInputOverlap` tinyint(1) DEFAULT '0' COMMENT '입력되는 연결선의 중복여부 (기본 중복 불허)',
  `isOutputOverlap` tinyint(1) DEFAULT '1' COMMENT '입력되는 연결선의 중복여부 (기본 중복 불허)',
  `rotationDegrees` double DEFAULT '0' COMMENT '회전 각도 (0 ~ 360)',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '부품의 등록 순서',
  PRIMARY KEY (`componentID`),
  KEY `DCOMP_REFS_DEVICEID` (`deviceID`),
  CONSTRAINT `DCOMP_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비의 부품관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DeviceComponent`
--

LOCK TABLES `DeviceComponent` WRITE;
/*!40000 ALTER TABLE `DeviceComponent` DISABLE KEYS */;
INSERT INTO `DeviceComponent` VALUES (111,21,21,'DeviceModule',148.571428571429,94.2857142857143,125.714285714286,35.7142857142857,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(112,21,15,'ModuleInput',58.5714285714286,142.857142857143,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(113,21,12,'ModuleOutput',301.428571428571,58.5714285714286,30,30,'UP,RIGHT,LEFT','UP,RIGHT,LEFT,DOWN',0,1,0,-1),(151,41,22,'DeviceIOUnit',204,75,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(152,41,41,'DeviceIOUnit',318,66,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(153,41,71,'DeviceModule',230,65,94,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(154,41,61,'ModuleInput',180,15,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(155,41,62,'ModuleInput',180,15,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(162,52,82,'DeviceModule',265,141,64,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(171,61,82,'DeviceModule',189,128,64,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(172,62,91,'DeviceModule',190,116,64,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(173,62,22,'DeviceIOUnit',270,118,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(191,81,71,'DeviceModule',114,115,94,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(192,81,61,'ModuleInput',64,65,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(193,81,62,'ModuleInput',64,65,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(194,81,24,'DeviceIOUnit',235,89,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(201,91,131,'DeviceModule',78,102,64,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(211,101,91,'DeviceModule',125,121,64,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(221,111,71,'DeviceModule',129,112,94,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(222,111,61,'ModuleInput',79,62,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(223,111,62,'ModuleInput',79,62,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(231,121,21,'DeviceModule',211,160,124,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(232,121,15,'ModuleInput',161,110,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(233,121,12,'ModuleOutput',385,110,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(234,123,21,'DeviceModule',205,168,124,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(235,122,21,'DeviceModule',205,168,124,34,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(236,123,15,'ModuleInput',155,118,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(237,122,15,'ModuleInput',155,118,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(238,123,12,'ModuleOutput',379,118,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1),(239,122,12,'ModuleOutput',379,118,30,30,'UP,RIGHT,DOWN,LEFT','UP,RIGHT,DOWN,LEFT',0,1,0,-1);
/*!40000 ALTER TABLE `DeviceComponent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DeviceDesigner`
--

DROP TABLE IF EXISTS `DeviceDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceDesigner` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `userID` bigint(20) NOT NULL COMMENT '장비개발자 ID',
  UNIQUE KEY `deviceID` (`deviceID`,`userID`),
  KEY `DEVICEDESIGNER_REFS_USERID` (`userID`),
  CONSTRAINT `DEVICEDESIGNER_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`),
  CONSTRAINT `DEVICEDESIGNER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DeviceDesigner`
--

LOCK TABLES `DeviceDesigner` WRITE;
/*!40000 ALTER TABLE `DeviceDesigner` DISABLE KEYS */;
/*!40000 ALTER TABLE `DeviceDesigner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DeviceLink`
--

DROP TABLE IF EXISTS `DeviceLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceLink` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `sourceID` bigint(20) NOT NULL COMMENT '시작하는 장비의 부품 ID',
  `targetID` bigint(20) NOT NULL COMMENT '종료하는 장비의 부품 ID',
  `borderWidth` int(11) NOT NULL DEFAULT '1' COMMENT '연결선의 두께',
  `borderColor` varchar(15) NOT NULL DEFAULT '000000' COMMENT '연결선의 색상',
  `isStartArrow` tinyint(1) DEFAULT '0' COMMENT '시작부분 화살표 유무',
  `isEndArrow` tinyint(1) DEFAULT '0' COMMENT '끝부분 화살표 유무',
  `inputIndex` int(11) NOT NULL DEFAULT '0' COMMENT 'Component의 출력에 연결된 Link의 순서, ex) IOUnit에서 2개의 선이 나와 두 개의 프로모터로 각각 연결되면 각 연결선의 outputIndex 는 0과 1이 됨',
  `outputIndex` int(11) NOT NULL DEFAULT '0' COMMENT 'Component의 입력에 연결된 Link의 순서, ex) 두 개의 I/O unit에서 선이 하나씩 나와 하나의 로직으로 연결되는 경우 각 연결선의 inputIndex는 0과 1이 됨',
  UNIQUE KEY `deviceID` (`deviceID`,`sourceID`,`targetID`),
  KEY `DLINK_REF_SOURCEID` (`sourceID`),
  KEY `DLINK_REF_TARGETID` (`targetID`),
  CONSTRAINT `DLINK_REF_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`),
  CONSTRAINT `DLINK_REF_SOURCEID` FOREIGN KEY (`sourceID`) REFERENCES `DeviceComponent` (`componentID`),
  CONSTRAINT `DLINK_REF_TARGETID` FOREIGN KEY (`targetID`) REFERENCES `DeviceComponent` (`componentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비의 부품 연결선 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DeviceLink`
--

LOCK TABLES `DeviceLink` WRITE;
/*!40000 ALTER TABLE `DeviceLink` DISABLE KEYS */;
INSERT INTO `DeviceLink` VALUES (21,111,113,1,'#000000',0,0,0,0),(21,112,111,1,'#000000',0,0,0,0),(41,154,153,1,'#000000',0,0,0,0),(41,155,153,1,'#000000',0,0,0,0),(81,192,191,1,'#000000',0,0,0,0),(81,193,191,1,'#000000',0,0,0,0),(111,222,221,1,'#000000',0,0,0,0),(111,223,221,1,'#000000',0,0,0,0),(121,231,233,1,'#000000',0,0,0,0),(121,232,231,1,'#000000',0,0,0,0),(122,235,239,1,'#000000',0,0,0,0),(122,237,235,1,'#000000',0,0,0,0),(123,234,238,1,'#000000',0,0,0,0),(123,236,234,1,'#000000',0,0,0,0);
/*!40000 ALTER TABLE `DeviceLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DeviceReferences`
--

DROP TABLE IF EXISTS `DeviceReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceReferences` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `deviceID` (`deviceID`,`articleID`),
  KEY `DEVICEREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `DEVICEREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `DEVICEREFERENCES_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DeviceReferences`
--

LOCK TABLES `DeviceReferences` WRITE;
/*!40000 ALTER TABLE `DeviceReferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `DeviceReferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Dictionary`
--

DROP TABLE IF EXISTS `Dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dictionary` (
  `dictionaryID` bigint(20) NOT NULL COMMENT '항목 ID',
  `name` varchar(128) NOT NULL COMMENT '항목이름',
  `parentID` bigint(20) DEFAULT NULL COMMENT '상위 항목 ID',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `description` text COMMENT '항목 설명',
  `nodeType` enum('Folder','Text') NOT NULL DEFAULT 'Folder' COMMENT '폴더인지 Leaf 노드인지 설정',
  `sourceType` varchar(16) NOT NULL DEFAULT 'Unknown' COMMENT 'Dictionary 종류',
  `removable` char(1) DEFAULT '1' COMMENT '삭제 가능성(0: 삭제 불가능, 1:삭제 가능)',
  `hasIcon` char(1) DEFAULT '0' COMMENT '아이콘 이미지를 갖고 있는지 여부(0: 이미지 존재 안 함, 1: 이미지 존재)',
  `creationDate` varchar(15) NOT NULL COMMENT '생성일자',
  PRIMARY KEY (`dictionaryID`),
  KEY `DICT_REFS_PARENTID` (`parentID`),
  KEY `DICT_REFS_USERID` (`userID`),
  KEY `Dictionary_Name_IDX` (`name`),
  CONSTRAINT `DICT_REFS_PARENTID` FOREIGN KEY (`parentID`) REFERENCES `Dictionary` (`dictionaryID`) ON DELETE CASCADE,
  CONSTRAINT `DICT_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시스템 사용항목 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Dictionary`
--

LOCK TABLES `Dictionary` WRITE;
/*!40000 ALTER TABLE `Dictionary` DISABLE KEYS */;
INSERT INTO `Dictionary` VALUES (1,'Part type',NULL,1,'Category of part type','Folder','Part','0','0','001377489221710'),(2,'IOUnit type',NULL,1,'category of IO unit type','Folder','IOUnit','0','0','001377489221724'),(3,'Module type',NULL,1,'Category of module type','Folder','Module','0','0','001377489221725'),(4,'Device type',NULL,1,'Category of device type','Folder','Device','0','0','001377489221726'),(5,'Logic',NULL,1,'Logic gate','Folder','Logic','0','0','001377489221726'),(6,'Promoter',1,1,'promoter','Text','Part','0','1','001377489221732'),(7,'Operator',1,1,'operator','Text','Part','0','1','001377489221740'),(8,'Gene',1,1,'Gene','Text','Part','0','1','001377489221750'),(9,'Five prime overhang',1,1,'five prime overhang','Text','Part','0','1','001377489221757'),(10,'Three prime overhang',1,1,'Three prime overhang','Text','Part','0','1','001377489221767'),(11,'Terminator',1,1,'terminator','Text','Part','0','1','001377489221787'),(12,'mRNA',2,1,'mRAN','Text','IOUnit','0','1','001377489221797'),(13,'protein oligomer',2,1,'protein dimer','Text','IOUnit','0','1','001377489221804'),(14,'chemicals',2,1,'chemicals','Text','IOUnit','0','1','001377489221811'),(15,'Expression Unit',3,1,'Expression Unit','Text','Module','0','0','001377489221819'),(16,'Reporter module',3,1,'Reporter','Text','Module','0','0','001377489221826'),(17,'Expression Unit',4,1,'Expression Unit','Text','Device','0','0','001377489221836'),(18,'Reporter',4,1,'Reporter','Text','Device','0','0','001377489221842'),(19,'AND',5,1,'AND gate','Text','Logic','0','1','001377489221848'),(20,'OR',5,1,'OR gate','Text','Logic','0','1','001377489221854'),(21,'CON',5,1,'CON gate','Text','Logic','0','1','001377489221861'),(32,'GESS',4,1,NULL,'Text','Dictionary','1','0','001377556208283'),(42,'RBS',1,22,'RBS','Text','Dictionary','1','1','001378106803469'),(61,'Gene_sensor',1,22,'Genes that sense molecules','Text','Dictionary','1','1','001381495547214'),(62,'Gene_reporter',1,22,'Genes that show any phenotypic value','Text','Part','1','1','001381495698583'),(63,'Gene_antibiotics',1,22,'antibiotic genes','Text','Dictionary','1','1','001381495814289'),(64,'Ori',1,22,NULL,'Text','Part','1','1','001381496304903'),(65,'Sensor module',3,22,NULL,'Text','Dictionary','1','0','001381496897713'),(81,'Vector',NULL,1,'category vector','Folder','Vector','1','0','001384443403672'),(82,'Culture condition',NULL,1,NULL,'Folder','CultureCondition','1','0','001384443426056'),(83,'strain',NULL,1,NULL,'Folder','Strain','1','0','001384443441766'),(84,'vector 1',81,1,'vector 1','Text','Vector','1','0','001384443453290'),(85,'vector 2',81,1,'vector 2','Text','Vector','1','0','001384443462914'),(86,'condition 1',82,1,'condition 1','Text','CultureCondition','1','0','001384443479658'),(87,'condition 2',82,1,NULL,'Text','CultureCondition','1','0','001384443490612'),(88,'strain 1',83,1,'strain 1','Text','Strain','1','0','001384443499103'),(89,'strain 2',83,1,'strain 2','Text','Strain','1','0','001384443506897'),(112,'Vector',1,22,'Vector','Text','Part','1','1','001403077732950'),(121,'output name',NULL,22,'output name','Folder','OutputName','1','0','001406525663949'),(122,'GREEN',121,22,'green','Text','OutputName','1','0','001406525684768'),(131,'Media',NULL,1,NULL,'Folder','Media','1','0','001406687747040'),(132,'media-01',131,1,NULL,'Text','Media','1','0','001406687758762'),(133,'media-02',131,1,NULL,'Text','Media','1','0','001406687767022');
/*!40000 ALTER TABLE `Dictionary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DictionaryProp`
--

DROP TABLE IF EXISTS `DictionaryProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DictionaryProp` (
  `dictionaryID` bigint(20) NOT NULL COMMENT '사용 항목 ID',
  `propName` varchar(128) NOT NULL COMMENT '사용항목 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '사용항목 속성 값',
  UNIQUE KEY `dictionaryID` (`dictionaryID`,`propName`),
  KEY `DictionaryProp_Name_IDX` (`propName`),
  KEY `DictionaryProp_Value_IDX` (`propValue`),
  CONSTRAINT `DICTPROP_REFS_DICTID` FOREIGN KEY (`dictionaryID`) REFERENCES `Dictionary` (`dictionaryID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시스템 사용항목 기타속성 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DictionaryProp`
--

LOCK TABLES `DictionaryProp` WRITE;
/*!40000 ALTER TABLE `DictionaryProp` DISABLE KEYS */;
INSERT INTO `DictionaryProp` VALUES (8,'image.small.uri','icons/16x16/cds.png'),(61,'image.small.uri','icons/16x16/cds_5.png'),(62,'image.small.uri','icons/16x16/cds_6.png'),(9,'image.small.uri','icons/16x16/five-prime-overhang.png'),(19,'image.small.uri','icons/16x16/gate-and.png'),(21,'image.small.uri','icons/16x16/gate-con.png'),(20,'image.small.uri','icons/16x16/gate-or.png'),(7,'image.small.uri','icons/16x16/operator.png'),(64,'image.small.uri','icons/16x16/origin-of-replication_1.png'),(6,'image.small.uri','icons/16x16/promoter.png'),(63,'image.small.uri','icons/16x16/rna-stability-element_1.png'),(11,'image.small.uri','icons/16x16/terminator.png'),(10,'image.small.uri','icons/16x16/three-prime-overhang.png'),(42,'image.small.uri','icons/16x16/translational-start-site_6.png'),(14,'image.small.uri','icons/16x16/unit-circle.png'),(13,'image.small.uri','icons/16x16/unit-diamond.png'),(12,'image.small.uri','icons/16x16/unit-rect.png'),(112,'image.small.uri','icons/16x16/vectorimg.PNG'),(8,'image.middle.uri','icons/30x30/cds.png'),(61,'image.middle.uri','icons/30x30/cds_5.png'),(62,'image.middle.uri','icons/30x30/cds_6.png'),(9,'image.middle.uri','icons/30x30/five-prime-overhang.png'),(19,'image.middle.uri','icons/30x30/gate-and.png'),(21,'image.middle.uri','icons/30x30/gate-con.png'),(20,'image.middle.uri','icons/30x30/gate-or.png'),(7,'image.middle.uri','icons/30x30/operator.png'),(64,'image.middle.uri','icons/30x30/origin-of-replication_1.png'),(6,'image.middle.uri','icons/30x30/promoter.png'),(63,'image.middle.uri','icons/30x30/rna-stability-element_1.png'),(11,'image.middle.uri','icons/30x30/terminator.png'),(10,'image.middle.uri','icons/30x30/three-prime-overhang.png'),(42,'image.middle.uri','icons/30x30/translational-start-site_6.png'),(14,'image.middle.uri','icons/30x30/unit-circle.png'),(13,'image.middle.uri','icons/30x30/unit-diamond.png'),(12,'image.middle.uri','icons/30x30/unit-rect.png'),(112,'image.middle.uri','icons/30x30/vectorimg.PNG'),(8,'image.large.uri','icons/48x48/cds.png'),(61,'image.large.uri','icons/48x48/cds_5.png'),(62,'image.large.uri','icons/48x48/cds_6.png'),(9,'image.large.uri','icons/48x48/five-prime-overhang.png'),(19,'image.large.uri','icons/48x48/gate-and.png'),(21,'image.large.uri','icons/48x48/gate-con.png'),(20,'image.large.uri','icons/48x48/gate-or.png'),(7,'image.large.uri','icons/48x48/operator.png'),(64,'image.large.uri','icons/48x48/origin-of-replication_1.png'),(6,'image.large.uri','icons/48x48/promoter.png'),(63,'image.large.uri','icons/48x48/rna-stability-element_1.png'),(11,'image.large.uri','icons/48x48/terminator.png'),(10,'image.large.uri','icons/48x48/three-prime-overhang.png'),(42,'image.large.uri','icons/48x48/translational-start-site_6.png'),(14,'image.large.uri','icons/48x48/unit-circle.png'),(13,'image.large.uri','icons/48x48/unit-diamond.png'),(12,'image.large.uri','icons/48x48/unit-rect.png'),(112,'image.large.uri','icons/48x48/vectorimg.PNG');
/*!40000 ALTER TABLE `DictionaryProp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EventHistory`
--

DROP TABLE IF EXISTS `EventHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventHistory` (
  `eventID` bigint(20) NOT NULL COMMENT '이벤트 ID',
  `eventType` varchar(16) NOT NULL COMMENT '이벤트 유형(Create, Change, Delete)',
  `message` varchar(256) NOT NULL COMMENT '메시지',
  `sourceID` bigint(20) NOT NULL COMMENT '이벤트 대상 ID',
  `sourceName` varchar(256) NOT NULL COMMENT '이벤트 대상 이름',
  `sourceType` varchar(32) NOT NULL COMMENT 'User, Dictionary, IOUnit, Part, Module, Device, Strain, Protocol, Experiment, Simulation',
  `userID` bigint(20) NOT NULL COMMENT '이벤트 실행 사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '이벤트 발생 시간',
  PRIMARY KEY (`eventID`),
  KEY `EVTHISTORY_REFS_USERID` (`userID`),
  KEY `Evtistory_SourceType_IDX` (`sourceType`),
  KEY `EvtHistory_CDate_IDX` (`creationDate`),
  CONSTRAINT `EVTHISTORY_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='이벤트 생성, 수정, 삭제 기록 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EventHistory`
--

LOCK TABLES `EventHistory` WRITE;
/*!40000 ALTER TABLE `EventHistory` DISABLE KEYS */;
INSERT INTO `EventHistory` VALUES (11,'Change','Change device \'DEV001:device-01\' information.',1,'DEV001:device-01','Device',1,'001377891478669'),(12,'Create','Append system type information \'test\'.',41,'test','Dictionary',22,'001378079541700'),(13,'Delete','Delete system type information \'test\'.',41,'test','Dictionary',22,'001378079552025'),(14,'Delete','Delete system type information \'RBS\'.',35,'RBS','Dictionary',22,'001378079571852'),(15,'Create','Append system type information \'RBS\'.',42,'RBS','Dictionary',22,'001378106803470'),(16,'Create','Create part \'RBS:PART031:myRBS\'.',31,'RBS:PART031:myRBS','Part',22,'001378106864754'),(17,'Change','Change module \'MOD013:repoter\' information.',13,'MOD013:repoter','Module',22,'001378106883320'),(18,'Change','Change system type information \'RBS\'.',42,'RBS','Dictionary',22,'001378107004219'),(19,'Change','Change system type information \'RBS\'.',42,'RBS','Dictionary',22,'001378107018553'),(20,'Change','Change system type information \'RBS\'.',42,'RBS','Dictionary',22,'001378107062617'),(21,'Change','Change system type information \'RBS\'.',42,'RBS','Dictionary',22,'001378107093841'),(22,'Change','Change system type information \'RBS\'.',42,'RBS','Dictionary',22,'001378107137201'),(23,'Change','Change device \'DEV013:gess v1\' information.',13,'DEV013:gess v1','Device',22,'001378107160569'),(24,'Create','Create part \'Terminator:PART032:tL3 terminator\'.',32,'Terminator:PART032:tL3 terminator','Part',22,'001378107892247'),(25,'Create','Create part \'Gene:PART033:dmpR_Rha\'.',33,'Gene:PART033:dmpR_Rha','Part',22,'001378108154713'),(26,'Change','Change part \'Gene:PART033:dmpR_Rha\' information.',33,'Gene:PART033:dmpR_Rha','Part',22,'001378108177370'),(27,'Create','Create part \'Promoter:PART034:dmpR original promoter\'.',34,'Promoter:PART034:dmpR original promoter','Part',22,'001378108614622'),(28,'Create','Create part \'Promoter:PART035:dmpR_Rha Po promoter\'.',35,'Promoter:PART035:dmpR_Rha Po promoter','Part',22,'001378108841622'),(29,'Create','Create part \'RBS:PART036:T7 RBS\'.',36,'RBS:PART036:T7 RBS','Part',22,'001378108861623'),(30,'Create','Create part \'Gene:PART037:EGFP\'.',37,'Gene:PART037:EGFP','Part',22,'001378108884071'),(31,'Create','Create part \'Terminator:PART038:rrnBT terminator\'.',38,'Terminator:PART038:rrnBT terminator','Part',22,'001378108902989'),(32,'Change','Change part \'Gene:PART033:dmpR_Rha\' information.',33,'Gene:PART033:dmpR_Rha','Part',22,'001378109767285'),(41,'Create','Append system type information \'test\'.',51,'test','Dictionary',22,'001378184789688'),(42,'Delete','Delete system type information \'test\'.',51,'test','Dictionary',22,'001378184798650'),(43,'Change','Change part \'Promoter:PART035:dmpR_Rha Po promoter\' information.',35,'Promoter:PART035:dmpR_Rha Po promoter','Part',22,'001378185473781'),(51,'Delete','Change experiment protocol \'PRTO003:test-protocol\' information.',3,'PRTO003:test-protocol','Protocol',22,'001379999975192'),(52,'Delete','Change experiment protocol \'PRTO011:GESS v1 protocol\' information.',11,'PRTO011:GESS v1 protocol','Protocol',22,'001380000101743'),(53,'Delete','Change experiment protocol \'PRTO003:test-protocol\' information.',3,'PRTO003:test-protocol','Protocol',22,'001380000148105'),(54,'Change','Change module \'MOD012:senser\' information.',12,'MOD012:senser','Module',22,'001380000304261'),(55,'Change','Change module \'MOD011:module-02\' information.',11,'MOD011:module-02','Module',22,'001380000370205'),(56,'Change','Change device \'DEV013:gess v1\' information.',13,'DEV013:gess v1','Device',22,'001380001277271'),(57,'Change','Change device \'DEV013:gess v1\' information.',13,'DEV013:gess v1','Device',22,'001380001297282'),(58,'Change','Change device \'DEV013:gess v1\' information.',13,'DEV013:gess v1','Device',22,'001380001357029'),(59,'Change','Change part \'Promoter:PART035:dmpR_Rha Po promoter\' information.',35,'Promoter:PART035:dmpR_Rha Po promoter','Part',1,'001380699675776'),(61,'Change','Change part \'Promoter:PART019:Po\' information.',19,'Promoter:PART019:Po','Part',1,'001381430311049'),(62,'Change','Change part \'Promoter:PART034:dmpR original promoter\' information.',34,'Promoter:PART034:dmpR original promoter','Part',1,'001381430317716'),(63,'Change','Change part \'Gene:PART033:dmpR_Rha\' information.',33,'Gene:PART033:dmpR_Rha','Part',22,'001381479779818'),(64,'Delete','Delete part \'Gene:PART021:RGFP\'.',21,'Gene:PART021:RGFP','Part',22,'001381479795879'),(65,'Delete','Delete part \'Gene:PART016:dmpR\'.',16,'Gene:PART016:dmpR','Part',22,'001381479799513'),(66,'Delete','Delete part \'Gene:PART002:CDS-01\'.',2,'Gene:PART002:CDS-01','Part',22,'001381479802899'),(67,'Delete','Delete part \'Gene:PART012:CDS-02\'.',12,'Gene:PART012:CDS-02','Part',22,'001381479805659'),(68,'Delete','Delete part \'Gene:PART013:CDS-03\'.',13,'Gene:PART013:CDS-03','Part',22,'001381479808251'),(69,'Change','Change part \'Gene:PART033:dmpR\' information.',33,'Gene:PART033:dmpR','Part',22,'001381479959662'),(70,'Change','Change part \'Gene:PART033:dmpR(WT)\' information.',33,'Gene:PART033:dmpR(WT)','Part',22,'001381479990270'),(71,'Create','Create part \'Gene:PART041:dmpR(E135K)\'.',41,'Gene:PART041:dmpR(E135K)','Part',22,'001381480020843'),(72,'Change','Change part \'Gene:PART041:dmpR(E135K)\' information.',41,'Gene:PART041:dmpR(E135K)','Part',22,'001381480047426'),(73,'Create','Create part \'Gene:PART042:tbuT\'.',42,'Gene:PART042:tbuT','Part',22,'001381480087845'),(74,'Create','Create part \'Gene:PART043:tbuT\'.',43,'Gene:PART043:tbuT','Part',22,'001381480159096'),(75,'Change','Change part \'Gene:PART043:xylR\' information.',43,'Gene:PART043:xylR','Part',22,'001381480178896'),(76,'Create','Create part \'Gene:PART044:nitR\'.',44,'Gene:PART044:nitR','Part',22,'001381480221832'),(77,'Delete','Delete part \'Gene:PART037:EGFP\'.',37,'Gene:PART037:EGFP','Part',22,'001381480265889'),(78,'Create','Create part \'Gene:PART045:styR\'.',45,'Gene:PART045:styR','Part',22,'001381480350263'),(79,'Create','Create part \'Gene:PART046:styS\'.',46,'Gene:PART046:styS','Part',22,'001381480382017'),(80,'Delete','Delete part \'Promoter:PART011:promoter-02\'.',11,'Promoter:PART011:promoter-02','Part',22,'001381480412969'),(81,'Delete','Delete part \'Promoter:PART020:HCE promoter\'.',20,'Promoter:PART020:HCE promoter','Part',22,'001381480415750'),(82,'Delete','Delete part \'Promoter:PART019:Po\'.',19,'Promoter:PART019:Po','Part',22,'001381480418689'),(83,'Create','Create part \'Promoter:PART047:sigma54 dependent promoter\'.',47,'Promoter:PART047:sigma54 dependent promoter','Part',22,'001381493536310'),(84,'Create','Create part \'Promoter:PART048:trc promoter\'.',48,'Promoter:PART048:trc promoter','Part',22,'001381493564842'),(85,'Create','Create part \'Promoter:PART049:T7 promoter\'.',49,'Promoter:PART049:T7 promoter','Part',22,'001381493632401'),(86,'Create','Create part \'Promoter:PART050:HCE\'.',50,'Promoter:PART050:HCE','Part',22,'001381493703192'),(87,'Change','Change part \'Promoter:PART049:T7\' information.',49,'Promoter:PART049:T7','Part',22,'001381493709752'),(88,'Delete','Delete part \'Promoter:PART035:dmpR_Rha Po promoter\'.',35,'Promoter:PART035:dmpR_Rha Po promoter','Part',22,'001381493715284'),(89,'Delete','Delete part \'Promoter:PART034:dmpR original promoter\'.',34,'Promoter:PART034:dmpR original promoter','Part',22,'001381493719161'),(90,'Change','Change part \'Promoter:PART047:sigma54 dependent\' information.',47,'Promoter:PART047:sigma54 dependent','Part',22,'001381493727593'),(91,'Change','Change part \'Promoter:PART048:TRC\' information.',48,'Promoter:PART048:TRC','Part',22,'001381493735861'),(92,'Create','Create part \'Promoter:PART051:Regulatory region for DmpR\'.',51,'Promoter:PART051:Regulatory region for DmpR','Part',22,'001381494443603'),(93,'Delete','Delete part \'RBS:PART031:myRBS\'.',31,'RBS:PART031:myRBS','Part',22,'001381494467253'),(94,'Delete','Delete part \'RBS:PART036:T7 RBS\'.',36,'RBS:PART036:T7 RBS','Part',22,'001381494469664'),(95,'Create','Create part \'RBS:PART052:T7 RBS\'.',52,'RBS:PART052:T7 RBS','Part',22,'001381494499271'),(96,'Change','Change part \'RBS:PART052:T7\' information.',52,'RBS:PART052:T7','Part',22,'001381494516655'),(97,'Create','Create part \'Gene:PART053:EGFP\'.',53,'Gene:PART053:EGFP','Part',22,'001381494681315'),(98,'Change','Change part \'Gene:PART053:EGFP\' information.',53,'Gene:PART053:EGFP','Part',22,'001381494709129'),(99,'Change','Change part \'Gene:PART053:EGFP\' information.',53,'Gene:PART053:EGFP','Part',22,'001381494722832'),(100,'Change','Change part \'Gene:PART053:EGFP\' information.',53,'Gene:PART053:EGFP','Part',22,'001381494752676'),(101,'Create','Create part \'Gene:PART054:sf-GFP\'.',54,'Gene:PART054:sf-GFP','Part',22,'001381494783803'),(102,'Create','Create part \'Gene:PART055:Turbo-RFP\'.',55,'Gene:PART055:Turbo-RFP','Part',22,'001381494807945'),(103,'Create','Create part \'Gene:PART056:mRFP\'.',56,'Gene:PART056:mRFP','Part',22,'001381494838599'),(104,'Create','Append system type information \'Gene_sensor\'.',61,'Gene_sensor','Dictionary',22,'001381495547215'),(105,'Create','Append system type information \'Gene_reporter\'.',62,'Gene_reporter','Dictionary',22,'001381495698584'),(106,'Create','Append system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381495814290'),(107,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381495955335'),(108,'Create','Create part \'Gene_antibiotics:PART057:Chloramphenicol resistant gene\'.',57,'Gene_antibiotics:PART057:Chloramphenicol resistant gene','Part',22,'001381495982532'),(109,'Change','Change part \'Gene_sensor:PART041:dmpR(E135K)\' information.',41,'Gene_sensor:PART041:dmpR(E135K)','Part',22,'001381495997252'),(110,'Change','Change part \'Gene_reporter:PART053:EGFP\' information.',53,'Gene_reporter:PART053:EGFP','Part',22,'001381496003993'),(111,'Change','Change part \'Gene_sensor:PART033:dmpR(WT)\' information.',33,'Gene_sensor:PART033:dmpR(WT)','Part',22,'001381496011301'),(112,'Change','Change part \'Gene_sensor:PART042:tbuT\' information.',42,'Gene_sensor:PART042:tbuT','Part',22,'001381496028241'),(113,'Change','Change part \'Gene_sensor:PART043:xylR\' information.',43,'Gene_sensor:PART043:xylR','Part',22,'001381496035897'),(114,'Change','Change part \'Gene_sensor:PART044:nitR\' information.',44,'Gene_sensor:PART044:nitR','Part',22,'001381496043373'),(115,'Change','Change part \'Gene_reporter:PART054:sf-GFP\' information.',54,'Gene_reporter:PART054:sf-GFP','Part',22,'001381496057282'),(116,'Change','Change part \'Gene_reporter:PART055:Turbo-RFP\' information.',55,'Gene_reporter:PART055:Turbo-RFP','Part',22,'001381496061895'),(117,'Change','Change part \'Gene_reporter:PART056:mRFP\' information.',56,'Gene_reporter:PART056:mRFP','Part',22,'001381496069545'),(118,'Create','Create part \'Gene_antibiotics:PART058:Kanamycin resistant gene\'.',58,'Gene_antibiotics:PART058:Kanamycin resistant gene','Part',22,'001381496096384'),(119,'Create','Create part \'Gene_antibiotics:PART059:Tetracycline resistant gene\'.',59,'Gene_antibiotics:PART059:Tetracycline resistant gene','Part',22,'001381496117341'),(120,'Create','Create part \'Gene:PART060:DAAT(d-amino acid aminotransferase) gene\'.',60,'Gene:PART060:DAAT(d-amino acid aminotransferase) gene','Part',22,'001381496150274'),(121,'Change','Change part \'Terminator:PART018:rrnBT1T2\' information.',18,'Terminator:PART018:rrnBT1T2','Part',22,'001381496181620'),(122,'Change','Change part \'Terminator:PART015:Tl3 terminator\' information.',15,'Terminator:PART015:Tl3 terminator','Part',22,'001381496196136'),(123,'Change','Change part \'Terminator:PART018:rrnBT1T2\' information.',18,'Terminator:PART018:rrnBT1T2','Part',22,'001381496206229'),(124,'Delete','Delete part \'Terminator:PART032:tL3 terminator\'.',32,'Terminator:PART032:tL3 terminator','Part',22,'001381496219671'),(125,'Delete','Delete part \'Terminator:PART003:terminator-01\'.',3,'Terminator:PART003:terminator-01','Part',22,'001381496222867'),(126,'Delete','Delete part \'Terminator:PART014:terminator-02\'.',14,'Terminator:PART014:terminator-02','Part',22,'001381496225273'),(127,'Create','Create part \'Terminator:PART061:t0 terminator\'.',61,'Terminator:PART061:t0 terminator','Part',22,'001381496249259'),(128,'Create','Append system type information \'Ori\'.',64,'Ori','Dictionary',22,'001381496304904'),(129,'Create','Create part \'Ori:PART062:ColE1\'.',62,'Ori:PART062:ColE1','Part',22,'001381496322478'),(130,'Create','Create part \'Ori:PART063:ACYC\'.',63,'Ori:PART063:ACYC','Part',22,'001381496343661'),(131,'Create','Create part \'Ori:PART064:RSF\'.',64,'Ori:PART064:RSF','Part',22,'001381496357425'),(132,'Create','Create part \'Ori:PART065:CDF\'.',65,'Ori:PART065:CDF','Part',22,'001381496370663'),(133,'Delete','Delete module \'MOD001:module-01\'.',1,'MOD001:module-01','Module',22,'001381496711834'),(134,'Delete','Delete module \'MOD012:senser\'.',12,'MOD012:senser','Module',22,'001381496718882'),(135,'Delete','Delete module \'MOD011:module-02\'.',11,'MOD011:module-02','Module',22,'001381496728663'),(136,'Delete','Delete module \'MOD013:repoter\'.',13,'MOD013:repoter','Module',22,'001381496745284'),(137,'Create','Append system type information \'Sensor\'.',65,'Sensor','Dictionary',22,'001381496897714'),(138,'Change','Change system type information \'Msensor\'.',65,'Msensor','Dictionary',22,'001381496939410'),(139,'Change','Change system type information \'Sensor module\'.',65,'Sensor module','Dictionary',22,'001381496962743'),(140,'Create','Create module \'MOD021:phenol sensor\'.',21,'MOD021:phenol sensor','Module',22,'001381497019153'),(141,'Change','Change I/O unit \'chemicals:IO0015:phenol\' information.',15,'chemicals:IO0015:phenol','IOUnit',22,'001381497138179'),(142,'Delete','Delete I/O unit \'mRNA:IO0001:mRNA-01\' information.',1,'mRNA:IO0001:mRNA-01','IOUnit',22,'001381497159323'),(143,'Delete','Delete I/O unit \'mRNA:IO0011:mRNA-02\' information.',11,'mRNA:IO0011:mRNA-02','IOUnit',22,'001381497162419'),(144,'Delete','Delete I/O unit \'mRNA:IO0012:mRNA-03\' information.',12,'mRNA:IO0012:mRNA-03','IOUnit',22,'001381497165436'),(145,'Delete','Delete I/O unit \'protein dimer:IO0018:RNA polymerase\' information.',18,'protein dimer:IO0018:RNA polymerase','IOUnit',22,'001381497172163'),(146,'Delete','Delete I/O unit \'protein dimer:IO0019:EGFP\' information.',19,'protein dimer:IO0019:EGFP','IOUnit',22,'001381497175132'),(147,'Delete','Delete I/O unit \'protein dimer:IO0020:dmpR\' information.',20,'protein dimer:IO0020:dmpR','IOUnit',22,'001381497177943'),(148,'Delete','Delete I/O unit \'chemicals:IO0016:chem-02\' information.',16,'chemicals:IO0016:chem-02','IOUnit',22,'001381497180957'),(149,'Delete','Delete I/O unit \'chemicals:IO0017:chem-01\' information.',17,'chemicals:IO0017:chem-01','IOUnit',22,'001381497183641'),(150,'Delete','Delete I/O unit \'protein dimer:IO0013:protein-02\' information.',13,'protein dimer:IO0013:protein-02','IOUnit',22,'001381497186192'),(151,'Delete','Delete I/O unit \'protein dimer:IO0014:protein-03\' information.',14,'protein dimer:IO0014:protein-03','IOUnit',22,'001381497191121'),(161,'Change','Change system type information \'Reporter module\'.',16,'Reporter module','Dictionary',22,'001381711854796'),(162,'Create','Create module \'MOD031:EGFP reporter\'.',31,'MOD031:EGFP reporter','Module',22,'001381711900287'),(163,'Create','Create I/O unit \'chemicals:IO0021:phenol\'.',21,'chemicals:IO0021:phenol','IOUnit',22,'001381711996831'),(164,'Delete','Delete I/O unit \'chemicals:IO0015:phenol\' information.',15,'chemicals:IO0015:phenol','IOUnit',22,'001381712058886'),(165,'Change','Change system type information \'protein oligomer\'.',13,'protein oligomer','Dictionary',22,'001381712089315'),(166,'Create','Create I/O unit \'protein dimer:IO0022:dmpR\'.',22,'protein dimer:IO0022:dmpR','IOUnit',22,'001381712106110'),(167,'Create','Create I/O unit \'protein oligomer:IO0023:RNA polymerase\'.',23,'protein oligomer:IO0023:RNA polymerase','IOUnit',22,'001381712136632'),(168,'Create','Create I/O unit \'protein oligomer:IO0024:EGFP\'.',24,'protein oligomer:IO0024:EGFP','IOUnit',22,'001381712151199'),(169,'Change','Change module \'MOD021:phenol sensor\' information.',21,'MOD021:phenol sensor','Module',22,'001381712260714'),(170,'Change','Change module \'MOD031:EGFP reporter\' information.',31,'MOD031:EGFP reporter','Module',22,'001381712291485'),(171,'Delete','Delete device \'DEV001:device-01\'.',1,'DEV001:device-01','Device',22,'001381712299097'),(172,'Delete','Delete device \'DEV011:device-01\'.',11,'DEV011:device-01','Device',22,'001381712306688'),(173,'Delete','Delete device \'DEV012:device-02\'.',12,'DEV012:device-02','Device',22,'001381712331585'),(174,'Delete','Delete device \'DEV013:gess v1\'.',13,'DEV013:gess v1','Device',22,'001381712342060'),(175,'Change','Change system type information \'GESS\'.',32,'GESS','Dictionary',22,'001381712621132'),(176,'Create','Create device \'DEV021:GESSv4\'.',21,'DEV021:GESSv4','Device',22,'001381712686079'),(177,'Create','Create experiment protocol \'PRTO031:Protocol Gv4\'.',31,'PRTO031:Protocol Gv4','Protocol',22,'001381713208273'),(178,'Create','Create experiment result \'EXPR021:FACS1\'.',21,'EXPR021:FACS1','Experiment',22,'001381713297453'),(179,'Change','Change module \'MOD021:phenol sensor\' information.',21,'MOD021:phenol sensor','Module',22,'001381713371213'),(180,'Change','Change device \'DEV021:GESSv4\' information.',21,'DEV021:GESSv4','Device',22,'001381713479896'),(181,'Change','Change experiment result \'EXPR021:FACS1\' information.',21,'EXPR021:FACS1','Experiment',22,'001381713548059'),(182,'Change','Change experiment result \'EXPR021:FACS1\' information.',21,'EXPR021:FACS1','Experiment',22,'001381713585274'),(183,'Delete','Change experiment protocol \'PRTO031:Protocol Gv4\' information.',31,'PRTO031:Protocol Gv4','Protocol',22,'001381714832264'),(184,'Create','Create part \'Gene:PART071:Metagenome\'.',71,'Gene:PART071:Metagenome','Part',22,'001381715898477'),(185,'Create','Create device \'DEV022:test\'.',22,'DEV022:test','Device',22,'001381717561121'),(186,'Delete','Delete device \'DEV022:test\'.',22,'DEV022:test','Device',22,'001381717629093'),(187,'Change','Change system type information \'Gene_reporter\'.',62,'Gene_reporter','Dictionary',22,'001381814196725'),(188,'Change','Change system type information \'Gene_sensor\'.',61,'Gene_sensor','Dictionary',22,'001381814211727'),(189,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814220482'),(190,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814229301'),(191,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814230399'),(192,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814262422'),(193,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814265525'),(194,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814336377'),(195,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814340157'),(196,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814345335'),(197,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',22,'001381814373813'),(198,'Create','Append system type information \'tet\'.',71,'tet','Dictionary',22,'001381814407030'),(199,'Delete','Delete system type information \'tet\'.',71,'tet','Dictionary',22,'001381814429425'),(200,'Create','Create module \'MOD032:test\'.',32,'MOD032:test','Module',22,'001381814729648'),(201,'Create','Create module \'MOD033:test2\'.',33,'MOD033:test2','Module',22,'001381814758975'),(202,'Delete','Delete module \'MOD033:test2\'.',33,'MOD033:test2','Module',22,'001381814798843'),(203,'Delete','Delete module \'MOD032:test\'.',32,'MOD032:test','Module',22,'001381814811110'),(204,'Create','Create device \'DEV023:test\'.',23,'DEV023:test','Device',22,'001381814960862'),(205,'Delete','Delete device \'DEV023:test\'.',23,'DEV023:test','Device',22,'001381815015405'),(206,'Create','Create experiment result \'EXPR022:test\'.',22,'EXPR022:test','Experiment',22,'001381815985762'),(207,'Change','Change experiment result \'EXPR021:FACS1\' information.',21,'EXPR021:FACS1','Experiment',22,'001381816001958'),(208,'Change','Change experiment result \'EXPR022:test\' information.',22,'EXPR022:test','Experiment',22,'001381816018807'),(209,'Change','Change experiment result \'EXPR022:test\' information.',22,'EXPR022:test','Experiment',22,'001381816041188'),(211,'Change','Change system type information \'RBS\'.',42,'RBS','Dictionary',1,'001383888259504'),(212,'Change','Change system type information \'Gene_antibiotics\'.',63,'Gene_antibiotics','Dictionary',1,'001383888293107'),(221,'Change','Change system type information \'Gene_sensor\'.',61,'Gene_sensor','Dictionary',1,'001384276889076'),(222,'Change','Change system type information \'Gene_reporter\'.',62,'Gene_reporter','Dictionary',1,'001384276898770'),(223,'Change','Change system type information \'Ori\'.',64,'Ori','Dictionary',1,'001384276974570'),(231,'Create','Append system type information \'Vector\'.',81,'Vector','Dictionary',1,'001384443403688'),(232,'Create','Append system type information \'Culture condition\'.',82,'Culture condition','Dictionary',1,'001384443426057'),(233,'Create','Append system type information \'strain\'.',83,'strain','Dictionary',1,'001384443441767'),(234,'Create','Append system type information \'vector 1\'.',84,'vector 1','Dictionary',1,'001384443453295'),(235,'Create','Append system type information \'vector 2\'.',85,'vector 2','Dictionary',1,'001384443462919'),(236,'Create','Append system type information \'condition 1\'.',86,'condition 1','Dictionary',1,'001384443479663'),(237,'Create','Append system type information \'condition 2\'.',87,'condition 2','Dictionary',1,'001384443490616'),(238,'Create','Append system type information \'strain 1\'.',88,'strain 1','Dictionary',1,'001384443499108'),(239,'Create','Append system type information \'strain 2\'.',89,'strain 2','Dictionary',1,'001384443506902'),(240,'Delete','Change experiment protocol \'PRTO031:Protocol Gv4\' information.',31,'PRTO031:Protocol Gv4','Protocol',1,'001384443523413'),(241,'Change','Change experiment result \'EXPR021:FACS1\' information.',21,'EXPR021:FACS1','Experiment',1,'001384443552869'),(242,'Change','Change experiment result \'EXPR022:test\' information.',22,'EXPR022:test','Experiment',1,'001384443570055'),(251,'Create','Create module \'MOD041:test\'.',41,'MOD041:test','Module',1,'001384494230026'),(252,'Create','Create experiment protocol \'PRTO041:test\'.',41,'PRTO041:test','Protocol',1,'001384494746535'),(253,'Delete','Change experiment protocol \'PRTO041:test\' information.',41,'PRTO041:test','Protocol',1,'001384494755808'),(254,'Create','Create experiment result \'EXPR031:test\'.',31,'EXPR031:test','Experiment',1,'001384494813581'),(261,'Change','Change system type information \'Ori\'.',64,'Ori','Dictionary',1,'001384840310129'),(262,'Change','Change device \'DEV021:GESSv4\' information.',21,'DEV021:GESSv4','Device',1,'001384840451686'),(263,'Change','Change system type information \'Gene_reporter\'.',62,'Gene_reporter','Dictionary',1,'001384847755572'),(271,'Create','Append system type information \'test\'.',91,'test','Dictionary',1,'001384953446005'),(272,'Create','Create part \'test:PART081:test\'.',81,'test:PART081:test','Part',1,'001384953481520'),(281,'Delete','Delete part \'test:PART081:test\'.',81,'test:PART081:test','Part',1,'001384954861023'),(282,'Delete','Delete system type information \'test\'.',91,'test','Dictionary',1,'001384954867476'),(291,'Create','Create module \'MOD051:mymodule2\'.',51,'mymodule2','Module',22,'001392360708922'),(292,'Create','Create device \'DEV031:dmprgessdetail\'.',31,'dmprgessdetail','Device',22,'001392560555247'),(293,'Change','Change device \'DEV031:dmprgessdetail\' information.',31,'dmprgessdetail','Device',22,'001392560612014'),(294,'Create','Create experiment protocol \'PRTO051:dmprgessgrowth\'.',51,'dmprgessgrowth','Protocol',22,'001392560678455'),(301,'Change','Change part \'Promoter:PART048:TRC\' information.',48,'Promoter:PART048:TRC','Part',22,'001397714316612'),(302,'Change','Change part \'Promoter:PART047:sigma54 dependent -12\' information.',47,'Promoter:PART047:sigma54 dependent -12','Part',22,'001397714376779'),(303,'Create','Create part \'Promoter:PART091:sigma54 dependent -24 \'.',91,'Promoter:PART091:sigma54 dependent -24 ','Part',22,'001397714416514'),(304,'Change','Change part \'Promoter:PART091:sigma54 dependent -24 \' information.',91,'Promoter:PART091:sigma54 dependent -24 ','Part',22,'001397714445610'),(305,'Change','Change part \'Promoter:PART051:Regulatory region for DmpR\' information.',51,'Promoter:PART051:Regulatory region for DmpR','Part',22,'001397714478190'),(306,'Change','Change part \'Promoter:PART049:T7\' information.',49,'Promoter:PART049:T7','Part',22,'001397714508641'),(307,'Change','Change part \'Promoter:PART050:HCE\' information.',50,'Promoter:PART050:HCE','Part',22,'001397714532252'),(308,'Create','Create part \'Promoter:PART092:Strongest promoter\'.',92,'Promoter:PART092:Strongest promoter','Part',22,'001397714604529'),(309,'Change','Change system type information \'Three prime overhang\'.',10,'Three prime overhang','Dictionary',22,'001398912075892'),(310,'Delete','Delete module \'MOD041:test\'.',41,'test','Module',22,'001398912222440'),(311,'Delete','Delete device \'DEV031:dmprgessdetail\'.',31,'dmprgessdetail','Device',22,'001398912258038'),(312,'Delete','Delete experiment protocol \'PRTO041:test\'.',41,'test','Protocol',22,'001399871938941'),(321,'Create','Append system type information \'test part\'.',101,'test part','Dictionary',1,'001400747750520'),(322,'Delete','Delete system type information \'test part\'.',101,'test part','Dictionary',1,'001400747760802'),(323,'Create','Append system type information \'test dir\'.',102,'test dir','Dictionary',1,'001400747775748'),(331,'Delete','Delete module \'MOD051:mymodule2\'.',51,'mymodule2','Module',22,'001402357895790'),(332,'Create','Create I/O unit \'chemicals:IO0031:Isoprene\'.',31,'chemicals:IO0031:Isoprene','IOUnit',22,'001402358013666'),(333,'Create','Create I/O unit \'chemicals:IO0032:Cellobiose \'.',32,'chemicals:IO0032:Cellobiose ','IOUnit',22,'001402358122192'),(334,'Create','Create I/O unit \'chemicals:IO0033:caprolactam\'.',33,'chemicals:IO0033:caprolactam','IOUnit',22,'001402358195439'),(335,'Create','Create experiment protocol \'PRTO061:phGESS\'.',61,'phGESS','Protocol',22,'001402358323267'),(336,'Delete','Delete experiment protocol \'PRTO031:Protocol Gv4\'.',31,'Protocol Gv4','Protocol',22,'001402358335641'),(337,'Create','Append system type information \'Vector\'.',111,'Vector','Dictionary',22,'001403071836677'),(338,'Create','Create module \'MOD061:pET11a\'.',61,'pET11a','Module',22,'001403072322579'),(339,'Change','Change module \'MOD061:pET11a\' information.',61,'pET11a','Module',22,'001403072329804'),(340,'Create','Create part \'Terminator:PART101:T7 Terminator\'.',101,'Terminator:PART101:T7 Terminator','Part',22,'001403072706968'),(341,'Create','Create part \'Operator:PART102:LacO\'.',102,'Operator:PART102:LacO','Part',22,'001403072825202'),(342,'Create','Create part \'Gene:PART103:LacI\'.',103,'Gene:PART103:LacI','Part',22,'001403072854175'),(343,'Create','Create part \'Gene_antibiotics:PART104:Ampicillin\'.',104,'Gene_antibiotics:PART104:Ampicillin','Part',22,'001403072896395'),(344,'Create','Create part \'Promoter:PART105:AmpR promoter\'.',105,'Promoter:PART105:AmpR promoter','Part',22,'001403072929642'),(345,'Change','Change module \'MOD061:pET11a\' information.',61,'pET11a','Module',22,'001403075687022'),(346,'Change','Change module \'MOD061:pET11a\' information.',61,'pET11a','Module',22,'001403076200558'),(347,'Create','Create I/O unit \'protein oligomer:IO0034:LACI\'.',34,'protein oligomer:IO0034:LACI','IOUnit',22,'001403076397452'),(348,'Change','Change I/O unit \'protein oligomer:IO0022:DMPR\' information.',22,'protein oligomer:IO0022:DMPR','IOUnit',22,'001403076409866'),(349,'Change','Change module \'MOD061:pET11a\' information.',61,'pET11a','Module',22,'001403076435323'),(350,'Delete','Delete module \'MOD061:pET11a\'.',61,'pET11a','Module',22,'001403077699556'),(351,'Delete','Delete system type information \'Vector\'.',111,'Vector','Dictionary',22,'001403077705735'),(352,'Create','Append system type information \'Vector\'.',112,'Vector','Dictionary',22,'001403077732955'),(353,'Change','Change system type information \'Vector\'.',112,'Vector','Dictionary',22,'001403077934860'),(354,'Create','Create part \'Vector:PART106:pET11a\'.',106,'Vector:PART106:pET11a','Part',22,'001403078033628'),(355,'Change','Change module \'MOD031:EGFP reporter\' information.',31,'EGFP reporter','Module',22,'001403078086290'),(356,'Change','Change module \'MOD021:phenol sensor\' information.',21,'phenol sensor','Module',22,'001403078182781'),(357,'Delete','Change experiment protocol \'PRTO061:phGESS\' information.',61,'phGESS','Protocol',22,'001403078276579'),(358,'Delete','Change experiment protocol \'PRTO061:phGESS\' information.',61,'phGESS','Protocol',22,'001403145795927'),(361,'Change','Change module \'EGFP reporter(Reporter module)\' information.',31,'EGFP reporter','Module',1,'001406396129933'),(362,'Change','Change module \'phenol sensor(Sensor module)\' information.',21,'phenol sensor','Module',1,'001406396143919'),(363,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001406396189407'),(371,'Create','Create experiment result \'experiment-test(phGESS)\'.',41,'experiment-test','Experiment',1,'001406481476148'),(372,'Create','Create simulation result \'test-simulation(phGESS)\'.',1,'test-simulation','Simulation',1,'001406481518620'),(373,'Change','Change module \'EGFP reporter(Reporter module)\' information.',31,'EGFP reporter','Module',22,'001406518634861'),(374,'Change','Change module \'EGFP reporter(Reporter module)\' information.',31,'EGFP reporter','Module',22,'001406518645304'),(375,'Change','Change module \'EGFP reporter(Reporter module)\' information.',31,'EGFP reporter','Module',22,'001406518707431'),(376,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406518766039'),(377,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406518809589'),(378,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406518861108'),(379,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406519005912'),(380,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406519032677'),(381,'Create','Create simulation result \'testtest(phGESS)\'.',2,'testtest','Simulation',22,'001406520060406'),(391,'Change','Change I/O unit \'caprolactam(chemicals)\' information.',33,'caprolactam(chemicals)','IOUnit',22,'001406523033756'),(392,'Change','Change module \'EGFP reporter(Reporter module)\' information.',31,'EGFP reporter','Module',22,'001406523164295'),(393,'Change','Change module \'EGFP reporter(Reporter module)\' information.',31,'EGFP reporter','Module',22,'001406523183019'),(394,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406523235298'),(395,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406523251664'),(396,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406523287278'),(397,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406523374740'),(398,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406523422320'),(399,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406524284286'),(400,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406524319355'),(401,'Create','Create experiment result \'test(phGESS)\'.',51,'test','Experiment',22,'001406525082939'),(402,'Create','Create experiment result \'rersr(phGESS)\'.',52,'rersr','Experiment',22,'001406525175336'),(403,'Change','Change experiment result \'rersr(phGESS)\' information.',52,'rersr','Experiment',22,'001406525380074'),(404,'Create','Append system type information \'output name\'.',121,'output name','Dictionary',22,'001406525663965'),(405,'Create','Append system type information \'GREEN\'.',122,'GREEN','Dictionary',22,'001406525684772'),(406,'Change','Change experiment result \'ASA(phGESS)\' information.',53,'ASA','Experiment',22,'001406525710251'),(407,'Change','Change experiment result \'ASA(phGESS)\' information.',53,'ASA','Experiment',22,'001406525842824'),(408,'Create','Create simulation result \'ttt(phGESS)\'.',11,'ttt','Simulation',22,'001406525938876'),(411,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001406634992868'),(412,'Change','Change module \'EGFP reporter(Reporter module)\' information.',31,'EGFP reporter','Module',1,'001406647563803'),(413,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406680161565'),(414,'Change','Change module \'phenol sensor(Sensor module)\' information.',21,'phenol sensor','Module',22,'001406680191181'),(415,'Create','Create simulation result \'20140730(phGESS)\'.',21,'20140730','Simulation',1,'001406681497736'),(416,'Change','Change simulation result \'20140730(phGESS)\' information.',21,'20140730','Simulation',1,'001406681531451'),(417,'Change','Change simulation result \'20140730(phGESS)\' information.',21,'20140730','Simulation',1,'001406681545519'),(418,'Change','Change simulation result \'20140730(phGESS)\' information.',21,'20140730','Simulation',1,'001406681564331'),(419,'Change','Change simulation result \'20140730(phGESS)\' information.',21,'20140730','Simulation',1,'001406682361832'),(420,'Change','Change experiment result \'experiment-test(phGESS)\' information.',41,'experiment-test','Experiment',1,'001406682398209'),(421,'Change','Change experiment result \'experiment-test(phGESS)\' information.',41,'experiment-test','Experiment',1,'001406682413846'),(422,'Change','Change simulation result \'20140730(phGESS)\' information.',21,'20140730','Simulation',1,'001406683347958'),(431,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001406687332154'),(432,'Create','Create strain result \'sample-strain\'.',1,'sample-strain','Strain',1,'001406687676468'),(433,'Create','Append system type information \'Media\'.',131,'Media','Dictionary',1,'001406687747041'),(434,'Create','Append system type information \'media-01\'.',132,'media-01','Dictionary',1,'001406687758766'),(435,'Create','Append system type information \'media-02\'.',133,'media-02','Dictionary',1,'001406687767026'),(436,'Change','Change experiment protocol \'phGESS(GESSv4)\' information.',61,'phGESS','Protocol',1,'001406687776348'),(437,'Delete','Delete experiment \'testtest(phGESS)\'.',2,'testtest(phGESS)','Experiment',1,'001406688024595'),(438,'Delete','Delete experiment \'ttt(phGESS)\'.',11,'ttt(phGESS)','Experiment',1,'001406688024596'),(439,'Change','Change simulation result \'20140730(phGESS)\' information.',21,'20140730','Simulation',1,'001406697671268'),(440,'Change','Change simulation result \'20140730(phGESS)\' information.',21,'20140730','Simulation',1,'001406697684343'),(441,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406701419835'),(442,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406701665659'),(443,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406701753306'),(444,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406701805028'),(445,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',22,'001406701832770'),(446,'Create','Create experiment protocol \'aaa(GESSv4)\'.',71,'aaa','Protocol',22,'001406701898180'),(447,'Create','Create experiment result \'aasd(aaa)\'.',61,'aasd','Experiment',22,'001406702072658'),(448,'Change','Change part \'TRC(Promoter)\' information.',48,'TRC(Promoter)','Part',22,'001406702290030'),(449,'Change','Change part \'TRC(Promoter)\' information.',48,'TRC(Promoter)','Part',22,'001406702303063'),(450,'Change','Change part \'TRC(Promoter)\' information.',48,'TRC(Promoter)','Part',22,'001406702368908'),(451,'Create','Create module \'teata(Expression Unit)\'.',71,'teata','Module',22,'001406702463918'),(452,'Change','Change part \'TRC(Promoter)\' information.',48,'TRC(Promoter)','Part',1,'001407330724065'),(453,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407330984799'),(454,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407331149034'),(455,'Delete','Delete experiment \'test(phGESS)\'.',51,'test(phGESS)','Experiment',1,'001407331188232'),(456,'Delete','Delete experiment \'experiment-test(phGESS)\'.',41,'experiment-test(phGESS)','Experiment',1,'001407331188287'),(461,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407564575823'),(462,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407564688364'),(471,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407570534991'),(472,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407570547634'),(473,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407570560117'),(474,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407570571096'),(475,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407594810312'),(476,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407594824530'),(477,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407594855437'),(481,'Change','Change device \'DEV021:GESSv4\' information.',21,'GESSv4','Device',1,'001407595652375'),(482,'Delete','Delete system type information \'test dir\'.',102,'test dir','Dictionary',1,'001407717651611'),(491,'Create','Create part \'Test(Promoter)\'.',111,'Test(Promoter)','Part',31,'001431067735807'),(501,'Create','Create I/O unit \'test(mRNA)\'.',41,'test(mRNA)','IOUnit',31,'001431479634239'),(502,'Create','Create strain result \'test\'.',11,'test','Strain',31,'001431479664928'),(511,'Create','Create part \'test2(Promoter)\'.',121,'test2(Promoter)','Part',31,'001432080977753'),(512,'Create','Create part \'test3(Promoter)\'.',122,'test3(Promoter)','Part',31,'001432080998966'),(513,'Create','Create part \'test4(Promoter)\'.',123,'test4(Promoter)','Part',31,'001432081013286'),(514,'Create','Create strain result \'test[2]\'.',21,'test[2]','Strain',31,'001432081048693'),(515,'Create','Create strain result \'test[3]\'.',22,'test[3]','Strain',31,'001432081058870'),(516,'Create','Create strain result \'test[4]\'.',23,'test[4]','Strain',31,'001432081069081'),(517,'Create','Create strain result \'test[5]\'.',24,'test[5]','Strain',31,'001432081077854'),(518,'Create','Create strain result \'test[6]\'.',25,'test[6]','Strain',31,'001432081088135'),(519,'Create','Create strain result \'test[7]\'.',26,'test[7]','Strain',31,'001432081102580'),(520,'Create','Create strain result \'test[8]\'.',27,'test[8]','Strain',31,'001432081111850'),(521,'Create','Create strain result \'test[9]\'.',28,'test[9]','Strain',31,'001432081122018'),(522,'Create','Create strain result \'test[10]\'.',29,'test[10]','Strain',31,'001432081132945'),(523,'Create','Create part \'test5(Promoter)\'.',124,'test5(Promoter)','Part',31,'001432081146359'),(524,'Create','Create part \'test6(Promoter)\'.',125,'test6(Promoter)','Part',31,'001432081155867'),(525,'Create','Create part \'test7(Promoter)\'.',126,'test7(Promoter)','Part',31,'001432081164343'),(526,'Create','Create part \'test8(Promoter)\'.',127,'test8(Promoter)','Part',31,'001432081173779'),(527,'Create','Create part \'test9(Promoter)\'.',128,'test9(Promoter)','Part',31,'001432081183473'),(528,'Create','Create part \'test10(Promoter)\'.',129,'test10(Promoter)','Part',31,'001432081193067'),(529,'Create','Create part \'test11(Gene)\'.',130,'test11(Gene)','Part',31,'001432081206265'),(530,'Create','Create part \'test12(Gene_reporter)\'.',131,'test12(Gene_reporter)','Part',31,'001432081218164'),(531,'Create','Create part \'test13(Three prime overhang)\'.',132,'test13(Three prime overhang)','Part',31,'001432081232042'),(532,'Create','Create part \'test14(Operator)\'.',133,'test14(Operator)','Part',31,'001432081243204'),(533,'Create','Create part \'test16(Operator)\'.',134,'test16(Operator)','Part',31,'001432081259832'),(534,'Create','Create part \'test15(Operator)\'.',135,'test15(Operator)','Part',31,'001432081271841'),(535,'Create','Create part \'test17(Operator)\'.',136,'test17(Operator)','Part',31,'001432081292206'),(541,'Create','Create part \'test18(Promoter)\'.',141,'test18(Promoter)','Part',31,'001432095407271'),(542,'Delete','Delete part \'test18(Promoter)\'.',141,'test18(Promoter)','Part',31,'001432095435154'),(551,'Create','Create strain result \'test[11]\'.',31,'test[11]','Strain',31,'001432099655094'),(552,'Create','Create strain result \'test[12]\'.',32,'test[12]','Strain',31,'001432099724367'),(561,'Create','Create strain result \'test[15]\'.',41,'test[15]','Strain',31,'001432100010629'),(562,'Create','Create strain result \'test[17]\'.',42,'test[17]','Strain',31,'001432100097305'),(563,'Create','Create part \'test20(Promoter)\'.',151,'test20(Promoter)','Part',31,'001432100319284'),(571,'Create','Create part \'test21(Promoter)\'.',161,'test21(Promoter)','Part',31,'001432103206405'),(572,'Create','Create part \'test30(Promoter)\'.',162,'test30(Promoter)','Part',31,'001432103463897'),(581,'Create','Create strain result \'test[18]\'.',51,'test[18]','Strain',31,'001432103766180'),(582,'Create','Create strain result \'test[20]\'.',52,'test[20]','Strain',31,'001432104204090'),(583,'Create','Create strain result \'test[21]\'.',53,'test[21]','Strain',31,'001432104216737'),(591,'Delete','Delete part \'test30(Promoter)\'.',162,'test30(Promoter)','Part',31,'001433226524713'),(592,'Delete','Delete part \'test21(Promoter)\'.',161,'test21(Promoter)','Part',31,'001433226524820'),(601,'Create','Create part \'tbuT(Gene_sensor)\'.',42,'tbuT(Gene_sensor)','Part',31,'001433228439459'),(602,'Create','Create part \'T7(Promoter)\'.',49,'T7(Promoter)','Part',31,'001433228528177'),(603,'Create','Create part \'T7(RBS)\'.',52,'T7(RBS)','Part',31,'001433228644549'),(604,'Create','Create part \'bbbbtest(Promoter)\'.',171,'bbbbtest(Promoter)','Part',41,'001433228921921'),(605,'Create','Create part \'bbbbtest2(Promoter)\'.',172,'bbbbtest2(Promoter)','Part',41,'001433229058798'),(606,'Create','Create part \'bbbbtest(Promoter)\'.',171,'bbbbtest(Promoter)','Part',31,'001433229372327'),(607,'Create','Create part \'bbbbtest2(Promoter)\'.',172,'bbbbtest2(Promoter)','Part',31,'001433229372452'),(611,'Create','Create part \'bbbbtest(Promoter)\'.',171,'bbbbtest(Promoter)','Part',31,'001433229980395'),(612,'Create','Create part \'bbbbtest2(Promoter)\'.',172,'bbbbtest2(Promoter)','Part',31,'001433229980567'),(613,'Create','Create part \'bbbbtest(Promoter)\'.',171,'bbbbtest(Promoter)','Part',31,'001433230230966'),(614,'Create','Create part \'bbbbtest2(Promoter)\'.',172,'bbbbtest2(Promoter)','Part',31,'001433230231083'),(615,'Create','Create part \'bbbbtest(Promoter)\'.',171,'bbbbtest(Promoter)','Part',31,'001433230418355'),(616,'Create','Create part \'bbbbtest2(Promoter)\'.',172,'bbbbtest2(Promoter)','Part',31,'001433230418472'),(621,'Create','Create part \'bbbbtest(Promoter)\'.',191,'bbbbtest(Promoter)','Part',31,'001433230761872'),(622,'Create','Create part \'bbbbtest2(Promoter)\'.',192,'bbbbtest2(Promoter)','Part',31,'001433230761987'),(623,'Create','Create part \'Ampicillin(Gene_antibiotics)\'.',193,'Ampicillin(Gene_antibiotics)','Part',31,'001433230878027'),(631,'Create','Create part \'bbbbtest(Promoter)\'.',191,'bbbbtest(Promoter)','Part',31,'001433235133919'),(641,'Change','Change part \'test8(Promoter)\' information.',127,'test8(Promoter)','Part',31,'001433296652153'),(642,'Change','Change part \'test9(Promoter)\' information.',128,'test9(Promoter)','Part',31,'001433296652260'),(643,'Change','Change part \'test10(Promoter)\' information.',129,'test10(Promoter)','Part',31,'001433296652385'),(644,'Create','Create module \'test(Expression Unit)\'.',82,'test','Module',31,'001433296925058'),(645,'Change','Change part \'test9(Promoter)\' information.',128,'test9(Promoter)','Part',31,'001433296930708'),(646,'Change','Change part \'test10(Promoter)\' information.',129,'test10(Promoter)','Part',31,'001433296930839'),(651,'Change','Change part \'test14(Operator)\' information.',133,'test14(Operator)','Part',31,'001433297455649'),(652,'Change','Change part \'test16(Operator)\' information.',134,'test16(Operator)','Part',31,'001433297455811'),(653,'Create','Create module \'test2(Expression Unit)\'.',91,'test2','Module',31,'001433297455955'),(654,'Create','Create part \'Test(Promoter)\'.',111,'Test(Promoter)','Part',31,'001433304045179'),(661,'Create','Create part \'test14(Operator)\'.',201,'test14(Operator)','Part',41,'001433468117423'),(671,'Create','Create part \'TRC(Promoter)\'.',211,'TRC(Promoter)','Part',31,'001433741608324'),(672,'Create','Create part \'HCE(Promoter)\'.',212,'HCE(Promoter)','Part',31,'001433741608467'),(673,'Create','Create strain result \'sample-strain\'.',71,'sample-strain','Strain',31,'001433741789635'),(674,'Create','Create part \'test2(Promoter)\'.',121,'test2(Promoter)','Part',31,'001433741943748'),(675,'Delete','Delete strain \'sample-strain\'.',71,'sample-strain','Strain',31,'001433742248509'),(681,'Create','Basket I/O unit \'phenol(chemicals)\' information.',61,'phenol(chemicals)','IOUnit',31,'001433749062792'),(682,'Delete','Delete I/O unit \'phenol(chemicals)\' information.',61,'phenol(chemicals)','IOUnit',31,'001433749483267'),(683,'Create','Create part \'dmpR(E135K)(Gene_sensor)\'.',221,'dmpR(E135K)(Gene_sensor)','Part',31,'001433749508831'),(691,'Create','Create module \'test15(Reporter module)\'.',101,'test15','Module',31,'001433751374108'),(701,'Create','Create module \'test20(Reporter module)\'.',111,'test20','Module',31,'001433810146067'),(711,'Create','Create device \'DEV041:test1090\'.',41,'test1090','Device',31,'001433812424034'),(721,'Create','Create experiment protocol \'testtest\'.',-1,'testtest','Protocol',31,'001433813922831'),(731,'Create','Create experiment protocol \'aaaaatest\'.',-1,'aaaaatest','Protocol',31,'001433814344433'),(741,'Create','Create experiment protocol \'testtest\'.',-1,'testtest','Protocol',31,'001433815786730'),(742,'Create','Create experiment protocol \'phGESS\'.',102,'phGESS','Protocol',31,'001433816233808'),(743,'Delete','Delete protocol \'phGESS\'.',102,'phGESS','Protocol',31,'001433816256498'),(751,'Create','Create experiment protocol \'phGESS\'.',111,'phGESS','Protocol',31,'001433901651956'),(752,'Delete','Delete experiment protocol \'phGESS\'.',111,'phGESS','Protocol',31,'001433901700515'),(761,'Create','Create experiment result \'test(aaa)\'.',111,'test','Experiment',31,'001433915750286'),(762,'Delete','Delete experiment result \'test(aaa)\'.',111,'test','Experiment',31,'001433915764473'),(763,'Create','Create module \'test(Expression Unit)\'.',82,'test','Module',31,'001433916396349'),(771,'Create','Create part \'Tl3 terminator(Terminator)\'.',231,'Tl3 terminator(Terminator)','Part',41,'001433926328051'),(781,'Change','Change part \'Tl3 terminator(Terminator)\' information.',231,'Tl3 terminator(Terminator)','Part',41,'001433927130142'),(782,'Change','Change part \'Tl3 terminator(Terminator)\' information.',231,'Tl3 terminator(Terminator)','Part',41,'001433927357953'),(791,'Create','Basket I/O unit \'DMPR(protein oligomer)\' information.',71,'DMPR(protein oligomer)','IOUnit',31,'001435816776461'),(801,'Create','Create experiment result \'Test(phGESS)\'.',131,'Test','Experiment',31,'001437460961640'),(811,'Delete','Delete experiment \'Test(phGESS)\'.',131,'Test(phGESS)','Experiment',31,'001437461870521'),(812,'Create','Create experiment result \'Test(phGESS)\'.',141,'Test','Experiment',31,'001437462025606'),(813,'Create','Create experiment result \'Test2(phGESS)\'.',142,'Test2','Experiment',31,'001437462690630'),(821,'Create','Create experiment result \'Test3(phGESS)\'.',151,'Test3','Experiment',31,'001437463322816'),(822,'Create','Create experiment result \'Test4(phGESS)\'.',152,'Test4','Experiment',31,'001437463989982'),(823,'Create','Create experiment result \'Test10(phGESS)\'.',153,'Test10','Experiment',31,'001437464311488'),(831,'Create','Create experiment result \'Test20(phGESS)\'.',161,'Test20','Experiment',31,'001437464659738'),(841,'Delete','Delete experiment \'Test20(phGESS)\'.',161,'Test20(phGESS)','Experiment',31,'001437467757878'),(842,'Delete','Delete experiment \'Test10(phGESS)\'.',153,'Test10(phGESS)','Experiment',31,'001437467759150'),(843,'Delete','Delete experiment \'Test4(phGESS)\'.',152,'Test4(phGESS)','Experiment',31,'001437467759972'),(844,'Delete','Delete experiment \'Test3(phGESS)\'.',151,'Test3(phGESS)','Experiment',31,'001437467760670'),(851,'Create','Create experiment result \'TEST1212(phGESS)\'.',171,'TEST1212','Experiment',31,'001437469514210'),(852,'Create','Create experiment result \'TEST13651(phGESS)\'.',173,'TEST13651','Experiment',31,'001437469778206'),(853,'Create','Create experiment result \'AAAAA(phGESS)\'.',174,'AAAAA','Experiment',31,'001437471252048'),(861,'Create','Create experiment result \'TESTEST(phGESS)\'.',181,'TESTEST','Experiment',31,'001437526883510'),(871,'Create','Create experiment result \'tEst1(phGESS)\'.',191,'tEst1','Experiment',31,'001437527873074'),(881,'Delete','Delete experiment \'tEst1(phGESS)\'.',191,'tEst1(phGESS)','Experiment',31,'001437528213785'),(882,'Delete','Delete experiment \'TESTEST(phGESS)\'.',181,'TESTEST(phGESS)','Experiment',31,'001437528214061'),(883,'Delete','Delete experiment \'AAAAA(phGESS)\'.',174,'AAAAA(phGESS)','Experiment',31,'001437528214336'),(884,'Delete','Delete experiment \'TEST13651(phGESS)\'.',173,'TEST13651(phGESS)','Experiment',31,'001437528214637'),(885,'Delete','Delete experiment \'TEST13651(phGESS)\'.',172,'TEST13651(phGESS)','Experiment',31,'001437528214745'),(886,'Delete','Delete experiment \'TEST1212(phGESS)\'.',171,'TEST1212(phGESS)','Experiment',31,'001437528215087'),(891,'Create','Create experiment result \'TEST6(phGESS)\'.',221,'TEST6','Experiment',31,'001437528790832'),(901,'Delete','Delete experiment \'TEST6(phGESS)\'.',221,'TEST6(phGESS)','Experiment',31,'001437530289231'),(902,'Delete','Delete experiment \'test4(phGESS)\'.',211,'test4(phGESS)','Experiment',31,'001437530289322'),(903,'Delete','Delete experiment \'TEST3(phGESS)\'.',201,'TEST3(phGESS)','Experiment',31,'001437530289414'),(904,'Create','Create experiment result \'Test45(phGESS)\'.',241,'Test45','Experiment',31,'001437530361121'),(911,'Delete','Delete experiment \'Test45(phGESS)\'.',241,'Test45(phGESS)','Experiment',31,'001437531451175'),(912,'Delete','Delete experiment \'Test2(phGESS)\'.',142,'Test2(phGESS)','Experiment',31,'001437531451710'),(913,'Create','Create experiment result \'Test11(phGESS)\'.',261,'Test11','Experiment',31,'001437531478110'),(921,'Delete','Delete experiment \'Test11(phGESS)\'.',261,'Test11(phGESS)','Experiment',31,'001437531673163'),(922,'Create','Create experiment result \'Test12(phGESS)\'.',281,'Test12','Experiment',31,'001437531711547'),(931,'Delete','Delete experiment result \'Test(phGESS)\'.',141,'Test','Experiment',31,'001437535561599'),(941,'Create','Create experiment result \'Test465(phGESS)\'.',301,'Test465','Experiment',31,'001437543460567'),(951,'Delete','Delete experiment \'Test465(phGESS)\'.',301,'Test465(phGESS)','Experiment',31,'001437957522731'),(952,'Delete','Delete experiment \'Test12(phGESS)\'.',281,'Test12(phGESS)','Experiment',31,'001437957530900'),(961,'Create','Create experiment result \'TEST4685(phGESS)\'.',321,'TEST4685','Experiment',31,'001438058087153'),(971,'Create','Create experiment result \'Test46985(phGESS)\'.',341,'Test46985','Experiment',31,'001438062447470'),(981,'Delete','Delete experiment \'TEST7946(phGESS)\'.',361,'TEST7946(phGESS)','Experiment',31,'001438133160003'),(991,'Delete','Delete experiment \'TEST7777(phGESS)\'.',371,'TEST7777(phGESS)','Experiment',31,'001438133438951'),(992,'Create','Create experiment result \'TEST7979(phGESS)\'.',381,'TEST7979','Experiment',31,'001438133483936'),(1001,'Create','Create experiment result \'TEST95465(phGESS)\'.',441,'TEST95465','Experiment',31,'001438143345069'),(1011,'Create','Create experiment result \'Test46985(phGESS)\'.',461,'Test46985','Experiment',31,'001438144181653'),(1021,'Create','Create experiment result \'TESt12221(phGESS)\'.',481,'TESt12221','Experiment',31,'001438144709992'),(1031,'Delete','Delete experiment \'TESt12221(phGESS)\'.',481,'TESt12221(phGESS)','Experiment',31,'001438145020658'),(1032,'Delete','Delete experiment \'Test46985(phGESS)\'.',461,'Test46985(phGESS)','Experiment',31,'001438145021068'),(1033,'Delete','Delete experiment \'TEST95465(phGESS)\'.',441,'TEST95465(phGESS)','Experiment',31,'001438145021467'),(1034,'Create','Create experiment result \'TEST1(phGESS)\'.',501,'TEST1','Experiment',31,'001438145078366'),(1041,'Create','Create strain result \'test134679\'.',81,'test134679','Strain',31,'001438676768757'),(1051,'Create','Create experiment result \'TESTEST(phGESS)\'.',521,'TESTEST','Experiment',31,'001439356331527'),(1061,'Create','Create experiment result \'TESTTEST(phGESS)\'.',-1,'TESTTEST','Experiment',31,'001439415555915'),(1062,'Create','Create experiment result \'TEST4652(phGESS)\'.',-1,'TEST4652','Experiment',31,'001439415634234'),(1063,'Delete','Delete experiment \'TEST1(phGESS)\'.',501,'TEST1(phGESS)','Experiment',31,'001439416332252'),(1064,'Create','Create experiment result \'TE(phGESS)\'.',-1,'TE','Experiment',31,'001439416361617'),(1071,'Create','Create experiment result \'TEST7878(phGESS)\'.',-1,'TEST7878','Experiment',31,'001439416991170'),(1081,'Create','Create device \'DEV052:testest\'.',52,'testest','Device',31,'001440564353238'),(1091,'Create','Create device \'DEV061:TEST0901\'.',61,'TEST0901','Device',31,'001441070581081'),(1092,'Create','Create device \'DEV062:TESTIO\'.',62,'TESTIO','Device',31,'001441071280148'),(1101,'Create','Create device \'DEV081:test123456\'.',81,'test123456','Device',31,'001441598256769'),(1102,'Create','Create part \'test111111(Promoter)\'.',241,'test111111(Promoter)','Part',31,'001441598425502'),(1111,'Create','Create experiment protocol \'1111\'.',-1,'1111','Protocol',31,'001441783489518'),(1112,'Create','Create experiment protocol \'protocol1\'.',-1,'protocol1','Protocol',31,'001441783560765'),(1113,'Create','Create experiment protocol \'phGESS\'.',123,'phGESS','Protocol',31,'001441783632350'),(1114,'Create','Create experiment result \'my exp(phGESS)\'.',-1,'my exp','Experiment',31,'001441783847960'),(1121,'Create','Create experiment protocol \'0910Protocoal\'.',-1,'0910Protocoal','Protocol',31,'001441847685095'),(1122,'Create','Create experiment result \'testExperiment(phGESS)\'.',-1,'testExperiment','Experiment',31,'001441847819598'),(1131,'Create','Create experiment protocol \'testtest\'.',-1,'testtest','Protocol',31,'001441849743940'),(1132,'Create','Create experiment result \'erwerqwerqer(phGESS)\'.',-1,'erwerqwerqer','Experiment',31,'001441850543686'),(1133,'Create','Create experiment result \'testwerqwer(phGESS)\'.',678,'testwerqwer','Experiment',31,'001441850733560'),(1141,'Create','Create experiment protocol \'tesdfwefsdf\'.',-1,'tesdfwefsdf','Protocol',31,'001442282118278'),(1151,'Create','Create experiment protocol \'tesdfswerasdf\'.',-1,'tesdfswerasdf','Protocol',31,'001442283221413'),(1152,'Create','Create experiment protocol \'yrdfgergwdf\'.',172,'yrdfgergwdf','Protocol',31,'001442283954632'),(1153,'Delete','Delete experiment protocol \'yrdfgergwdf\'.',172,'yrdfgergwdf','Protocol',31,'001442289315443'),(1154,'Delete','Delete experiment result \'testwerqwer(phGESS)\'.',678,'testwerqwer','Experiment',31,'001442290322664'),(1161,'Create','Create experiment result \'tesdfweqwerasdf(aaa)\'.',701,'tesdfweqwerasdf','Experiment',31,'001442306602889'),(1171,'Change','Change experiment result \'tesdfweqwerasdf(phGESS)\' information.',701,'tesdfweqwerasdf','Experiment',31,'001442309654812'),(1172,'Change','Change experiment result \'TESTEST(phGESS)\' information.',521,'TESTEST','Experiment',31,'001442309817945'),(1181,'Create','Create experiment result \'aaaaa(phGESS)\'.',-1,'aaaaa','Experiment',31,'001442551815846'),(1191,'Create','Create experiment result \'tasdfqsd(phGESS)\'.',741,'tasdfqsd','Experiment',31,'001442552288040'),(1201,'Create','Create experiment result \'tesdfwef(aaa)\'.',761,'tesdfwef','Experiment',31,'001442554408032'),(1202,'Delete','Delete experiment result \'tesdfwef(aaa)\'.',761,'tesdfwef','Experiment',31,'001442554440681'),(1211,'Change','Change experiment result \'tasdfqsd(phGESS)\' information.',741,'tasdfqsd','Experiment',31,'001442801309573'),(1212,'Create','Create experiment result \'DateTest(phGESS)\'.',781,'DateTest','Experiment',31,'001442801959173'),(1213,'Create','Create experiment result \'DateTest(phGESS)\'.',798,'DateTest','Experiment',31,'001442801994183'),(1214,'Change','Change experiment result \'DateTest(phGESS)\' information.',781,'DateTest','Experiment',31,'001442802039316'),(1221,'Change','Change experiment result \'DateTest(phGESS)\' information.',798,'DateTest','Experiment',31,'001442806499373'),(1231,'Change','Change experiment result \'DateTest(phGESS)\' information.',781,'DateTest','Experiment',31,'001442808347161'),(1232,'Change','Change experiment result \'DateTest(phGESS)\' information.',798,'DateTest','Experiment',31,'001442809212531'),(1241,'Change','Change experiment result \'DateTest(phGESS)\' information.',781,'DateTest','Experiment',31,'001442809518734'),(1251,'Create','Create experiment result \'testtesttest(aaa)\'.',831,'testtesttest','Experiment',31,'001444023781241'),(1261,'Create','Create experiment result \'test1313(aaa)\'.',931,'test1313','Experiment',31,'001444025199038'),(1271,'Change','Change part \'TRC(Promoter)\' information.',48,'TRC(Promoter)','Part',31,'001444091552464'),(1272,'Change','Change part \'HCE(Promoter)\' information.',50,'HCE(Promoter)','Part',31,'001444091552638'),(1273,'Create','Create module \'ModuleTest(Expression Unit)\'.',131,'ModuleTest','Module',31,'001444091552734'),(1281,'Create','Create experiment result \'testset123123123123(aaa)\'.',1031,'testset123123123123','Experiment',31,'001444098988160'),(1291,'Create','Create device \'DEV091:test10101362\'.',91,'test10101362','Device',31,'001444113331085'),(1301,'Create','Create device \'DEV101:DeviceTest\'.',101,'DeviceTest','Device',31,'001444122128289'),(1311,'Create','Create device \'DEV111:tsdfwerType\'.',111,'tsdfwerType','Device',31,'001444195203863'),(1321,'Create','Create experiment result \'TESTTECAN(phGESS)\'.',1231,'TESTTECAN','Experiment',31,'001444970441698'),(1331,'Delete','Delete experiment \'TESTTECAN(phGESS)\'.',1231,'TESTTECAN(phGESS)','Experiment',31,'001444977191819'),(1341,'Create','Create experiment result \'TESTTECAN(phGESS)\'.',1241,'TESTTECAN','Experiment',31,'001444979010911'),(1351,'Create','Create experiment result \'testtesttecan(phGESS)\'.',1251,'testtesttecan','Experiment',31,'001444980012610'),(1361,'Delete','Delete experiment \'TESTTECAN(phGESS)\'.',1241,'TESTTECAN(phGESS)','Experiment',31,'001444980225961'),(1362,'Delete','Delete experiment \'testtesttecan(phGESS)\'.',1251,'testtesttecan(phGESS)','Experiment',31,'001444980226261'),(1371,'Create','Create experiment result \'testTecan(phGESS)\'.',1261,'testTecan','Experiment',31,'001444982779142'),(1381,'Create','Create experiment result \'testTecan1(phGESS)\'.',1271,'testTecan1','Experiment',31,'001444983001336'),(1391,'Delete','Delete experiment \'testTecan1(phGESS)\'.',1271,'testTecan1(phGESS)','Experiment',31,'001444983275249'),(1401,'Create','Create experiment result \'testA(phGESS)\'.',1281,'testA','Experiment',31,'001444984163182'),(1402,'Delete','Delete experiment \'testA(phGESS)\'.',1281,'testA(phGESS)','Experiment',31,'001444984268614'),(1411,'Delete','Delete experiment \'testTecan(phGESS)\'.',1261,'testTecan(phGESS)','Experiment',31,'001444986449671'),(1421,'Create','Create experiment result \'testTecan(phGESS)\'.',1291,'testTecan','Experiment',31,'001445312917846'),(1422,'Delete','Delete experiment \'testTecan(phGESS)\'.',1291,'testTecan(phGESS)','Experiment',31,'001445313911897'),(1431,'Create','Create experiment result \'testTecan2(phGESS)\'.',1301,'testTecan2','Experiment',31,'001445314253435'),(1441,'Create','Create experiment result \'testTecan(phGESS)\'.',1311,'testTecan','Experiment',31,'001445315038446'),(1451,'Delete','Delete experiment \'testTecan(phGESS)\'.',1311,'testTecan(phGESS)','Experiment',31,'001445315381139'),(1452,'Delete','Delete experiment \'testTecan2(phGESS)\'.',1301,'testTecan2(phGESS)','Experiment',31,'001445315381772'),(1453,'Create','Create experiment result \'TestTecan1(phGESS)\'.',1321,'TestTecan1','Experiment',31,'001445315451604'),(1461,'Create','Create experiment result \'testTecan2(phGESS)\'.',1332,'testTecan2','Experiment',31,'001445323829933'),(1471,'Create','Create experiment result \'testVictor1321(phGESS)\'.',1351,'testVictor1321','Experiment',31,'001445387481334'),(1481,'Delete','Delete experiment \'testVictor(phGESS)\'.',1341,'testVictor(phGESS)','Experiment',31,'001445393999122'),(1482,'Delete','Delete experiment \'testTecan2(phGESS)\'.',1332,'testTecan2(phGESS)','Experiment',31,'001445394014850'),(1483,'Delete','Delete experiment \'testTecan2(phGESS)\'.',1331,'testTecan2(phGESS)','Experiment',31,'001445394023385'),(1484,'Create','Create part \'test6(Promoter)\'.',125,'test6(Promoter)','Part',31,'001445394148169'),(1485,'Create','Create part \'test7(Promoter)\'.',126,'test7(Promoter)','Part',31,'001445394160148'),(1491,'Delete','Delete experiment \'testVictor1321(phGESS)\'.',1351,'testVictor1321(phGESS)','Experiment',31,'001445506712280'),(1501,'Create','Create experiment result \'TestVictor12(phGESS)\'.',1361,'TestVictor12','Experiment',31,'001445574455009'),(1502,'Change','Change experiment result \'TestVictor12(phGESS)\' information.',1361,'TestVictor12','Experiment',31,'001445574540983'),(1511,'Delete','Delete experiment \'TestVictor12(phGESS)\'.',1361,'TestVictor12(phGESS)','Experiment',31,'001445838502852'),(1512,'Create','Create experiment result \'testVictor1(phGESS)\'.',1371,'testVictor1','Experiment',31,'001445838575474'),(1521,'Delete','Delete experiment \'TestTecan1(phGESS)\'.',1321,'TestTecan1(phGESS)','Experiment',31,'001445848930219'),(1531,'Create','Create experiment result \'testTecan2(phGESS)\'.',1391,'testTecan2','Experiment',31,'001445909714278'),(1541,'Create','Create experiment result \'testTecan3(phGESS)\'.',1401,'testTecan3','Experiment',31,'001445918911228'),(1551,'Delete','Delete experiment \'testTecan3(phGESS)\'.',1401,'testTecan3(phGESS)','Experiment',31,'001445919108179'),(1552,'Create','Create experiment result \'testTecan3(phGESS)\'.',1411,'testTecan3','Experiment',31,'001445919414894'),(1561,'Create','Create experiment result \'testTecan4545(aaa)\'.',1421,'testTecan4545','Experiment',31,'001445922853252'),(1571,'Delete','Delete experiment \'testTecan4545(aaa)\'.',1421,'testTecan4545(aaa)','Experiment',31,'001445936031938'),(1572,'Delete','Delete experiment \'testTecan3(phGESS)\'.',1411,'testTecan3(phGESS)','Experiment',31,'001445936032272'),(1573,'Delete','Delete experiment \'testTecan2(phGESS)\'.',1391,'testTecan2(phGESS)','Experiment',31,'001445936032606'),(1574,'Delete','Delete experiment \'testTecan2(phGESS)\'.',1381,'testTecan2(phGESS)','Experiment',31,'001445936032775'),(1575,'Create','Create experiment result \'testTecan12(phGESS)\'.',1431,'testTecan12','Experiment',31,'001445936454145'),(1581,'Create','Create experiment result \'tecan1(phGESS)\'.',1441,'tecan1','Experiment',31,'001445936905260'),(1591,'Create','Create experiment protocol \'ABCD\'.',181,'ABCD','Protocol',31,'001446082192915'),(1592,'Create','Create experiment protocol \'TFCD\'.',182,'TFCD','Protocol',31,'001446082216020'),(1593,'Create','Create device \'DEV121:Device2\'.',121,'Device2','Device',31,'001446083674169'),(1594,'Create','Create device \'DEV123:Device2323\'.',123,'Device2323','Device',31,'001446083917722'),(1595,'Create','Create device \'DEV122:Device2323\'.',122,'Device2323','Device',31,'001446083917766');
/*!40000 ALTER TABLE `EventHistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Experiment`
--

DROP TABLE IF EXISTS `Experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Experiment` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험 결과 ID',
  `protocolID` bigint(20) NOT NULL COMMENT '실험 프로토콜 ID',
  `name` varchar(128) DEFAULT NULL COMMENT '실험결과 이름',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '실험 생성자 ID',
  `experimentDate` varchar(15) DEFAULT NULL COMMENT '실험날짜',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  `p_description` text,
  `outputList` varchar(100) DEFAULT NULL,
  `kind` varchar(10) NOT NULL DEFAULT 'Victor',
  PRIMARY KEY (`experimentID`),
  KEY `ER_REFS_PROTOCOLID` (`protocolID`),
  KEY `ER_REFS_USERID` (`userID`),
  KEY `Exper_Name_IDX` (`name`),
  CONSTRAINT `ER_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`),
  CONSTRAINT `ER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Experiment`
--

LOCK TABLES `Experiment` WRITE;
/*!40000 ALTER TABLE `Experiment` DISABLE KEYS */;
INSERT INTO `Experiment` VALUES (52,61,'rersr','rarrw',22,'001404399600000','001406525175316','001406525389853','YES',NULL,'A,B',''),(53,61,'ASA',NULL,22,'001404313200000','001406525492295','001406525842820','YES',NULL,'A,B',''),(61,71,'aasd',NULL,22,'001404140400000','001406702072652','001406702072652','YES',NULL,'A,B',''),(781,123,'DateTest',NULL,31,'001404104040000','001442801954290','001442809518083','YES','Protocol name ..................... Haseong - EGFP/OD time\nProtocol number ................... N/A\nName of the plate type ............ Generic 8x12 size plate\nNumber of repeats ................. 1\nDelay between repeats ............. 900 s\nMeasurement height ................ 8.00 mm\nProtocol notes .................... \n\nName of the label ................. 595nm_kk\nLabel technology .................. Photometry\nCW-lamp filter name ............... 595/10\nCW-lamp filter slot ............... A4\nMeasurement time .................. 0.1 s\nAbsorbance Mode ................... Visible\nExcitation Aperture ............... Normal\n\nName of the label ................. EGFP_sulim\nLabel technology .................. Prompt fluorometry\nCW-lamp filter name ............... F485\nCW-lamp filter slot ............... A5\nEmission filter name .............. D535/40\nEmission filter slot .............. A3\nMeasurement time .................. 0.1 s\nEmission aperture ................. Normal\nCW-lamp energy .................... 1000\nSecond measurement CW-lamp energy . 0\nEmission side ..................... Above\nCW-Lamp Control ................... Stabilized Energy\nExcitation Aperture ............... Normal\n\n=============================\nPlate map of palte 1\nA | M   M   M   M   M   M   M   M   M   M   E   E   \nB | M   M   M   M   M   M   M   M   M   M   E   E   \nC | M   M   M   M   M   M   M   M   M   M   E   E   \nD | M   M   M   M   M   M   M   M   M   M   E   E   \nE | M   M   M   M   M   M   M   M   M   M   E   E   \nF | M   M   M   M   M   M   M   M   M   M   E   E   \nG | M   M   M   M   M   M   M   M   M   M   E   E   \nH | M   M   M   M   M   M   M   M   M   M   E   E   \n\n\n\nAssay ID:  ........................ 6529','Photometry(  595nm_kk),  Prompt fluorometry(  EGFP_sulim)',''),(798,123,'DateTest',NULL,31,'001404104040000','001442801979081','001442809211946','YES','Protocol name ..................... Haseong - EGFP/OD time\nProtocol number ................... N/A\nName of the plate type ............ Generic 8x12 size plate\nNumber of repeats ................. 1\nDelay between repeats ............. 900 s\nMeasurement height ................ 8.00 mm\nProtocol notes .................... \n\nName of the label ................. 595nm_kk\nLabel technology .................. Photometry\nCW-lamp filter name ............... 595/10\nCW-lamp filter slot ............... A4\nMeasurement time .................. 0.1 s\nAbsorbance Mode ................... Visible\nExcitation Aperture ............... Normal\n\nName of the label ................. EGFP_sulim\nLabel technology .................. Prompt fluorometry\nCW-lamp filter name ............... F485\nCW-lamp filter slot ............... A5\nEmission filter name .............. D535/40\nEmission filter slot .............. A3\nMeasurement time .................. 0.1 s\nEmission aperture ................. Normal\nCW-lamp energy .................... 1000\nSecond measurement CW-lamp energy . 0\nEmission side ..................... Above\nCW-Lamp Control ................... Stabilized Energy\nExcitation Aperture ............... Normal\n\n=============================\nPlate map of palte 1\nA | M   M   M   M   M   M   M   M   M   M   E   E   \nB | M   M   M   M   M   M   M   M   M   M   E   E   \nC | M   M   M   M   M   M   M   M   M   M   E   E   \nD | M   M   M   M   M   M   M   M   M   M   E   E   \nE | M   M   M   M   M   M   M   M   M   M   E   E   \nF | M   M   M   M   M   M   M   M   M   M   E   E   \nG | M   M   M   M   M   M   M   M   M   M   E   E   \nH | M   M   M   M   M   M   M   M   M   M   E   E   \n\n\n\nAssay ID:  ........................ 6529','Photometry(  595nm_kk),  Prompt fluorometry(  EGFP_sulim)',''),(831,71,'testtesttest',NULL,31,'001426172400000','001444023774218','001444023774218','YES','Protocol name ..................... Haseong - EGFP/OD time\nProtocol number ................... N/A\nName of the plate type ............ Generic 8x12 size plate\nNumber of repeats ................. 1\nDelay between repeats ............. 900 s\nMeasurement height ................ 8.00 mm\nProtocol notes .................... \n\nName of the label ................. 595nm_kk\nLabel technology .................. Photometry\nCW-lamp filter name ............... 595/10\nCW-lamp filter slot ............... A4\nMeasurement time .................. 0.1 s\nAbsorbance Mode ................... Visible\nExcitation Aperture ............... Normal\n\nName of the label ................. EGFP_sulim\nLabel technology .................. Prompt fluorometry\nCW-lamp filter name ............... F485\nCW-lamp filter slot ............... A5\nEmission filter name .............. D535/40\nEmission filter slot .............. A3\nMeasurement time .................. 0.1 s\nEmission aperture ................. Normal\nCW-lamp energy .................... 1000\nSecond measurement CW-lamp energy . 0\nEmission side ..................... Above\nCW-Lamp Control ................... Stabilized Energy\nExcitation Aperture ............... Normal\n\n=============================\nPlate map of palte 1\nA | M   M   M   M   M   M   M   M   M   M   E   E   \nB | M   M   M   M   M   M   M   M   M   M   E   E   \nC | M   M   M   M   M   M   M   M   M   M   E   E   \nD | M   M   M   M   M   M   M   M   M   M   E   E   \nE | M   M   M   M   M   M   M   M   M   M   E   E   \nF | M   M   M   M   M   M   M   M   M   M   E   E   \nG | M   M   M   M   M   M   M   M   M   M   E   E   \nH | M   M   M   M   M   M   M   M   M   M   E   E   \n\n\n\nAssay ID:  ........................ 6529','  Photometry(  595nm_kk),  Prompt fluorometry(  EGFP_sulim)',''),(931,71,'test1313',NULL,31,'001426172400000','001444025192976','001444025192976','YES','Protocol name ..................... Haseong - EGFP/OD time\nProtocol number ................... N/A\nName of the plate type ............ Generic 8x12 size plate\nNumber of repeats ................. 1\nDelay between repeats ............. 900 s\nMeasurement height ................ 8.00 mm\nProtocol notes .................... \n\nName of the label ................. 595nm_kk\nLabel technology .................. Photometry\nCW-lamp filter name ............... 595/10\nCW-lamp filter slot ............... A4\nMeasurement time .................. 0.1 s\nAbsorbance Mode ................... Visible\nExcitation Aperture ............... Normal\n\nName of the label ................. EGFP_sulim\nLabel technology .................. Prompt fluorometry\nCW-lamp filter name ............... F485\nCW-lamp filter slot ............... A5\nEmission filter name .............. D535/40\nEmission filter slot .............. A3\nMeasurement time .................. 0.1 s\nEmission aperture ................. Normal\nCW-lamp energy .................... 1000\nSecond measurement CW-lamp energy . 0\nEmission side ..................... Above\nCW-Lamp Control ................... Stabilized Energy\nExcitation Aperture ............... Normal\n\n=============================\nPlate map of palte 1\nA | M   M   M   M   M   M   M   M   M   M   E   E   \nB | M   M   M   M   M   M   M   M   M   M   E   E   \nC | M   M   M   M   M   M   M   M   M   M   E   E   \nD | M   M   M   M   M   M   M   M   M   M   E   E   \nE | M   M   M   M   M   M   M   M   M   M   E   E   \nF | M   M   M   M   M   M   M   M   M   M   E   E   \nG | M   M   M   M   M   M   M   M   M   M   E   E   \nH | M   M   M   M   M   M   M   M   M   M   E   E   \n\n\n\nAssay ID:  ........................ 6529','  Photometry(  595nm_kk),  Prompt fluorometry(  EGFP_sulim)',''),(1031,71,'testset123123123123',NULL,31,'001426172400000','001444098962173','001444098962173','YES','Protocol name ..................... Haseong - EGFP/OD time\nProtocol number ................... N/A\nName of the plate type ............ Generic 8x12 size plate\nNumber of repeats ................. 1\nDelay between repeats ............. 900 s\nMeasurement height ................ 8.00 mm\nProtocol notes .................... \n\nName of the label ................. 595nm_kk\nLabel technology .................. Photometry\nCW-lamp filter name ............... 595/10\nCW-lamp filter slot ............... A4\nMeasurement time .................. 0.1 s\nAbsorbance Mode ................... Visible\nExcitation Aperture ............... Normal\n\nName of the label ................. EGFP_sulim\nLabel technology .................. Prompt fluorometry\nCW-lamp filter name ............... F485\nCW-lamp filter slot ............... A5\nEmission filter name .............. D535/40\nEmission filter slot .............. A3\nMeasurement time .................. 0.1 s\nEmission aperture ................. Normal\nCW-lamp energy .................... 1000\nSecond measurement CW-lamp energy . 0\nEmission side ..................... Above\nCW-Lamp Control ................... Stabilized Energy\nExcitation Aperture ............... Normal\n\n=============================\nPlate map of palte 1\nA | M   M   M   M   M   M   M   M   M   M   E   E   \nB | M   M   M   M   M   M   M   M   M   M   E   E   \nC | M   M   M   M   M   M   M   M   M   M   E   E   \nD | M   M   M   M   M   M   M   M   M   M   E   E   \nE | M   M   M   M   M   M   M   M   M   M   E   E   \nF | M   M   M   M   M   M   M   M   M   M   E   E   \nG | M   M   M   M   M   M   M   M   M   M   E   E   \nH | M   M   M   M   M   M   M   M   M   M   E   E   \n\n\n\nAssay ID:  ........................ 6529','  Photometry(  595nm_kk),  Prompt fluorometry(  EGFP_sulim)','Victor'),(1371,61,'testVictor1',NULL,31,'001426172400000','001445838563921','001445838563921','YES','Protocol name ..................... Haseong - EGFP/OD time\nProtocol number ................... N/A\nName of the plate type ............ Generic 8x12 size plate\nNumber of repeats ................. 1\nDelay between repeats ............. 900 s\nMeasurement height ................ 8.00 mm\nProtocol notes .................... \n\nName of the label ................. 595nm_kk\nLabel technology .................. Photometry\nCW-lamp filter name ............... 595/10\nCW-lamp filter slot ............... A4\nMeasurement time .................. 0.1 s\nAbsorbance Mode ................... Visible\nExcitation Aperture ............... Normal\n\nName of the label ................. EGFP_sulim\nLabel technology .................. Prompt fluorometry\nCW-lamp filter name ............... F485\nCW-lamp filter slot ............... A5\nEmission filter name .............. D535/40\nEmission filter slot .............. A3\nMeasurement time .................. 0.1 s\nEmission aperture ................. Normal\nCW-lamp energy .................... 1000\nSecond measurement CW-lamp energy . 0\nEmission side ..................... Above\nCW-Lamp Control ................... Stabilized Energy\nExcitation Aperture ............... Normal\n\n=============================\nPlate map of palte 1\nA | M   M   M   M   M   M   M   M   M   M   E   E   \nB | M   M   M   M   M   M   M   M   M   M   E   E   \nC | M   M   M   M   M   M   M   M   M   M   E   E   \nD | M   M   M   M   M   M   M   M   M   M   E   E   \nE | M   M   M   M   M   M   M   M   M   M   E   E   \nF | M   M   M   M   M   M   M   M   M   M   E   E   \nG | M   M   M   M   M   M   M   M   M   M   E   E   \nH | M   M   M   M   M   M   M   M   M   M   E   E   \n\n\n\nAssay ID:  ........................ 6529','  Photometry(  595nm_kk),  Prompt fluorometry(  EGFP_sulim)','Victor'),(1431,61,'testTecan12',NULL,31,'001442329200000','001445936285645','001445936285645','YES','infinite F50\nInstrument serial number: 1504005699\nPlate\nPlate Description: [GRE96ft] - Greiner 96 Flat Transparent\nPlate with Cover: Yes\nBarcode: No\n  Part of Plate\n  Range: A1:H12\n    Temperature\n    Mode: On\n    Temperature: 37.0 °C\n    Wait for Temperature\n    Minimum Temperature: 36.0 °C\n    Maximum Temperature: 38.0 °C\n    Kinetic Cycle\n    Number of Cycles: 150\n      Incubation\n      Incubation Time: 00:15:00\n      Incubation Action: Shaking\n      Duration: 420 sec\n      Mode: Orbital\n      Amplitude: 2 mm\n      Frequency: 280.8 rpm\n      Incubation Action: Wait (Timer)\n      Wait Time: 00:05:00\n      Incubation Action: Shaking\n      Duration: 60 sec\n      Mode: Linear\n      Amplitude: 1 mm\n      Frequency: 886.9 rpm\n      Incubation Action: Remaining Wait (Timer)\n      Measurement Wavelength: 600 nm\n      Measurement Bandwidth: 9 nm\n      Number of Reads: 10\n      Settle Time: 0 ms\n      Label: Label1\n      Excitation Wavelength: 488 nm\n      Excitation Bandwidth: 9 nm\n      Emission Wavelength: 515 nm\n      Emission Bandwidth: 20 nm\n      ReadingMode: Top\n      Lag Time: 0 ?s\n      Integration Time: 20 ?s\n      Number of Reads: 25\n      Settle Time: 0 ms\n      Gain: Manual\n      Gain Value: 54\n      Z-Position: Manual\n      Z-Position height: 19095 ?m\n      Label: Label2\nTotal kinetic run time: 1days 18h 9min 15s \n','      Absorbance( 600 nm)\n      Fluorescence Intensity( 488 nm)','Tecan'),(1441,61,'tecan1',NULL,31,'001442329200000','001445936732626','001445936732626','YES','infinite F50\nInstrument serial number: 1504005699\nPlate\nPlate Description: [GRE96ft] - Greiner 96 Flat Transparent\nPlate with Cover: Yes\nBarcode: No\n  Part of Plate\n  Range: A1:H12\n    Temperature\n    Mode: On\n    Temperature: 37.0 °C\n    Wait for Temperature\n    Minimum Temperature: 36.0 °C\n    Maximum Temperature: 38.0 °C\n    Kinetic Cycle\n    Number of Cycles: 150\n      Incubation\n      Incubation Time: 00:15:00\n      Incubation Action: Shaking\n      Duration: 420 sec\n      Mode: Orbital\n      Amplitude: 2 mm\n      Frequency: 280.8 rpm\n      Incubation Action: Wait (Timer)\n      Wait Time: 00:05:00\n      Incubation Action: Shaking\n      Duration: 60 sec\n      Mode: Linear\n      Amplitude: 1 mm\n      Frequency: 886.9 rpm\n      Incubation Action: Remaining Wait (Timer)\n      Measurement Wavelength: 600 nm\n      Measurement Bandwidth: 9 nm\n      Number of Reads: 10\n      Settle Time: 0 ms\n      Label: Label1\n      Excitation Wavelength: 488 nm\n      Excitation Bandwidth: 9 nm\n      Emission Wavelength: 515 nm\n      Emission Bandwidth: 20 nm\n      ReadingMode: Top\n      Lag Time: 0 ?s\n      Integration Time: 20 ?s\n      Number of Reads: 25\n      Settle Time: 0 ms\n      Gain: Manual\n      Gain Value: 54\n      Z-Position: Manual\n      Z-Position height: 19095 ?m\n      Label: Label2\nTotal kinetic run time: 1days 18h 9min 15s \n','      Absorbance( 600 nm)\n      Fluorescence Intensity( 488 nm)','Tecan');
/*!40000 ALTER TABLE `Experiment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExperimentAttachment`
--

DROP TABLE IF EXISTS `ExperimentAttachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentAttachment` (
  `experimentID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `attachmentID` bigint(20) NOT NULL COMMENT '첨부파일 ID',
  `type` varchar(15) NOT NULL DEFAULT 'output' COMMENT '첨부파일 종류 (결과파일, 기타...)',
  UNIQUE KEY `experimentID` (`experimentID`,`attachmentID`),
  KEY `ERA_REFS_ATTACHID` (`attachmentID`),
  CONSTRAINT `ERA_REFS_ATTACHID` FOREIGN KEY (`attachmentID`) REFERENCES `Attachment` (`attachmentID`),
  CONSTRAINT `ERA_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 결과 이미지 첨부파일 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExperimentAttachment`
--

LOCK TABLES `ExperimentAttachment` WRITE;
/*!40000 ALTER TABLE `ExperimentAttachment` DISABLE KEYS */;
INSERT INTO `ExperimentAttachment` VALUES (52,50,'input'),(53,52,'output'),(61,74,'output'),(781,413,'output'),(781,415,'input'),(781,431,'input'),(781,441,'input'),(798,414,'input'),(798,421,'input'),(798,432,'input'),(831,452,'output'),(931,462,'output'),(1031,472,'output'),(1371,632,'output'),(1431,682,'output'),(1441,692,'output');
/*!40000 ALTER TABLE `ExperimentAttachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExperimentDesigner`
--

DROP TABLE IF EXISTS `ExperimentDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentDesigner` (
  `experimentID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `userID` bigint(20) NOT NULL COMMENT '프로토콜 개발자 ID',
  UNIQUE KEY `experimentID` (`experimentID`,`userID`),
  KEY `ERD_REFS_USERID` (`userID`),
  CONSTRAINT `ERD_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`),
  CONSTRAINT `ERD_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExperimentDesigner`
--

LOCK TABLES `ExperimentDesigner` WRITE;
/*!40000 ALTER TABLE `ExperimentDesigner` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExperimentDesigner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExperimentInput`
--

DROP TABLE IF EXISTS `ExperimentInput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentInput` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험 결과 ID',
  `componentID` bigint(20) NOT NULL COMMENT 'DeviceComponent 테이블의 입력에 해당하는 부품 ID',
  `quantity` double DEFAULT '0' COMMENT '입력 수치',
  `unit` varchar(10) DEFAULT NULL COMMENT '수치 단위',
  UNIQUE KEY `experimentID` (`experimentID`,`componentID`),
  KEY `EXPRIN_REFS_COMPID` (`componentID`),
  CONSTRAINT `EXPRIN_REFS_COMPID` FOREIGN KEY (`componentID`) REFERENCES `DeviceComponent` (`componentID`),
  CONSTRAINT `EXPRIN_REFS_SIMULID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 입력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExperimentInput`
--

LOCK TABLES `ExperimentInput` WRITE;
/*!40000 ALTER TABLE `ExperimentInput` DISABLE KEYS */;
INSERT INTO `ExperimentInput` VALUES (52,112,1234,NULL),(53,112,123,NULL),(61,112,0,NULL);
/*!40000 ALTER TABLE `ExperimentInput` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExperimentOutput`
--

DROP TABLE IF EXISTS `ExperimentOutput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentOutput` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험 결과 ID',
  `name` varchar(128) NOT NULL COMMENT '출력정보 이름',
  `mean` double DEFAULT '0' COMMENT '평균 출력 값',
  `variance` double DEFAULT '0' COMMENT '분산',
  UNIQUE KEY `experimentID` (`experimentID`,`name`),
  KEY `EXPROut_NAME_IDX` (`name`),
  CONSTRAINT `EXPROUT_REFS_SIMULID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 출력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExperimentOutput`
--

LOCK TABLES `ExperimentOutput` WRITE;
/*!40000 ALTER TABLE `ExperimentOutput` DISABLE KEYS */;
INSERT INTO `ExperimentOutput` VALUES (52,'FL1-H',1.1062022566834,13.1917968107848),(52,'FL2-H',3.07835964340778,0.0826679017025325),(52,'FL3-H',4.17979337722617,0.167239729561361),(53,'FL1-H',1.1062022566834,13.1917968107848),(53,'FL2-H',3.07835964340778,0.0826679017025325),(53,'FL3-H',4.17979337722617,0.167239729561361),(53,'FSC-H',2.52478301387267,2.65623415665855),(53,'GREEN',5.16361152982303,3590.38149715057),(61,'FL1-H',1.1062022566834,13.1917968107848),(61,'FSC-H',2.52478301387267,2.65623415665855),(61,'SSC-H',5.16361152982303,3590.38149715057);
/*!40000 ALTER TABLE `ExperimentOutput` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExperimentProp`
--

DROP TABLE IF EXISTS `ExperimentProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentProp` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험결과 ID',
  `propName` varchar(64) NOT NULL COMMENT '실험결과 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '실험결과 속성 값',
  UNIQUE KEY `experimentID` (`experimentID`,`propName`),
  KEY `EXPRProp_PName_IDX` (`propName`),
  KEY `EXPRProp_PValue_IDX` (`propValue`),
  CONSTRAINT `ERP_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 속성 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExperimentProp`
--

LOCK TABLES `ExperimentProp` WRITE;
/*!40000 ALTER TABLE `ExperimentProp` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExperimentProp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExperimentReferences`
--

DROP TABLE IF EXISTS `ExperimentReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentReferences` (
  `experimentID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `experimentID` (`experimentID`,`articleID`),
  KEY `ERR_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `ERR_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `ERR_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 결과 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExperimentReferences`
--

LOCK TABLES `ExperimentReferences` WRITE;
/*!40000 ALTER TABLE `ExperimentReferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExperimentReferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Experiment_Input`
--

DROP TABLE IF EXISTS `Experiment_Input`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Experiment_Input` (
  `experimentID` bigint(20) NOT NULL COMMENT 'experimentID`',
  `no` varchar(5) NOT NULL COMMENT 'no',
  `name` varchar(15) DEFAULT NULL,
  `typeName` varchar(20) DEFAULT NULL,
  `value` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Experiment_Input`
--

LOCK TABLES `Experiment_Input` WRITE;
/*!40000 ALTER TABLE `Experiment_Input` DISABLE KEYS */;
INSERT INTO `Experiment_Input` VALUES (741,'0','RNA polymerase','protein oligomer','5.0'),(798,'0','RNA polymerase','protein oligomer','0.0'),(781,'0','RNA polymerase','protein oligomer','0.0'),(831,'1','RNA polymerase','protein oligomer','0.0'),(931,'1','RNA polymerase','protein oligomer','26.4'),(1031,'1','RNA polymerase','protein oligomer','0.0'),(1371,'1','RNA polymerase','protein oligomer','5.0'),(1431,'1','RNA polymerase','protein oligomer','10.0'),(1441,'1','RNA polymerase','protein oligomer','10.0');
/*!40000 ALTER TABLE `Experiment_Input` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IOUnit`
--

DROP TABLE IF EXISTS `IOUnit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOUnit` (
  `unitID` bigint(20) NOT NULL COMMENT '분자 ID',
  `serial` varchar(16) NOT NULL COMMENT '고유번호',
  `name` varchar(32) NOT NULL COMMENT '분자이름',
  `unitType` bigint(20) NOT NULL COMMENT '분자유형 ID',
  `unitValue` varchar(1000) DEFAULT NULL COMMENT '분자 량 혹은 서열',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) DEFAULT 'YES',
  PRIMARY KEY (`unitID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `IOUNIT_REFS_TYPE` (`unitType`),
  KEY `IOUNIT_REFS_USERID` (`userID`),
  KEY `IOUnit_Serial_IDX` (`serial`),
  KEY `IOUnit_Name_IDX` (`name`),
  CONSTRAINT `IOUNIT_REFS_TYPE` FOREIGN KEY (`unitType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `IOUNIT_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='분자 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IOUnit`
--

LOCK TABLES `IOUnit` WRITE;
/*!40000 ALTER TABLE `IOUnit` DISABLE KEYS */;
INSERT INTO `IOUnit` VALUES (21,'IO0021','phenol',14,NULL,'phenol',22,'001381711996830','001381711996830','YES'),(22,'IO0022','DMPR',13,NULL,'dmpR hexamer',22,'001381712106109','001403076409861','YES'),(23,'IO0023','RNA polymerase',13,NULL,'RNA polymerase',22,'001381712136630','001381712136630','YES'),(24,'IO0024','EGFP',13,NULL,'EGFP',22,'001381712151195','001381712151195','YES'),(31,'IO0031','Isoprene',14,NULL,'Isoprene, or 2-methyl-1,3-butadiene, is a common organic compound with the formula CH2=C(CH3)CH=CH2. It is a colorless volatile liquid. Isoprene is produced by many plants.\n\n-wiki-',22,'001402358013634','001402358013634','YES'),(32,'IO0032','Cellobiose ',14,NULL,'Cellobiose is a disaccharide with the formula [HOCH2CHO(CHOH)3]2O. Cellobiose consists of two glucose molecules linked by a Î²(1â†’4) bond. It can be hydrolyzed to glucose enzymatically or with acid.[1] Cellobiose has eight free alcohol (OH) groups, one acetal linkage and one hemiacetal linkage, which give rise to strong inter- and intra-molecular hydrogen bonds. It can be obtained by enzymatic or acidic hydrolysis of cellulose and cellulose rich materials such as cotton, jute, or paper.',22,'001402358122191','001402358122191','YES'),(33,'IO0033','caprolactam',14,NULL,'Caprolactam (CPL) is an organic compound with the formula (CH2)5C(O)NH. This colourless solid is a lactam or a cyclic amide of caproic acid. Approximately 4.5 billion kilograms are produced annually. Caprolactam is the precursor to Nylon 6, a widely used synthetic polymer.[1]\n\n',22,'001402358195437','001406523033742','YES'),(34,'IO0034','LACI',13,NULL,'LacI protein',22,'001403076397451','001403076397451','YES'),(41,'IO0041','test',12,'test','test',31,'001431479634121','001431479634121','NO'),(71,'IO0071','DMPR',13,NULL,'dmpR hexamer',31,'001435816776233','001435816776233','YES');
/*!40000 ALTER TABLE `IOUnit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IOUnitReferences`
--

DROP TABLE IF EXISTS `IOUnitReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOUnitReferences` (
  `unitID` bigint(20) NOT NULL COMMENT '분자 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `unitID` (`unitID`,`articleID`),
  KEY `IOUNITREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `IOUNITREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `IOUNITREFERENCES_REFS_IOUNITID` FOREIGN KEY (`unitID`) REFERENCES `IOUnit` (`unitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='분자의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IOUnitReferences`
--

LOCK TABLES `IOUnitReferences` WRITE;
/*!40000 ALTER TABLE `IOUnitReferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `IOUnitReferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Link`
--

DROP TABLE IF EXISTS `Link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Link` (
  `linkID` bigint(20) NOT NULL COMMENT '링크 ID',
  `experimentID` bigint(20) NOT NULL COMMENT 'experimentID`',
  `loginID` bigint(20) NOT NULL COMMENT 'link를 수행한 ID',
  PRIMARY KEY (`linkID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Link`
--

LOCK TABLES `Link` WRITE;
/*!40000 ALTER TABLE `Link` DISABLE KEYS */;
INSERT INTO `Link` VALUES (101,52,31),(102,53,31),(103,61,31),(104,52,41);
/*!40000 ALTER TABLE `Link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Module`
--

DROP TABLE IF EXISTS `Module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Module` (
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `serial` varchar(16) NOT NULL COMMENT '고유번호',
  `name` varchar(64) NOT NULL COMMENT '이름이 없는 경우 고유번호 사용',
  `moduleType` bigint(20) NOT NULL COMMENT '모듈 유형 ID',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`moduleID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `MODULE_REFS_TYPE` (`moduleType`),
  KEY `MODULE_REFS_USERID` (`userID`),
  KEY `Module_Serial_IDX` (`serial`),
  KEY `Module_Name_IDX` (`name`),
  CONSTRAINT `MODULE_REFS_TYPE` FOREIGN KEY (`moduleType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `MODULE_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module(=Unit) 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Module`
--

LOCK TABLES `Module` WRITE;
/*!40000 ALTER TABLE `Module` DISABLE KEYS */;
INSERT INTO `Module` VALUES (21,'MOD021','phenol sensor',65,'sensing phenol',22,'001381497019150','001406680190948','YES'),(31,'MOD031','EGFP reporter',16,'EGFP reporter',22,'001381711900267','001406647563488','YES'),(71,'MOD071','teata',15,'testata',22,'001406702463885','001406702463885','YES'),(82,'MOD082','test',15,'test',31,'001433296924892','001433296924892','YES'),(91,'MOD091','test2',15,'test2',31,'001433297455541','001433297455541','YES'),(101,'MOD101','test15',16,'test',31,'001433751373707','001433751373707','NO'),(111,'MOD111','test20',16,'test',31,'001433810145823','001433810145823','NO'),(131,'MOD131','ModuleTest',15,'test',31,'001444091552381','001444091552381','YES');
/*!40000 ALTER TABLE `Module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModuleDesigner`
--

DROP TABLE IF EXISTS `ModuleDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleDesigner` (
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `userID` bigint(20) NOT NULL COMMENT '모듈개발자 ID',
  UNIQUE KEY `moduleID` (`moduleID`,`userID`),
  KEY `MODULEDESIGNER_REFS_USERID` (`userID`),
  CONSTRAINT `MODULEDESIGNER_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`),
  CONSTRAINT `MODULEDESIGNER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModuleDesigner`
--

LOCK TABLES `ModuleDesigner` WRITE;
/*!40000 ALTER TABLE `ModuleDesigner` DISABLE KEYS */;
/*!40000 ALTER TABLE `ModuleDesigner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModuleIOUnit`
--

DROP TABLE IF EXISTS `ModuleIOUnit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleIOUnit` (
  `moduleUnitID` bigint(20) NOT NULL COMMENT '모듈의 input/output IOUnit ID',
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `unitID` bigint(20) NOT NULL COMMENT 'IOUnit ID',
  `modulePartID` bigint(20) NOT NULL COMMENT 'IOUnit 의 대상이 되는 ModulePart ID',
  `ioType` enum('Input','Output','Both') NOT NULL DEFAULT 'Both' COMMENT 'input/output 유형',
  `positionX` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 X 좌표',
  `positionY` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 Y 좌표',
  `width` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 넓이',
  `height` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 높이',
  PRIMARY KEY (`moduleUnitID`),
  KEY `MODULEUNIT_REFS_MODULEID` (`moduleID`),
  KEY `MODULEUNIT_REFS_IOUNITID` (`unitID`),
  KEY `MODULEUNIT_REFS_MODULEPARTID` (`modulePartID`),
  CONSTRAINT `MODULEUNIT_REFS_IOUNITID` FOREIGN KEY (`unitID`) REFERENCES `IOUnit` (`unitID`),
  CONSTRAINT `MODULEUNIT_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`),
  CONSTRAINT `MODULEUNIT_REFS_MODULEPARTID` FOREIGN KEY (`modulePartID`) REFERENCES `ModulePart` (`modulePartID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈의 input/output 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModuleIOUnit`
--

LOCK TABLES `ModuleIOUnit` WRITE;
/*!40000 ALTER TABLE `ModuleIOUnit` DISABLE KEYS */;
INSERT INTO `ModuleIOUnit` VALUES (12,21,22,42,'Output',219,-112,30,30),(13,31,22,51,'Input',-201,-87,30,30),(15,21,23,41,'Input',-225,-94,30,30),(51,31,22,53,'Input',64,-78,30,30),(61,71,22,92,'Input',162,-78,30,30),(62,71,21,91,'Input',-246,-97,30,30);
/*!40000 ALTER TABLE `ModuleIOUnit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModulePart`
--

DROP TABLE IF EXISTS `ModulePart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModulePart` (
  `modulePartID` bigint(20) NOT NULL COMMENT '모듈의 파트 ID',
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `seq` int(11) NOT NULL DEFAULT '1' COMMENT '부품의 순서',
  PRIMARY KEY (`modulePartID`),
  UNIQUE KEY `moduleID` (`moduleID`,`partID`,`seq`),
  KEY `MP_REFS_PARTID` (`partID`),
  CONSTRAINT `MP_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`),
  CONSTRAINT `MP_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈의 파트목록 관리테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModulePart`
--

LOCK TABLES `ModulePart` WRITE;
/*!40000 ALTER TABLE `ModulePart` DISABLE KEYS */;
INSERT INTO `ModulePart` VALUES (43,21,15,2),(42,21,33,1),(41,21,50,0),(89,21,106,3),(51,31,51,0),(53,31,53,2),(54,31,61,3),(88,31,106,4),(93,71,38,2),(92,71,45,1),(91,71,48,0),(102,82,128,0),(103,82,129,1),(111,91,133,0),(112,91,134,1),(121,101,48,0),(122,101,50,1),(123,101,59,2),(131,131,48,0),(132,131,50,1);
/*!40000 ALTER TABLE `ModulePart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModuleReferences`
--

DROP TABLE IF EXISTS `ModuleReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleReferences` (
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `moduleID` (`moduleID`,`articleID`),
  KEY `MODULEREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `MODULEREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `MODULEREFERENCES_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModuleReferences`
--

LOCK TABLES `ModuleReferences` WRITE;
/*!40000 ALTER TABLE `ModuleReferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `ModuleReferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Part`
--

DROP TABLE IF EXISTS `Part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Part` (
  `partID` bigint(20) NOT NULL COMMENT '부품ID',
  `serial` varchar(16) NOT NULL COMMENT '부품 고유번호',
  `name` varchar(64) NOT NULL COMMENT '부붐 이름',
  `partType` bigint(20) NOT NULL COMMENT '부품 타입 ID',
  `sequence` text COMMENT '서열정보',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) DEFAULT 'YES',
  `direction` varchar(10) NOT NULL DEFAULT 'FOWARD',
  PRIMARY KEY (`partID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `PART_REFS_TYPE` (`partType`),
  KEY `PART_REFS_USERID` (`userID`),
  KEY `Part_Serial_IDX` (`serial`),
  KEY `Part_Name_IDX` (`name`),
  CONSTRAINT `PART_REFS_TYPE` FOREIGN KEY (`partType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `PART_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Part`
--

LOCK TABLES `Part` WRITE;
/*!40000 ALTER TABLE `Part` DISABLE KEYS */;
INSERT INTO `Part` VALUES (15,'PART015','Tl3 terminator',11,'aatggcgatg acgcatcctc acgataatat ccgggtaggc gcaatcactt tcgtctactc cgttacaaag cgaggctggg tatttcccgg cctttctgtt atccgaaatc cactgaaagc acagcggctg gctgaggaga taaataataa acgaggggct gtatgcacaa agcatcttct gttgagttaa gaacgagtat cgagatggca catagccttg ctcaaattgg aatcaggttt gtgccaatac cagtagaaac agacgaagaa\n','Tl3 terminator',1,'001377555275774','001381496196128','YES','FOWARD'),(18,'PART018','rrnBT1T2',11,'Ctgttttggc ggatgagaga agattttcag cctgatacag attaaatcag aacgcagaag cggtctgata aaacagaatt tgcctggcgg cagtagcgcg gtggtcccac ctgaccccat gccgaactca gaagtgaaac gccgtagcgc cgatggtagt gtggggtctc cccatgcgag agtagggaac tgccaggcat caaataaaac gaaaggctca gtcgaaagac tgggcctttc gttttatctg ttgtttgtcg gtgaacgctc tcctgagtag gacaaatccg ccgggagcgg atttgaacgt tgcgaagcaa cggcccggag ggtggcgggc aggacgcccg ccataaactg ccaggcatca aattaagcag aaggccatcc tgacggatgg cctttttgcg tttctacaaa ctctt\n','rrnBT1T2 terminator\n',1,'001377555626788','001381496206221','YES','FOWARD'),(33,'PART033','dmpR(WT)',61,'atgccgatca agtacaagcc tgaaatccag cactccgatt tcaaggacct gaccaacctg atccacttcc agagcacgga Aggcaagatc ggcttggcg aacaacgcat gctgttgctg caggtttcag caatggccag ctttcgccgg gaaatggtca Ataccctggg catcgaacgc gccaagggct tcttcctgcg ccagggttac cagtccggcc tgaaggatgc cgaactggcc aggaagctta gaccgaatgc cagcgagtac gacatgttcc tcgccggccc gcagctgcat tcgctcaagg gtctggtcaa Ggtccgcccc accgaggtcg atatcgacaa ggaatgcggg cgcttctatg ccgagatgga gtggatcgac tccttcgagg\ntggaaatctg ccagaccgac ctggggcaga tgcaagaccc ggtgtgctgg actctgctcg gctacgcctg cgcctattcc Tcggcgttca tgggccggga aatcatcttc aaggaagtca gctgccgcgg ctgcggcggc gacaagtgcc gggtcattgg Caagccggcc gaagagtggg acgacgttgc cagcttcaaa cagtatttca agaacgaccc catcatcgag gaactctacg agttgcaatc gcaactggtg tcgctgcgta ccaacctcga caaacaggaa ggccagtact acggcatcgg tcagaccccg Gcctaccaga ccgtgcgcaa tatgatggac aaggccgcac agggcaaagt ctcggtgctg ctgcttggcg agaccggggt\nCggcaaggag gtcatcgcgc gtagcgtgca cctgcgcagc aaacgcgccg ccgagccctt tgtcgcggtg aactgtgcgg Cgatcccgcc ggacctgatc gagtccgaat tgttcggcgt ggaaaaaggc gccttcaccg gcgccaccca gtcacgcatg Ggccgcttcg agcgggccga caagggcacc atcttccttg acgaggtgat cgaactcagc ccgcgcgctc aggccagttt Gctgcgcgtg ctgcaagaag gcgagctgga gcgagttggc gacaaccgca cgcgcaagat cgacgtaagg gttatcgcag Ccacccacga ggacctggcc gaagcggtca aggccgggcg ttttcgcgcc gacctgtact accggctgaa cgttttcccg\nGtggcgatcc cggcgttgcg cgaacgccgc gaggacattc cactgctggt tgagcacttc cttcagcgct tccaccagga Gtacggcaag agaaccctcg gcctttcaga caaagccctg gaggcctgcc tgcattacag ttggccgggc aatatccgtg Agctggagaa cgtcatcgag cgcggcatca tcctcaccga tccgaacgaa agcatcagcg tgcaggcgct gttcccacgg Gcgccggaag agccgcagac cgccagcgag cgggtgtcgt cggacggcgt gctgattcag ccaggcaatg gccagggcag Ttggatcagc cagttgttga gcagcggcct gagcctcgac gagatcgagg aaagcctgat gcgcaaagcc atgcaacagg\nCcaaccaaaa cgtctccggt gccgcgcgct tgctcggcct aagccgaccg gcactggcct atcggctgaa gaaaatcggc Atcgaaggct ag\n','Source: Pseudomonas putida KCTC1452\n1692bp, wild-type\ndifferent from shingler\'s dmpR.\n',22,'001378108154712','001381496011296','YES','FOWARD'),(38,'PART038','rrnBT terminator',11,'CTGTTTTGGCGGATGAGAGAAGATTTTCAGCCTGATACAGATTAAATCAGAACGCAGAAGCGGTCTGATAAAACAGAATTTGCCTGGCGGCAGTAGCGCGGTGGTCCCACCTGACCCCATGCCGAACTCAGAAGTGAAACGCCGTAGCGCCGATGGTAGTGTGGGGTCTCCCCATGCGAGAGTAGGGAACTGCCAGGCATCAAATAAAACGAAAGGCTCAGTCGAAAGACTGGGCCTTTCGTTTTATCTGTTGTTTGTCGGTGAACGCTCTCCTGAGTAGGACAAATCCGCCGGGAGCGGATTTGAACGTTGCGAAGCAACGGCCCGGAGGGTGGCGGGCAGGACGCCCGCCATAAACTGCCAGGCATCAAATTAAGCAGAAGGCCATCCTGACGGATGGCCTTTTTGCGTTTCTACAAACTCTT',NULL,22,'001378108902981','001378108902981','YES','FOWARD'),(41,'PART041','dmpR(E135K)',61,'atgccgatca agtacaagcc tgaaatccag cactccgatt tcaaggacct gaccaacctg atccacttcc agagcacgga Aggcaagatc tggcttggcg aacaacgcat gctgttgctg caggtttcag caatggccag ctttcgccgg gaaatggtca Ataccctggg catcgaacgc gccaagggct tcttcctgcg ccagggttac cagtccggcc tgaaggatgc cgaactggcc Aggaagctta gaccgaatgc cagcgagtac gacatgttcc tcgccggccc gcagctgcat tcgctcaagg gtctggtcaa Ggtccgcccc accgaggtcg atatcgacaa ggaatgcggg cgcttctatg ccgagatgga gtggatcgac tccttcgagg\nTgaaaatctg ccagaccgac ctggggcaga tgcaagaccc ggtgtgctgg actctgctcg gctacgcctg cgcctattcc Tcggcgttca tgggccggga aatcatcttc aaggaagtca gctgccgcgg ctgcggcggc gacaagtgcc gggtcattgg Caagccggcc gaagagtggg acgacgttgc cagcttcaaa cagtatttca agaacgaccc catcatcgag gaactctacg Agttgcaatc gcaactggtg tcgctgcgta ccaacctcga caaacaggaa ggccagtact acggcatcgg tcagaccccg Gcctaccaga ccgtgcgcaa tatgatggac aaggccgcac agggcaaagt ctcggtgctg ctgcttggcg agaccggggt\nCggcaaggag gtcatcgcgc gtagcgtgca cctgcgcagc aaacgcgccg ccgagccctt tgtcgcggtg aactgtgcgg Cgatcccgcc ggacctgatc gagtccgaat tgttcggcgt ggaaaaaggc gccttcaccg gcgccaccca gtcacgcatg Ggccgcttcg agcgggccga caagggcacc atcttccttg acgaggtgat cgaactcagc ccgcgcgctc aggccagttt Gctgcgcgtg ctgcaagaag gcgagctgga gcgagttggc gacaaccgca cgcgcaagat cgacgtaagg gttatcgcag Ccacccacga ggacctggcc gaagcggtca aggccgggcg ttttcgcgcc gacctgtact accggctgaa cgttttcccg\nGtggcgatcc cggcgttgcg cgaacgccgc gaggacattc cactgctggt tgagcacttc cttcagcgct tccaccagga Gtacggcaag agaaccctcg gcctttcaga caaagccctg gaggcctgcc tgcattacag ttggccgggc aatatccgtg Agctggagaa cgtcatcgag cgcggcatca tcctcaccga tccgaacgaa agcatcagcg tgcaggcgct gttcccacgg Gcgccggaag agccgcagac cgccagcgag cgggtgtcgt cggacggcgt gctgattcag ccaggcaatg gccagggcag Ttggatcagc cagttgttga gcagcggcct gagcctcgac gagatcgagg aaagcctgat gcgcaaagcc atgcaacagg\nCcaaccaaaa cgtctccggt gccgcgcgct tgctcggcct aagccgaccg gcactggcct atcggctgaa gaaaatcggc Atcgaaggct ag\n','Source: Pseudomonas putida KCTC1452\nDifferent from shingler\'s dmpR.',22,'001381480020836','001381495997243','YES','FOWARD'),(43,'PART043','xylR',61,'Atgtcgctta catacaaacc caagatgcag catgaggata tgcaagacct tagcagccag atccgtttcg ttgccgccga aggcaagatc tggttgggag agcagcgcat gctcgtaatg cagctatcta cgctggccag cttccgtcgc gaaattatca gcttgatcgg cgtcgagcgg gccaagggtt tcttcctgcg gttgggctat cagtccggcc tgatggatgc cgagctggca cgcaagctgc ggccggccat gcgcgaggag gaggtgttcc tggctgggcc tcaattgtat gcgctcaagg ggatggtcaa agtacgcttg ctgacaatgg atatcgccat ccgggacgga cgtttcaacg tggaggccga gtggattgat tcctttgaag \nTggatatctg ccgaactgag ctgggcctga tgaatgagcc cgtctgctgg acggtgctag gctatgctag cggctatggt tcggcattca tgggccgcag aatcattttc caggaaacta gctgtcgcgg gtgcggtgac gataaatgcc ttatcgtcgg caagaccgca gaagagtggg gcgatgtcag cagtttcgaa gcctacttca aaagcgaccc gatcgtagac gagcgctacg agctgcagac ccaggttgcc aacctgcgca accgcctgaa gcagtacgat gggcagtatt acggcattgg ccattcgcca gcctacaagc gcatctgtga gaccatcgac aaggctgcac gcggcagggt ttcggtcctg ctactgggtg agactggggt gggcaaggag gtaatcgcgc gcagcgtgca tttgcgcagt gagcgcgcag agcaaccctt cgtcgcggtg aactgtgcgg caattccgcc ggatctgatc gagtcggaac tgtttggtgt cgataagggc gcctatacgg gcgcggtcaa tgcacgcgct ggacgttttg aacgggccaa cggcggcacc atctttcttg atgaggtgat cgaattgacg ccgagggccc aggccaccct gctacgggta ttgcaggaag gagagctaga gcgggtcggc ggcgaccgca cgcgaaaggt cgacgtgagg ttaatcaccg caacaaacga gaacctggaa gaggcggtca agatggggcg ctttcgcgca gacctgttct ttcggctgaa tgtttttccc \nGtgcatatcc cgccgttgcg cgagcgcgtg gaagatatcc cgctgctggt cgagcatttt cttagaaggc accataagga atacggtaag aagactcttg gcctgtctga tcgagcgatg gaggcctgcc tccactacca atggccaggc aatatccgcg agctggagaa cgcccttgag cgcggggtga ttcttaccga gagcaacgaa agcatcaatg tcgagtcgct gttcccgggg ttggcgacgg ctaccgaagg cgacaggcta tcgagcgagg gccggttgga ggaggagtcc ggtgacagtt ggtttaggca aattatcgac cagggcgtca gcctcgaaga tctcgaagcg ggtttaatgc gcacggccat ggaccgttgt gggcagaata tctcacaggc ggcgcggttg ctgggattga cccgcccggc aatggcctat cgacttaaga agcttgaccc cagcttatct gtgaaagcaa tgggccga\n','Source : Pseudomonas putida\n',22,'001381480159095','001381496035887','YES','FOWARD'),(44,'PART044','nitR',61,'Atggagcaat ggtccaccac ggcaattccc gctacccggc gcgcacccta ttggatggag gccgtcaaca aggcctatgt gcaactggaa tgcgccgttc cctcccgctc cagcgcccct ttcttcggag ccattacgcg tcgtgagctg gcggccgtca gcctctccca tatcacctcg accacacaga cggtactgcg cacgcctttg caaatttcca gagcatccga ggatattttt ctgctcagca ttcaggtggc cggagcggga aaactggttc aggacgaaaa aaccgctcac ctgaaacctg gtgacctggc cctgtacgac tccacccgcc cctatcagct cttgtttgac catgacttcg agcagtatgt gctttctttg cccggctcca tcttgcgcaa gcgtttacac aatgccgagg acatgacggc ctgcaaaatc accagcgccc aatccggcac ggcacgtttg ctctcccata tggtcagcga actgatggac tgcccaccct ccggtggccc aattgtagat ttgtcgcttg ccgacagcct ggtcggtatt ctggtcgctg cgctggcaga aaatctgggc agcctgcctt taagcgacga tacggggtct gtccgacgcg accgcatcaa agcctatgtg ctggaaaatc tgcgcgaccc ggagctgaat ataggcaaga ttgccaaacg tttgagcctg acggccagca ccgtgcatcg cgcctgggag ggcgaagccg attcactgac aaactggatt tggtccatgc gcctgaaagg cgctgaacag gatttgcgcc ggctggccca ccacaacaag accatcacgg aaattgctta ccactggggg ttcagcagct ctgcccattt cagccgggca tttcggcagc actttggtgt gccgcccaaa gaggcccgcg aaagtatgcg ggccctggtt caatcagact cgatgcctgt c\n','Source : Alkaligenes faecalis\n',22,'001381480221830','001381496043369','YES','FOWARD'),(45,'PART045','styR',8,'Atgcctggag cctggaacgt catgagcgct accgatctgc ctggcgactc cgttcgaagt gttgggaatg tgattctcaa tccggacgac agcccccaga cccacagtga aaaaatggcc cggatcattc tggaccgcat gtatcacttc gccgggcttt tggaccgtga tggcaccatc ctggaaatca accttccggc gcttgaaggt gcaggcgtgc ggatcgagga tattcgtggc acgccatttt gggaggctcg ctggttagca gtatcggaag aaagcaaaga gcttcagcat cagctcgttc agcgagctgc cgccggtgag tttattcgct gtgacctgga ggtctacggg gaggggagcg gcgagcagac catcgttgtg gactactcgc tcaccccgtt gcgagataac cacggcgagg tggccttcct gctggcggaa gggcggaaca tcaccagcaa gaaaaagtac gaacaggaga ttgcccggaa aaacgctgag ctggagaaac ttgtcgagca gatccgaatg ctggatgagc aaaagaatcg gttcttctcc aacctgagtc atgagttacg gaccccgtta tcgctcatcc ttggccccgt ggatgagatg cttgtgagca gtgagttttc ggagcatcag cacactaacc tcgcgagcat cagaaggaac gcagtcacct tgcttcggca tgtgaacgag ttgctcgatc tcgcaaaaat cgatgctggt aaacttcagt tggcctacga gctcatcgat attacgggct tggtcaagga gatcacggcc cacttcgagg cgcatgccaa acagcgccga atacactgtg cagtactttc gccagggccg attctgcttg aggccgatcc agaaaagatc agtcatgtcg ttttcaacct ggtggccaat gccttcaacg ccacgccgga cggagggcgc atcagttgcc acgttgagat cggggaagga aatcgatgtc tactgacggt tagcgatact ggccccggag ttcctcccga tatgcgtcag cgaatcttcg agcgattcca gcaaggtgtg gaggagcacg gagaggcacg ggccgggagc ggattaggcc tggcaatcgt gaaggagttc atcgagctcc atggcggcac agttaccgtc ggagaggcac cctcaagcgg tgccattttc caagtagagc tgcccgcagc ggctccgccg caggtcttgg tgcggaaggg ctcggtccgt gagcagactt tttctcctga attgccttca gggggcgacg tatccctcct cccagggcgt ggccttgttt ctgatggccg aacagatctc ccccgcgttc tcgtggtcga agacaacgag gaaatgctgc acctcatcgc caggacactt tccaacgagt tcagtgttga gtgcgccagc aacggtaagc aaggctttgc ttacatgctc gccaatcccc cggatctggt gatcacagat ctcatgctgc cgggcatgag cggggaaaag ttgatccgcc gtatgcgcga agagggcgcg ttgactcaga tccctgtatt ggttctgtcg gctcgcgctg acgaggagct gcgcatgacg ctcttggcga ctctggttca ggactacgtg accaagccat tcttcattcc cgaactgttg agccgggtcc gtaatctggt gatgactcgc agggcaagac ttgccctgca ggatgaactg aaaactcaca atgcagactt cgttcaactg gcacgcgagt tgatttcagg ccggcgagcc attcaacgaa gccttgaagc tcaacagaaa tcggagctgc gctggcgggc catccatgag aactccgcgg tgggaatcgc agtggtcgac ctgcagtggc ggttcgtaaa cgccaaccct gcattttgca ggatgctcgg ctacacccag gaggaactac taggtcattc agttctggag cacacccacc ctgacgaccg aaacatcact gatcagcgcc tgcatcacct tctggatggc cgattgcgga cctaccacca ccagaagcgc ttcctgcaca aagatggcca cagcctatgg acacgctcga gcgtatcggt gattccaggc agtggcgaca ctccaccgct gatgatcgga gttgtcgaag acatcgatgc acaaaaacgt gctgagcacg aattggagcg ggctcgctcg gagttggccc gtgtcatgcg agtcactgcg atgggtgagc tggtggcgtc gatcacccat gaactgaatc agccgttggc ggcgatggtc gccaacagtc atgcctgtcg gcgatggctg aatagtagcc ctccgaattt gaaggagggg atcgccagtg tcgaggctgt agtccgtgat agccagcggg ctgcggaagt ggtccttcgg ctgcggatgt tcatgaggcg tggagagacc cagcatgagc ctctgaacct ctcaggcgtc gtggaggagg ttctcggcta tgtacgagag tctctggtga tgcagggcat ctccctggaa acgacgctac ccactgatct accgatggtg ctggcggata gagtccagtt gcaacaggtt gtactcaatc tggttctgaa cgctattgag gcgattcagg cggcgtcgcc tagtgttccc cggcttacgt tgcgcatttg ccgcagcccg gataatggtc ctctccgact ggaggtggag gacaatggct gtggagtacc ttcgtcgcaa acggaacgga tcttcgaacc gttttatacg accaagagcc atggaatggg gatgggattg gccatctgca ggacgatttt agaggcccat ggtggacagt taaatctcct ccctccatct gacaactgct cagcttctgg aagcgtcttc caggtcatcc tgcctactga ccaaggaact ctact','Source : Pseudomonas fluorescence\n',22,'001381480350262','001381480350262','YES','FOWARD'),(46,'PART046','styS',8,'atgac cacaaagccc acagtattcg tagtcgacga cgacttgtcg gttcgagagg ggctgcgaaa tcttctgagg tcagcggggt tcgaggtaga aacctttgat tgtgcttcga ctttcctaga acaccgacgt cccgagcagc atggctgcct ggttctggac atgcgtatgc cagggatgag cggaatagag cttcaggagc agctgacagc gatcagcgac ggtataccta tcgtgttcat tacggcacac ggtgacatcc ccatgaccgt gcgagccatg aaggccggag cgatagagtt ccttcccaag ccatttgagg agcaggcact gctggatgct atcgaacagg ggcttcaatt ggatgctgag cggcgccagg cacgggcaac tcaataccag ctagaacagc tgttttcatc cttaaccgtg cgtgagcaac aggtactgca actgacgatc cgagggttga tgaataagca gattgcaggc gagctaggta ttgccgaggt aacggtaaag gtccatcgcc acaacatcat gcagaagctg aatgtcagat ctctggcgaa tcttgttcat ctggttgaga aatatgaatc gtttgaaagg ggcgactga','Source : Pseudomonas fluorescence\n',22,'001381480382013','001381480382013','YES','FOWARD'),(47,'PART047','sigma54 dependent -12',6,'TTGC ','Parts registry BBa_J23100\nConstitutive promoter for E. coli\nÏƒ70-dependent \nw/o RBS\n',22,'001381493536305','001397714376734','YES','FOWARD'),(48,'PART048','TRC',6,'atgcTGTTTGACAGCTTATCATCGATAAGCTTTAATGCGGTAGTTTATCACAGTTAAATTGCTAACGCAGTCAGGCAC\nCGTGTATGAAATCTAACAATGCGCTCATCGTCATCCTCGGCACCGTCACCCTGGATGCTGTAGGCATAGGCTTGGTTATG\nCCGGTACTGCCGGGCCTCTTGCGGGATATCCGGATATAGTTCCTCCTTTCAGCAAAAAACCCCTCAAGACCCGTTTAGAG\nGCCCCAAGGGGTTATGCTAGTTATTGCTCAGCGGTGGCAGCAGCCAACTCAGCTTCCTTTCGGGCTTTGTTAGCAGCCGG\nATCAAAGTGCCCCGGAGGATGAGATTTTCTTAAAGCGGTTGGCTGCTGAGACGGCTATGAAATTCTTTTTCCATCGTCGC\nTTGGTACCATTCCCACGTGGATACATGGATCTACCAAGTGATGCGCGTCTAAGGGATCCGACCCATTTGCTGTCCACCAG\nTCATGCTAGCCATGGTATATCTCCTTCTTAAAGTTAAACAAAATTATTTCTAGAGGGGAATTGTTATCCGCTCACAATTC\nCCCTATAGTGAGTCGTATTAATTTCGCGGGATCGAGATCTCGATCCTCTACGCCGGACGCATCGTGGCCGGCATCACCGG\nCGCCACAGGTGCGGTTGCTGGCGCCTATATCGCCGACATCACCGATGGGGAAGATCGGGCTCGCCACTTCGGGCTCATGA\nGCGCTTGTTTCGGCGTGGGTATGGTGGCAGGCCCCGTGGCCGGGGGACTGTTGGGCGCCATCTCCTTGCATGCACCATTC\nCTTGCGGCGGCGGTGCTCAACGGCCTCAACCTACTACTGGGCTGCTTCCTAATGCAGGAGTCGCATAAGGGAGAGCGTCG\nAGATCCCGGACACCATCGAATGGCGCAAAACCTTTCGCGGTATGGCATGATAGCGCCCGGAAGAGAGTCAATTCAGGGTG\nGTGAATGTGAAACCAGTAACGTTATACGATGTCGCAGAGTATGCCGGTGTCTCTTATCAGACCGTTTCCCGCGTGGTGAA\nCCAGGCCAGCCACGTTTCTGCGAAAACGCGGGAAAAAGTGGAAGCGGCGATGGCGGAGCTGAATTACATTCCCAACCGCG\nTGGCACAACAACTGGCGGGCAAACAGTCGTTGCTGATTGGCGTTGCCACCTCCAGTCTGGCCCTGCACGCGCCGTCGCAA\nATTGTCGCGGCGATTAAATCTCGCGCCGATCAACTGGGTGCCAGCGTGGTGGTGTCGATGGTAGAACGAAGCGGCGTCGA\nAGCCTGTAAAGCGGCGGTGCACAATCTTCTCGCGCAACGCGTCAGTGGGCTGATCATTAACTATCCGCTGGATGACCAGG\nATGCCATTGCTGTGGAAGCTGCCTGCACTAATGTTCCGGCGTTATTTCTTGATGTCTCTGACCAGACACCCATCAACAGT\nATTATTTTCTCCCATGAAGACGGTACGCGACTGGGCGTGGAGCATCTGGTCGCATTGGGTCACCAGCAAATCGCGCTGTT\nAGCGGGCCCATTAAGTTCTGTCTCGGCGCGTCTGCGTCTGGCTGGCTGGCATAAATATCTCACTCGCAATCAAATTCAGC\nCGATAGCGGAACGGGAAGGCGACTGGAGTGCCATGTCCGGTTTTCAACAAACCATGCAAATGCTGAATGAGGGCATCGTT\nCCCACTGCGATGCTGGTTGCCAACGATCAGATGGCGCTGGGCGCAATGCGCGCCATTACCGAGTCCGGGCTGCGCGTTGG\nTGCGGATATCTCGGTAGTGGGATACGACGATACCGAAGACAGCTCATGTTATATCCCGCCGTTAACCACCATCAAACAGG\nATTTTCGCCTGCTGGGGCAAACCAGCGTGGACCGCTTGCTGCAACTCTCTCAGGGCCAGGCGGTGAAGGGCAATCAGCTG\nTTGCCCGTCTCACTGGTGAAAAGAAAAACCACCCTGGCGCCCAATACGCAAACCGCCTCTCCCCGCGCGTTGGCCGATTC\nATTAATGCAGCTGGCACGACAGGTTTCCCGACTGGAAAGCGGGCAGTGAGCGCAACGCAATTAATGTAAGTTAGCTCACT\nCATTAGGCACCGGGATCTCGACCGATGCCCTTGAGAGCCTTCAACCCAGTCAGCTCCTTCCGGTGGGCGCGGGGCATGAC\nTATCGTCGCCGCACTTATGACTGTCTTCTTTATCATGCAACTCGTAGGACAGGTGCCGGCAGCGCTCTGGGTCATTTTCG\nGCGAGGACCGCTTTCGCTGGAGCGCGACGATGATCGGCCTGTCGCTTGCGGTATTCGGAATCTTGCACGCCCTCGCTCAA\nGCCTTCGTCACTGGTCCCGCCACCAAACGTTTCGGCGAGAAGCAGGCCATTATCGCCGGCATGGCGGCCGACGCGCTGGG\nCTACGTCTTGCTGGCGTTCGCGACGCGAGGCTGGATGGCCTTCCCCATTATGATTCTTCTCGCTTCCGGCGGCATCGGGA\nTGCCCGCGTTGCAGGCCATGCTGTCCAGGCAGGTAGATGACGACCATCAGGGACAGCTTCAAGGATCGCTCGCGGCTCTT\nACCAGCCTAACTTCGATCACTGGACCGCTGATCGTCACGGCGATTTATGCCGCCTCGGCGAGCACATGGAACGGGTTGGC\nATGGATTGTAGGCGCCGCCCTATACCTTGTCTGCCTCCCCGCGTTGCGTCGCGGTGCATGGAGCCGGGCCACCTCGACCT\nGAATGGAAGCCGGCGGCACCTCGCTAACGGATTCACCACTCCAAGAATTGGAGCCAATCAATTCTTGCGGAGAACTGTGA\nATGCGCAAACCAACCCTTGGCAGAACATATCCATCGCGTCCGCCATCTCCAGCAGCCGCACGCGGCGCATCTCGGGCAGC\nGTTGGGTCCTGGCCACGGGTGCGCATGATCGTGCTCCTGTCGTTGAGGACCCGGCTAGGCTGGCGGGGTTGCCTTACTGG\nTTAGCAGAATGAATCACCGATACGCGAGCGAACGTGAAGCGACTGCTGCTGCAAAACGTCTGCGACCTGAGCAACAACAT\nGAATGGTCTTCGGTTTCCGTGTTTCGTAAAGTCTGGAAACGCGGAAGTCAGCGCCCTGCACCATTATGTTCCGGATCTGC\nATCGCAGGATGCTGCTGGCTACCCTGTGGAACACCTACATCTGTATTAACGAAGCGCTGGCATTGACCCTGAGTGATTTT\nTCTCTGGTCCCGCCGCATCCATACCGCCAGTTGTTTACCCTCACAACGTTCCAGTAACCGGGCATGTTCATCATCAGTAA\nCCCGTATCGTGAGCATCCTCTCTCGTTTCATCGGTATCATTACCCCCATGAACAGAAATCCCCCTTACACGGAGGCATCA\nGTGACCAAACAGGAAAAAACCGCCCTTAACATGGCCCGCTTTATCAGAAGCCAGACATTAACGCTTCTGGAGAAACTCAA\nCGAGCTGGACGCGGATGAACAGGCAGACATCTGTGAATCGCTTCACGACCACGCTGATGAGCTTTACCGCAGCTGCCTCG\nCGCGTTTCGGTGATGACGGTGAAAACCTCTGACACATGCAGCTCCCGGAGACGGTCACAGCTTGTCTGTAAGCGGATGCC\nGGGAGCAGACAAGCCCGTCAGGGCGCGTCAGCGGGTGTTGGCGGGTGTCGGGGCGCAGCCATGACCCAGTCACGTAGCGA\nTAGCGGAGTGTATACTGGCTTAACTATGCGGCATCAGAGCAGATTGTACTGAGAGTGCACCATATATGCGGTGTGAAATA\nCCGCACAGATGCGTAAGGAGAAAATACCGCATCAGGCGCTCTTCCGCTTCCTCGCTCACTGACTCGCTGCGCTCGGTCGT\nTCGGCTGCGGCGAGCGGTATCAGCTCACTCAAAGGCGGTAATACGGTTATCCACAGAATCAGGGGATAACGCAGGAAAGA\nACATGTGAGCAAAAGGCCAGCAAAAGGCCAGGAACCGTAAAAAGGCCGCGTTGCTGGCGTTTTTCCATAGGCTCCGCCCC\nCCTGACGAGCATCACAAAAATCGACGCTCAAGTCAGAGGTGGCGAAACCCGACAGGACTATAAAGATACCAGGCGTTTCC\nCCCTGGAAGCTCCCTCGTGCGCTCTCCTGTTCCGACCCTGCCGCTTACCGGATACCTGTCCGCCTTTCTCCCTTCGGGAA\nGCGTGGCGCTTTCTCATAGCTCACGCTGTAGGTATCTCAGTTCGGTGTAGGTCGTTCGCTCCAAGCTGGGCTGTGTGCAC\nGAACCCCCCGTTCAGCCCGACCGCTGCGCCTTATCCGGTAACTATCGTCTTGAGTCCAACCCGGTAAGACACGACTTATC\nGCCACTGGCAGCAGCCACTGGTAACAGGATTAGCAGAGCGAGGTATGTAGGCGGTGCTACAGAGTTCTTGAAGTGGTGGC\nCTAACTACGGCTACACTAGAAGGACAGTATTTGGTATCTGCGCTCTGCTGAAGCCAGTTACCTTCGGAAAAAGAGTTGGT\nAGCTCTTGATCCGGCAAACAAACCACCGCTGGTAGCGGTGGTTTTTTTGTTTGCAAGCAGCAGATTACGCGCAGAAAAAA\nAGGATCTCAAGAAGATCCTTTGATCTTTTCTACGGGGTCTGACGCTCAGTGGAACGAAAACTCACGTTAAGGGATTTTGG\nTCATGAGATTATCAAAAAGGATCTTCACCTAGATCCTTTTAAATTAAAAATGAAGTTTTAAATCAATCTAAAGTATATAT\nGAGTAAACTTGGTCTGACAGTTACCAATGCTTAATCAGTGAGGCACCTATCTCAGCGATCTGTCTATTTCGTTCATCCAT\nAGTTGCCTGACTCCCCGTCGTGTAGATAACTACGATACGGGAGGGCTTACCATCTGGCCCCAGTGCTGCAATGATACCGC\nGAGACCCACGCTCACCGGCTCCAGATTTATCAGCAATAAACCAGCCAGCCGGAAGGGCCGAGCGCAGAAGTGGTCCTGCA\nACTTTATCCGCCTCCATCCAGTCTATTAATTGTTGCCGGGAAGCTAGAGTAAGTAGTTCGCCAGTTAATAGTTTGCGCAA\nCGTTGTTGCCATTGCTGCAGGCATCGTGGTGTCACGCTCGTCGTTTGGTATGGCTTCATTCAGCTCCGGTTCCCAACGAT\nCAAGGCGAGTTACATGATCCCCCATGTTGTGCAAAAAAGCGGTTAGCTCCTTCGGTCCTCCGATCGTTGTCAGAAGTAAG\nTTGGCCGCAGTGTTATCACTCATGGTTATGGCAGCACTGCATAATTCTCTTACTGTCATGCCATCCGTAAGATGCTTTTC\nTGTGACTGGTGAGTACTCAACCAAGTCATTCTGAGAATAGTGTATGCGGCGACCGAGTTGCTCTTGCCCGGCGTCAACAC\nGGGATAATACCGCGCCACATAGCAGAACTTTAAAAGTGCTCATCATTGGAAAACGTTCTTCGGGGCGAAAACTCTCAAGG\nATCTTACCGCTGTTGAGATCCAGTTCGATGTAACCCACTCGTGCACCCAACTGATCTTCAGCATCTTTTACTTTCACCAG\nCGTTTCTGGGTGAGCAAAAACAGGAAGGCAAAATGCCGCAAAAAAGGGAATAAGGGCGACACGGAAATGTTGAATACTCA\nTACTCTTCCTTTTTCAATATTATTGAAGCATTTATCAGGGTTATTGTCTCATGAGCGGATACATATTTGAATGTATTTAG\nAAAAATAAACAAATAGGGGTTCCGCGCACATTTCCCCGAAAAGTGCCACCTGACGTCTAAGAAACCATTATTATCATGAC\nATTAACCTATAAAAATAGGCGTATCACGAGGCCCTTTCGTCTTCAAGAAA\n','from pTRC99a vector\nw/ RBS\n',22,'001381493564837','001407330724032','YES','FOWARD'),(50,'PART050','HCE',6,'GATCTCTCCT TCACAGATTC CCAATCTCTT GTTAAATAAC GAAAAAGCAT CAATCAAAAC GGCGGCATGT CTTTCTATAT TCCAGCAATG TTTTATAGGG GACATATTGA TGAAGATGGG TATCACCTTA GTAAAAAAAG AATTGCTATA AGCTGCTCTT TTTTGTTCGT GATATACTGA TAATAAATTG AATTTTCACA CTTCTGGAAA AAGGAGATAT CA ',NULL,22,'001381493703185','001397714532247','YES','FOWARD'),(51,'PART051','Regulatory region for DmpR',6,'GTAAGCGAGG CCCCTATTTA TTTTTAGATG GGGAAATCAG GGTCGCCGCT ATAGCGCAAG GCAGGCGGCG ATTCCAGATG GGGTCATGGG GAAAATCGGC AGTTTTTTCA CCTGCGCCAG CTCCCCATAG CCGACCTCAG TATCTGGCAA AAGTCAACCA AATGATCAAT CGACGGCAGT GACTTTAGTG CTAGAGATCA GCCTTCAGCT GCGCCATGAG CATTTGCTCA AGCGGCCTTG GGCAATTGAT CAAATGCTTA AAAAGCCTGC GCAAGCGCGG CTTAATTTCG CTCGCTCCGA TCATTCTAAA AATTAGAAAC ACATTGAAAA ACAATACCTT GAAGTCGGTT TTCAGACCTT GGCACAGCTG TTGCACTTTG TCCTGCGCAA TCCGCCAACC TGGAG ','Source : Pseudomonas putida KCTC1452\nDmpR binding site / Ïƒ54-dependent promoter / DmpR promoter\n',22,'001381494443603','001397714478185','YES','FOWARD'),(53,'PART053','EGFP',62,'Atggtgagca agggcgagga gctgttcacc ggggtggtgc ccatcctggt cgagctggac ggcgacgtaa acggccacaa gttcagcgtg tccggcgagg gcgagggcga tgccacctac ggcaagctga ccctgaagtt catctgcacc accggcaagc tgcccgtgcc ctggcccacc ctcgtgacca ccctgaccta cggcgtgcag tgcttcagcc gctaccccga ccacatgaag cagcacgact tcttcaagtc cgccatgccc gaaggctacg tccaggagcg caccatcttc ttcaaggacg acggcaacta caagacccgc gccgaggtga agttcgaggg cgacaccctg gtgaaccgca tcgagctgaa gggcatcgac ttcaaggagg acggcaacat cctggggcac aagctggagt acaactacaa cagccacaac gtctatatca tggccgacaa gcagaagaac ggcatcaagg tgaacttcaa gatccgccac aacatcgagg acggcagcgt gcagctcgcc gaccactacc agcagaacac ccccatcggc gacggccccg tgctgctgcc cgacaaccac tacctgagca cccagtccgc cctgagcaaa gaccccaacg agaagcgcga tcacatggtc ctgctggagt tcgtgaccgc cgccgggatc actctcggca tggacaagct gtacaag',NULL,22,'001381494681307','001381496003989','YES','FOWARD'),(54,'PART054','sf-GFP',62,'Atgagcaaag gtgaagaact gtttaccggc gttgtgccga ttctggtgga actggatggc gatgtgaacg gtcacaaatt cagcgtgcgt ggtgaaggtg aaggcgatgc cacgattggc aaactgacgc tgaaatttat ctgcaccacc ggcaaactgc cggtgccgtg gccgacgctg gtgaccaccc tgacctatgg cgttcagtgt tttagtcgct atccggatca catgaaacgt cacgatttct ttaaatctgc aatgccggaa ggctatgtgc aggaacgtac gattagcttt aaagatgatg gcaaatataa aacgcgcgcc gttgtgaaat ttgaaggcga taccctggtg aaccgcattg aactgaaagg cacggatttt aaagaagatg gcaatatcct gggccataaa ctggaataca actttaatag ccataatgtt tatattacgg cggataaaca gaaaaatggc atcaaagcga attttaccgt tcgccataac gttgaagatg gcagtgtgca gctggcagat cattatcagc agaatacccc gattggtgat ggtccggtgc tgctgccgga taatcattat ctgagcacgc agaccgttct gtctaaagat ccgaacgaaa aacgggacca catggttctg cacgaatatg tgaatgcggc aggtattacg tggagccatc cgcagttcga aaaaggtggt tct\n',NULL,22,'001381494783795','001381496057277','YES','FOWARD'),(55,'PART055','Turbo-RFP',62,'Atgagcgagc tgatcaagga gaacatgcac atgaagctgt acatggaggg caccgtgaac aaccaccact tcaagtgcac atccgagggc gaaggcaagc cctacgaggg cacccagacc atgaagatca aggtggtcga gggcggccct ctccccttcg ccttcgacat cctggctacc agcttcatgt acggcagcaa agccttcatc aaccacaccc agggcatccc cgacttcttt aagcagtcct tccctgaggg cttcacatgg gagagaatca ccacatacga agacgggggc gtgctgaccg ctacccagga caccagcttc cagaacggct gcatcatcta caacgtcaag atcaacgggg tgaacttccc atccaacggc cctgtgatgc agaagaaaac acgcggctgg gaggccaaca ccgagatgct gtaccccgct gacggcggcc tgagaggcca cagccagatg gccctgaagc tcgtgggcgg gggctacctg cactgctcct tcaagaccac atacagatcc aagaaacccg ctaagaacct caagatgccc ggcttccact tcgtggacca cagactggaa agaatcaagg aggccgacaa agagacctac gtcgagcagc acgagatggc tgtggccaag tactgcgacc tccctagcaa actggggcac agagatgaat ccggactcag atctcgaatc agccatggc\n',NULL,22,'001381494807944','001381496061890','YES','FOWARD'),(56,'PART056','mRFP',62,'Atggcctcct ccgaggacgt catcaaggag ttcatgcgct tcaaggtgcg catggagggc tccgtgaacg gccacgagtt cgagatcgag ggcgagggcg agggccgccc ctacgagggc acccagaccg ccaagctgaa ggtgaccaag ggcggccccc tgcccttcgc ctgggacatc ctgtcccctc agttcacgta cggctccaag gcctacgtga agcaccccgc cgacatcccc gactacttga agctgtcctt ccccgagggc ttcaagtggg agcgcgtgat gaacttcgag gacggcggcg tggtgaccgt gacccaggac tcctccctgc aggacggcga gttcatctac aaggtgaagc tgcgcggcac caacttcccc tccgacggcc ccgtaatgca gaagaagacc atgggctggg aggcctccac cgagcggatg taccccgagg acggcgccct gaagggcgag atcaagatga ggctgaagct gaaggacggc ggccactacg acgccgaggt caagaccacc tacatggcca agaagcccgt gcagctgccc ggcgcctaca agaccgacat caagctggac atcacctccc acaacgagga ctacaccatc gtggaacagt acgagcgcgc cgagggccgc cactccaccg gcgcctaa\n','Mutant RFP_Q66T\n',22,'001381494838591','001381496069534','YES','FOWARD'),(57,'PART057','Chloramphenicol resistant gene',63,'Atggagaaaa aaatcactgg atataccacc gttgatatat cccaatggca tcgtaaagaa cattttgagg catttcagtc agttgctcaa tgtacctata accagaccgt tcagctggat attacggcct ttttaaagac cgtaaagaaa aataagcaca agttttatcc ggcctttatt cacattcttg cccgcctgat gaatgctcat ccggaattcc gtatggcaat gaaagacggt gagctggtga tatgggatag tgttcaccct tgttacaccg ttttccatga gcaaactgaa acgttttcat cgctctggag tgaataccac gacgatttcc ggcagtttct acacatatat tcgcaagatg tggcgtgtta cggtgaaaac ctggcctatt tccctaaagg gtttattgag aatatgtttt tcgtctcagc caatccctgg gtgagtttca ccagttttga tttaaacgtg gccaatatgg acaacttctt cgcccccgtt ttcaccatgg gcaaatatta tacgcaaggc gacaaggtgc tgatgccgct ggcgattcag gttcatcatg ccgtttgtga tggcttccat gtcggcagaa tgcttaatga attacaacag tactgcgatg agtggcaggg cggggcgtaa\n',NULL,22,'001381495982532','001381495982532','YES','FOWARD'),(58,'PART058','Kanamycin resistant gene',63,'Atgagccata ttcaacggga aacgtcttgc tctaggccgc gattaaattc caacatggat gctgatttat atgggtataa atgggctcgc gataatgtcg ggcaatcagg tgcgacaatc tatcgattgt atgggaagcc cgatgcgcca gagttgtttc tgaaacatgg caaaggtagc gttgccaatg atgttacaga tgagatggtc agactaaact ggctgacgga atttatgcct cttccgacca tcaagcattt tatccgtact cctgatgatg catggttact caccactgcg atccccggga aaacagcatt ccaggtatta gaagaatatc ctgattcagg tgaaaatatt gttgatgcgc tggcagtgtt cctgcgccgg ttgcattcga ttcctgtttg taattgtcct tttaacagcg atcgcgtatt tcgtctcgct caggcgcaat cacgaatgaa taacggtttg gttgatgcga gtgattttga tgacgagcgt aatggctggc ctgttgaaca agtctggaaa gaaatgcata aacttttgcc attctcaccg gattcagtcg tcactcatgg tgatttctca cttgataacc ttatttttga cgaggggaaa ttaataggtt gtattgatgt tggacgagtc ggaatcgcag accgatacca ggatcttgcc atcctatgga actgcctcgg tgagttttct ccttcattac agaaacggct ttttcaaaaa tatggtattg ataatcctga tatgaataaa ttgcagtttc atttgatgct cgatgagttt ttctaa\n',NULL,22,'001381496096383','001381496096383','YES','FOWARD'),(59,'PART059','Tetracycline resistant gene',63,'atgaaatctaacaatgcgctcatcgtcatcctcggcaccgtcaccctggatgctgtaggcataggcttggttatgccggtactgccgggcctcttgcgggatatcgtccattccgacagcatcgccagtcactatggcgtgctgctagcgctatatgcgttgatgcaatttctatgcgcacccgttctcggagcactgtccgaccgctttggccgccgcccagtcctgctcgcttcgctacttggagccactatcgactacgcgatcatggcgaccacacccgtcctgtggatcctctacgccggacgcatcgtggccggcatcaccggcgccacaggtgcggttgctggcgcctatatcgccgacatcaccgatggggaagatcgggctcgccacttcgggctcatgagcgcttgtttcggcgtgggtatggtggcaggccccgtggccgggggactgttgggcgccatctccttgcatgcaccattccttgcggcggcggtgctcaacggcctcaacctactactgggctgcttcctaatgcaggagtcgcataagggagagcgtcgaccgatgcccttgagagccttcaacccagtcagctccttccggtgggcgcggggcatgactatcgtcgccgcacttatgactgtcttctttatcatgcaactcgtaggacaggtgccggcagcgctctgggtcattttcggcgaggaccgctttcgctggagcgcgacgatgatcggcctgtcgcttgcggtattcggaatcttgcacgccctcgctcaagccttcgtcactggtcccgccaccaaacgtttcggcgagaagcaggccattatcgccggcatggcggccgacgcgctgggctacgtcttgctggcgttcgcgacgcgaggctggatggccttccccattatgattcttctcgcttccggcggcatcgggatgcccgcgttgcaggccatgctgtccaggcaggtagatgacgaccatcagggacagcttcaaggatcgctcgcggctcttaccagcctaacttcgatcattggaccgctgatcgtcacggcgatttatgccgcctcggcgagcacatggaacgggttggcatggattgtaggcgccgccctataccttgtctgcctccccgcgttgcgtcgcggtgcatggagccgggccacctcgacctga\n',NULL,22,'001381496117336','001381496117336','YES','FOWARD'),(60,'PART060','DAAT(d-amino acid aminotransferase) gene',8,'Atgttggaac aaccaatagg agtcattgat tccggggttg gcggtttaac cgttgcgaag gaaatcatga gacagctacc taaagaaaat attatctacg tcggggatac gaaacggtgt ccttacgggc cgcgccctga agaagaggtg cttcaatata cgtgggagct gacgaattat ttactcgaaa accaccacat caaaatgctc gtgatcgcct gtaatacagc aacagcgatc gctttggatg acatccagcg cagcgtcggt ataccggtgg tcggagtcat ccagcctggt gcgagagcag cgataaaagt gacggataat cagcatatcg gtgtcatcgg cacagagaat acgattaaga gcaatgcata cgaagaagcg cttttggcat taaaccctga tttgaaggtt gaaaaccttg cctgcccgct gcttgtgcct tttgtggaaa gcgggaagtt tctcgacaaa acagcagacg agattgttaa aacctcgctg tatccgttaa aagacacatc aattgattcg ctgattttag gctgcaccca ttaccctatt ttaaaagaag ccattcaaag atatatggga gagcacgtaa acattatttc gtccggcgat gaaacagccc gggaagtcag cacaattctc tcttataaag ggctgctgaa ccagtctccg attgccccgg atcatcagtt cctgacaaca ggggcgcgtg atcagtttgc aaaaatcgca gacgattggt ttggccatga agtcgggcat gtggaatgta tctcactgca agaaccgatt aaaagat ag\n',NULL,22,'001381496150269','001381496150269','YES','FOWARD'),(61,'PART061','t0 terminator',11,'Gcgggactct ggggttcgag agctcgcttg gactcctgtt gatagatcca gtaatgacct cagaactcca tctggatttg ttcagaacgc tcggttgccg ccgggcgttt tttattggtg agaatccaag cactagtaac aacttatatc gtatggggct gacttcaggt gctacatttg aagagata\n',NULL,22,'001381496249259','001381496249259','YES','FOWARD'),(62,'PART062','ColE1',64,'Ttgagatcct ttttttctgc gcgtaatctg ctgcttgcaa acaaaaaaac caccgctacc agcggtggtt tgtttgccgg atcaagagct accaactctt tttccgaagg taactggctt cagcagagcg cagataccaa atactgtcct tctagtgtag ccgtagttag gccaccactt caagaactct gtagcaccgc ctacatacct cgctctgcta atcctgttac cagtggctgc tgccagtggc gataagtcgt gtcttaccgg gttggactca agacgatagt taccggataa ggcgcagcgg tcgggctgaa cggggggttc gtgcacacag cccagcttgg agcgaacgac ctacaccgaa ctgagatacc tacagcgtga gctatgagaa agcgccacgc ttcccgaagg gagaaaggcg gacaggtatc cggtaagcgg cagggtcgga acaggagagc gcacgaggga gcttccaggg ggaaacgcct ggtatcttta tagtcctgtc gggtttcgcc acctctgact tgagcgtcga tttttgtgat gctcgtcagg ggggcggagc ctatggaaa\n',NULL,22,'001381496322473','001381496322473','YES','FOWARD'),(63,'PART063','ACYC',64,'Gcgctagcgg agtgtatact ggcttactat gttggcactg atgagggtgt cagtgaagtg cttcatgtgg caggagaaaa aaggctgcac cggtgcgtca gcagaatatg tgatacagga tatattccgc ttcctcgctc actgactcgc tacgctcggt cgttcgactg cggcgagcgg aaatggctta cgaacggggc ggagatttcc tggaagatgc caggaagata cttaacaggg aagtgagagg gccgcggcaa agccgttttt ccataggctc cgcccccctg acaagcatca cgaaatctga cgctcaaatc agtggtggcg aaacccgaca ggactataaa gataccaggc gtttccccct ggcggctccc tcgtgcgctc tcctgttcct gcctttcggt ttaccggtgt cattccgctg ttatggccgc gtttgtctca ttccacgcct gacactcagt tccgggtagg cagttcgctc caagctggac tgtatgcacg aaccccccgt tcagtccgac cgctgcgcct tatccggtaa ctatcgtctt gagtccaacc cggaaagaca tgcaaaagca ccactggcag cagccactgg taattgattt agaggagtta gtcttgaagt catgcgccgg ttaaggctaa actgaaagga caagttttgg tgactgcgct cctccaagcc agttacctcg gttcaaagag ttggtagctc agagaacctt cgaaaaaccg ccctgcaagg cggttttttc gttttcagag caagagatta cgcgcagacc aaaacgatct caagaagatc atcttattaa tcagataaaa tatttctaga tttcagtgca atttatctct tcaaatgtag cacctgaagt cagccccata cgatataagt tgt\n',NULL,22,'001381496343654','001381496343654','YES','FOWARD'),(64,'PART064','RSF',64,'Cttccgcttc ctcgctcact gactcgctac gctcggtcgt tcgactgcgg cgagcggtgt cagctcactc aaaagcggta atacggttat ccacagaatc aggggataaa gccggaaaga acatgtgagc aaaaagcaaa gcaccggaag aagccaacgc cgcaggcgtt tttccatagg ctccgccccc ctgacgagca tcacaaaaat cgacgctcaa gccagaggtg gcgaaacccg acaggactat aaagatacca ggcgtttccc cctggaagct ccctcgtgcg ctctcctgtt ccgaccctgc cgcttaccgg atacctgtcc gcctttctcc cttcgggaag cgtggcgctt tctcatagct cacgctgttg gtatctcagt tcggtgtagg tcgttcgctc caagctgggc tgtgtgcacg aaccccccgt tcagcccgac cgctgcgcct tatccggtaa ctatcgtctt gagtccaacc cggtaagaca cgacttatcg ccactggcag cagccattgg taactgattt agaggacttt gtcttgaagt tatgcacctg ttaaggctaa actgaaagaa cagattttgg tgagtgcggt cctccaaccc acttaccttg gttcaaagag ttggtagctc agcgaacctt gagaaaacca ccgttggtag cggtggtttt tctttattta tgagatgatg aatcaatcgg tctatcaagt caacgaacag ctattccgtt\n',NULL,22,'001381496357420','001381496357420','YES','FOWARD'),(65,'PART065','CDF',64,'Gatcaaagga tcttcttgag atcctttttt tctgcgcgta atcttttgcc ctgtaaacga aaaaaccacc tggggaggtg gtttgatcga aggttaagtc agttggggaa ctgcttaacc gtggtaactg gctttcgcag agcacagcaa ccaaatctgt ccttccagtg tagccggact ttggcgcaca cttcaagagc aaccgcgtgt ttagctaaac aaatcctctg cgaactccca gttaccaatg gctgctgcca gtggcgtttt accgtgcttt tccgggttgg actcaagtga acagttaccg gataaggcgc agcagtcggg ctgaacgggg agttcttgct tacagcccag cttggagcga acgacctaca ccgagccgag ataccagtgt gtgagctatg agaaagcgcc acacttcccg taagggagaa aggcggaaca ggtatccggt aaacggcagg gtcggaacag gagagcgcaa gagggagcga cccgccggaa acggtgggga tctttaagtc ctgtcgggtt tcgcccgtac tgtcagattc atggttgagc ctcacggctc ccacagatgc accggaaaag cgtctgttta tgtgaactct ggcaggaggg cggagcctat ggaaaaacgc caccggcgcg gccctgctgt tttgcctcac atgttagtcc cctgcttatc cacggaatct gtgggtaact ttgtatgtgt ccgcagcgc\n',NULL,22,'001381496370659','001381496370659','YES','FOWARD'),(71,'PART071','Metagenome',8,'atgc','atgc',22,'001381715898476','001381715898476','YES','FOWARD'),(91,'PART091','sigma54 dependent -24 ',6,'TGGC ','Parts registry BBa_J23100 Constitutive promoter for E. coli Ïƒ70-dependent w/o RBS  ',22,'001397714416507','001397714445602','YES','FOWARD'),(92,'PART092','Strongest promoter',6,'TTGACGGCTA GCTCAGTCCT AGGTACAGTG CTAGC ',NULL,22,'001397714604527','001397714604527','YES','FOWARD'),(101,'PART101','T7 Terminator',11,'CTGCTAACAA AGCCCGAAAG GAAGCTGAGT TGGCTGCTGC CACCGCTGAG CAATAACTAG CATAACCCCT TGGGGCCTCT AAACGGGTCT TGAGGGGTTT TTTGCTGAAA GGAGGAACTA TATCCGGAT ','T7 Terminator',22,'001403072706938','001403072706938','YES','FOWARD'),(102,'PART102','LacO',7,'GGGAATTGTG AGCGGATAAC AATTCCCC ','LacO',22,'001403072825191','001403072825191','YES','FOWARD'),(103,'PART103','LacI',8,'GTGGTGAATG TGAAACCAGT AACGTTATAC GATGTCGCAG AGTATGCCGG TGTCTCTTAT CAGACCGTTT CCCGCGTGGT GAACCAGGCC AGCCACGTTT CTGCGAAAAC GCGGGAAAAA GTGGAAGCGG CGATGGCGGA GCTGAATTAC ATTCCCAACC GCGTGGCACA ACAACTGGCG GGCAAACAGT CGTTGCTGAT TGGCGTTGCC ACCTCCAGTC TGGCCCTGCA CGCGCCGTCG CAAATTGTCG CGGCGATTAA ATCTCGCGCC GATCAACTGG GTGCCAGCGT GGTGGTGTCG ATGGTAGAAC GAAGCGGCGT CGAAGCCTGT AAAGCGGCGG TGCACAATCT TCTCGCGCAA CGCGTCAGTG GGCTGATCAT TAACTATCCG CTGGATGACC AGGATGCCAT TGCTGTGGAA GCTGCCTGCA CTAATGTTCC GGCGTTATTT CTTGATGTCT CTGACCAGAC ACCCATCAAC AGTATTATTT TCTCCCATGA AGACGGTACG CGACTGGGCG TGGAGCATCT GGTCGCATTG GGTCACCAGC AAATCGCGCT GTTAGCGGGC CCATTAAGTT CTGTCTCGGC GCGTCTGCGT CTGGCTGGCT GGCATAAATA TCTCACTCGC AATCAAATTC AGCCGATAGC GGAACGGGAA GGCGACTGGA GTGCCATGTC CGGTTTTCAA CAAACCATGC AAATGCTGAA TGAGGGCATC GTTCCCACTG CGATGCTGGT TGCCAACGAT CAGATGGCGC TGGGCGCAAT GCGCGCCATT ACCGAGTCCG GGCTGCGCGT TGGTGCGGAT ATCTCGGTAG TGGGATACGA CGATACCGAA GACAGCTCAT GTTATATCCC GCCGTTAACC ACCATCAAAC AGGATTTTCG CCTGCTGGGG CAAACCAGCG TGGACCGCTT GCTGCAACTC TCTCAGGGCC AGGCGGTGAA GGGCAATCAG CTGTTGCCCG TCTCACTGGT GAAAAGAAAA ACCACCCTGG CGCCCAATAC GCAAACCGCC TCTCCCCGCG CGTTGGCCGA TTCATTAATG CAGCTGGCAC GACAGGTTTC CCGACTGGAA AGCGGGCAGT GA ','LacI',22,'001403072854174','001403072854174','YES','FOWARD'),(104,'PART104','Ampicillin',63,'ATGAGTATTC AACATTTCCG TGTCGCCCTT ATTCCCTTTT TTGCGGCATT TTGCCTTCCT GTTTTTGCTC ACCCAGAAAC GCTGGTGAAA GTAAAAGATG CTGAAGATCA GTTGGGTGCA CGAGTGGGTT ACATCGAACT GGATCTCAAC AGCGGTAAGA TCCTTGAGAG TTTTCGCCCC GAAGAACGTT TTCCAATGAT GAGCACTTTT AAAGTTCTGC TATGTGGCGC GGTATTATCC CGTGTTGACG CCGGGCAAGA GCAACTCGGT CGCCGCATAC ACTATTCTCA GAATGACTTG GTTGAGTACT CACCAGTCAC AGAAAAGCAT CTTACGGATG GCATGACAGT AAGAGAATTA TGCAGTGCTG CCATAACCAT GAGTGATAAC ACTGCGGCCA ACTTACTTCT GACAACGATC GGAGGACCGA AGGAGCTAAC CGCTTTTTTG CACAACATGG GGGATCATGT AACTCGCCTT GATCGTTGGG AACCGGAGCT GAATGAAGCC ATACCAAACG ACGAGCGTGA CACCACGATG CCTGCAGCAA TGGCAACAAC GTTGCGCAAA CTATTAACTG GCGAACTACT TACTCTAGCT TCCCGGCAAC AATTAATAGA CTGGATGGAG GCGGATAAAG TTGCAGGACC ACTTCTGCGC TCGGCCCTTC CGGCTGGCTG GTTTATTGCT GATAAATCTG GAGCCGGTGA GCGTGGGTCT CGCGGTATCA TTGCAGCACT GGGGCCAGAT GGTAAGCCCT CCCGTATCGT AGTTATCTAC ACGACGGGGA GTCAGGCAAC TATGGATGAA CGAAATAGAC AGATCGCTGA GATAGGTGCC TCACTGATTA AGCATTGGTA A ','Ampicillin',22,'001403072896387','001403072896387','YES','FOWARD'),(105,'PART105','AmpR promoter',6,'TTCAAATATG TATCCGCTCA TGAGACAAT ',NULL,22,'001403072929641','001403072929641','YES','FOWARD'),(106,'PART106','pET11a',112,'TTCTCATGTT TGACAGCTTA TCATCGATAA GCTTTAATGC GGTAGTTTAT CACAGTTAAA TTGCTAACGC AGTCAGGCAC CGTGTATGAA ATCTAACAAT GCGCTCATCG TCATCCTCGG CACCGTCACC CTGGATGCTG TAGGCATAGG CTTGGTTATG CCGGTACTGC CGGGCCTCTT GCGGGATATC CGGATATAGT TCCTCCTTTC AGCAAAAAAC CCCTCAAGAC CCGTTTAGAG GCCCCAAGGG GTTATGCTAG TTATTGCTCA GCGGTGGCAG CAGCCAACTC AGCTTCCTTT CGGGCTTTGT TAGCAGCCGG ATCCGCGACC CATTTGCTGT CCACCAGTCA TGCTAGCCAT ATGTATATCT CCTTCTTAAA GTTAAACAAA ATTATTTCTA GAGGGGAATT GTTATCCGCT CACAATTCCC CTATAGTGAG TCGTATTAAT TTCGCGGGAT CGAGATCTCG ATCCTCTACG CCGGACGCAT CGTGGCCGGC ATCACCGGCG CCACAGGTGC GGTTGCTGGC GCCTATATCG CCGACATCAC CGATGGGGAA GATCGGGCTC GCCACTTCGG GCTCATGAGC GCTTGTTTCG GCGTGGGTAT GGTGGCAGGC CCCGTGGCCG GGGGACTGTT GGGCGCCATC TCCTTGCATG CACCATTCCT TGCGGCGGCG GTGCTCAACG GCCTCAACCT ACTACTGGGC TGCTTCCTAA TGCAGGAGTC GCATAAGGGA GAGCGTCGAG ATCCCGGACA CCATCGAATG GCGCAAAACC TTTCGCGGTA TGGCATGATA GCGCCCGGAA GAGAGTCAAT TCAGGGTGGT GAATGTGAAA CCAGTAACGT TATACGATGT CGCAGAGTAT GCCGGTGTCT CTTATCAGAC CGTTTCCCGC GTGGTGAACC AGGCCAGCCA CGTTTCTGCG AAAACGCGGG AAAAAGTGGA AGCGGCGATG GCGGAGCTGA ATTACATTCC CAACCGCGTG GCACAACAAC TGGCGGGCAA ACAGTCGTTG CTGATTGGCG TTGCCACCTC CAGTCTGGCC CTGCACGCGC CGTCGCAAAT TGTCGCGGCG ATTAAATCTC GCGCCGATCA ACTGGGTGCC AGCGTGGTGG TGTCGATGGT AGAACGAAGC GGCGTCGAAG CCTGTAAAGC GGCGGTGCAC AATCTTCTCG CGCAACGCGT CAGTGGGCTG ATCATTAACT ATCCGCTGGA TGACCAGGAT GCCATTGCTG TGGAAGCTGC CTGCACTAAT GTTCCGGCGT TATTTCTTGA TGTCTCTGAC CAGACACCCA TCAACAGTAT TATTTTCTCC CATGAAGACG GTACGCGACT GGGCGTGGAG CATCTGGTCG CATTGGGTCA CCAGCAAATC GCGCTGTTAG CGGGCCCATT AAGTTCTGTC TCGGCGCGTC TGCGTCTGGC TGGCTGGCAT AAATATCTCA CTCGCAATCA AATTCAGCCG ATAGCGGAAC GGGAAGGCGA CTGGAGTGCC ATGTCCGGTT TTCAACAAAC CATGCAAATG CTGAATGAGG GCATCGTTCC CACTGCGATG CTGGTTGCCA ACGATCAGAT GGCGCTGGGC GCAATGCGCG CCATTACCGA GTCCGGGCTG CGCGTTGGTG CGGATATCTC GGTAGTGGGA TACGACGATA CCGAAGACAG CTCATGTTAT ATCCCGCCGT TAACCACCAT CAAACAGGAT TTTCGCCTGC TGGGGCAAAC CAGCGTGGAC CGCTTGCTGC AACTCTCTCA GGGCCAGGCG GTGAAGGGCA ATCAGCTGTT GCCCGTCTCA CTGGTGAAAA GAAAAACCAC CCTGGCGCCC AATACGCAAA CCGCCTCTCC CCGCGCGTTG GCCGATTCAT TAATGCAGCT GGCACGACAG GTTTCCCGAC TGGAAAGCGG GCAGTGAGCG CAACGCAATT AATGTAAGTT AGCTCACTCA TTAGGCACCG GGATCTCGAC CGATGCCCTT GAGAGCCTTC AACCCAGTCA GCTCCTTCCG GTGGGCGCGG GGCATGACTA TCGTCGCCGC ACTTATGACT GTCTTCTTTA TCATGCAACT CGTAGGACAG GTGCCGGCAG CGCTCTGGGT CATTTTCGGC GAGGACCGCT TTCGCTGGAG CGCGACGATG ATCGGCCTGT CGCTTGCGGT ATTCGGAATC TTGCACGCCC TCGCTCAAGC CTTCGTCACT GGTCCCGCCA CCAAACGTTT CGGCGAGAAG CAGGCCATTA TCGCCGGCAT GGCGGCCGAC GCGCTGGGCT ACGTCTTGCT GGCGTTCGCG ACGCGAGGCT GGATGGCCTT CCCCATTATG ATTCTTCTCG CTTCCGGCGG CATCGGGATG CCCGCGTTGC AGGCCATGCT GTCCAGGCAG GTAGATGACG ACCATCAGGG ACAGCTTCAA GGATCGCTCG CGGCTCTTAC CAGCCTAACT TCGATCACTG GACCGCTGAT CGTCACGGCG ATTTATGCCG CCTCGGCGAG CACATGGAAC GGGTTGGCAT GGATTGTAGG CGCCGCCCTA TACCTTGTCT GCCTCCCCGC GTTGCGTCGC GGTGCATGGA GCCGGGCCAC CTCGACCTGA ATGGAAGCCG GCGGCACCTC GCTAACGGAT TCACCACTCC AAGAATTGGA GCCAATCAAT TCTTGCGGAG AACTGTGAAT GCGCAAACCA ACCCTTGGCA GAACATATCC ATCGCGTCCG CCATCTCCAG CAGCCGCACG CGGCGCATCT CGGGCAGCGT TGGGTCCTGG CCACGGGTGC GCATGATCGT GCTCCTGTCG TTGAGGACCC GGCTAGGCTG GCGGGGTTGC CTTACTGGTT AGCAGAATGA ATCACCGATA CGCGAGCGAA CGTGAAGCGA CTGCTGCTGC AAAACGTCTG CGACCTGAGC AACAACATGA ATGGTCTTCG GTTTCCGTGT TTCGTAAAGT CTGGAAACGC GGAAGTCAGC GCCCTGCACC ATTATGTTCC GGATCTGCAT CGCAGGATGC TGCTGGCTAC CCTGTGGAAC ACCTACATCT GTATTAACGA AGCGCTGGCA TTGACCCTGA GTGATTTTTC TCTGGTCCCG CCGCATCCAT ACCGCCAGTT GTTTACCCTC ACAACGTTCC AGTAACCGGG CATGTTCATC ATCAGTAACC CGTATCGTGA GCATCCTCTC TCGTTTCATC GGTATCATTA CCCCCATGAA CAGAAATCCC CCTTACACGG AGGCATCAGT GACCAAACAG GAAAAAACCG CCCTTAACAT GGCCCGCTTT ATCAGAAGCC AGACATTAAC GCTTCTGGAG AAACTCAACG AGCTGGACGC GGATGAACAG GCAGACATCT GTGAATCGCT TCACGACCAC GCTGATGAGC TTTACCGCAG CTGCCTCGCG CGTTTCGGTG ATGACGGTGA AAACCTCTGA CACATGCAGC TCCCGGAGAC GGTCACAGCT TGTCTGTAAG CGGATGCCGG GAGCAGACAA GCCCGTCAGG GCGCGTCAGC GGGTGTTGGC GGGTGTCGGG GCGCAGCCAT GACCCAGTCA CGTAGCGATA GCGGAGTGTA TACTGGCTTA ACTATGCGGC ATCAGAGCAG ATTGTACTGA GAGTGCACCA TATATGCGGT GTGAAATACC GCACAGATGC GTAAGGAGAA AATACCGCAT CAGGCGCTCT TCCGCTTCCT CGCTCACTGA CTCGCTGCGC TCGGTCGTTC GGCTGCGGCG AGCGGTATCA GCTCACTCAA AGGCGGTAAT ACGGTTATCC ACAGAATCAG GGGATAACGC AGGAAAGAAC ATGTGAGCAA AAGGCCAGCA AAAGGCCAGG AACCGTAAAA AGGCCGCGTT GCTGGCGTTT TTCCATAGGC TCCGCCCCCC TGACGAGCAT CACAAAAATC GACGCTCAAG TCAGAGGTGG CGAAACCCGA CAGGACTATA AAGATACCAG GCGTTTCCCC CTGGAAGCTC CCTCGTGCGC TCTCCTGTTC CGACCCTGCC GCTTACCGGA TACCTGTCCG CCTTTCTCCC TTCGGGAAGC GTGGCGCTTT CTCATAGCTC ACGCTGTAGG TATCTCAGTT CGGTGTAGGT CGTTCGCTCC AAGCTGGGCT GTGTGCACGA ACCCCCCGTT CAGCCCGACC GCTGCGCCTT ATCCGGTAAC TATCGTCTTG AGTCCAACCC GGTAAGACAC GACTTATCGC CACTGGCAGC AGCCACTGGT AACAGGATTA GCAGAGCGAG GTATGTAGGC GGTGCTACAG AGTTCTTGAA GTGGTGGCCT AACTACGGCT ACACTAGAAG GACAGTATTT GGTATCTGCG CTCTGCTGAA GCCAGTTACC TTCGGAAAAA GAGTTGGTAG CTCTTGATCC GGCAAACAAA CCACCGCTGG TAGCGGTGGT TTTTTTGTTT GCAAGCAGCA GATTACGCGC AGAAAAAAAG GATCTCAAGA AGATCCTTTG ATCTTTTCTA CGGGGTCTGA CGCTCAGTGG AACGAAAACT CACGTTAAGG GATTTTGGTC ATGAGATTAT CAAAAAGGAT CTTCACCTAG ATCCTTTTAA ATTAAAAATG AAGTTTTAAA TCAATCTAAA GTATATATGA GTAAACTTGG TCTGACAGTT ACCAATGCTT AATCAGTGAG GCACCTATCT CAGCGATCTG TCTATTTCGT TCATCCATAG TTGCCTGACT CCCCGTCGTG TAGATAACTA CGATACGGGA GGGCTTACCA TCTGGCCCCA GTGCTGCAAT GATACCGCGA GACCCACGCT CACCGGCTCC AGATTTATCA GCAATAAACC AGCCAGCCGG AAGGGCCGAG CGCAGAAGTG GTCCTGCAAC TTTATCCGCC TCCATCCAGT CTATTAATTG TTGCCGGGAA GCTAGAGTAA GTAGTTCGCC AGTTAATAGT TTGCGCAACG TTGTTGCCAT TGCTGCAGGC ATCGTGGTGT CACGCTCGTC GTTTGGTATG GCTTCATTCA GCTCCGGTTC CCAACGATCA AGGCGAGTTA CATGATCCCC CATGTTGTGC AAAAAAGCGG TTAGCTCCTT CGGTCCTCCG ATCGTTGTCA GAAGTAAGTT GGCCGCAGTG TTATCACTCA TGGTTATGGC AGCACTGCAT AATTCTCTTA CTGTCATGCC ATCCGTAAGA TGCTTTTCTG TGACTGGTGA GTACTCAACC AAGTCATTCT GAGAATAGTG TATGCGGCGA CCGAGTTGCT CTTGCCCGGC GTCAACACGG GATAATACCG CGCCACATAG CAGAACTTTA AAAGTGCTCA TCATTGGAAA ACGTTCTTCG GGGCGAAAAC TCTCAAGGAT CTTACCGCTG TTGAGATCCA GTTCGATGTA ACCCACTCGT GCACCCAACT GATCTTCAGC ATCTTTTACT TTCACCAGCG TTTCTGGGTG AGCAAAAACA GGAAGGCAAA ATGCCGCAAA AAAGGGAATA AGGGCGACAC GGAAATGTTG AATACTCATA CTCTTCCTTT TTCAATATTA TTGAAGCATT TATCAGGGTT ATTGTCTCAT GAGCGGATAC ATATTTGAAT GTATTTAGAA AAATAAACAA ATAGGGGTTC CGCGCACATT TCCCCGAAAA GTGCCACCTG ACGTCTAAGA AACCATTATT ATCATGACAT TAACCTATAA AAATAGGCGT ATCACGAGGC CCTTTCGTCT TCAAGAA ','Copy number: 40',22,'001403078033625','001403078033625','YES','FOWARD'),(122,'PART122','test3',6,'test3','test3',31,'001432080998899','001432080998899','NO','FOWARD'),(123,'PART123','test4',6,'test4','test4',31,'001432081013204','001432081013204','NO','FOWARD'),(124,'PART124','test5',6,NULL,NULL,31,'001432081146291','001432081146291','NO','FOWARD'),(127,'PART127','test8',6,NULL,NULL,31,'001432081173704','001433296651979','NO','FOWARD'),(128,'PART128','test9',6,NULL,NULL,31,'001432081183408','001433296930631','NO','FOWARD'),(129,'PART129','test10',6,NULL,NULL,31,'001432081193002','001433296930758','NO','FOWARD'),(130,'PART130','test11',8,NULL,NULL,31,'001432081206180','001432081206180','NO','FOWARD'),(131,'PART131','test12',62,NULL,NULL,31,'001432081218114','001432081218114','NO','FOWARD'),(132,'PART132','test13',10,NULL,NULL,31,'001432081231914','001432081231914','NO','FOWARD'),(133,'PART133','test14',7,NULL,NULL,31,'001432081243153','001432081243153','YES','FOWARD'),(134,'PART134','test16',7,NULL,NULL,31,'001432081259767','001432081259767','YES','FOWARD'),(135,'PART135','test15',7,NULL,NULL,31,'001432081271767','001432081271767','NO','FOWARD'),(136,'PART136','test17',7,NULL,NULL,31,'001432081292143','001432081292143','NO','FOWARD'),(151,'PART151','test20',6,NULL,NULL,31,'001432100319226','001432100319226',NULL,'FOWARD'),(171,'PART171','bbbbtest',6,'aaaa','aa',41,'001433228921853','001433228921853','YES','FOWARD'),(172,'PART172','bbbbtest2',6,'bbbbb','bbbb',41,'001433229058734','001433229058734','YES','FOWARD'),(192,'PART192','bbbbtest2',6,'bbbbb','bbbb',31,'001433230761930','001433230761930','YES','FOWARD'),(193,'PART193','Ampicillin',63,'ATGAGTATTC AACATTTCCG TGTCGCCCTT ATTCCCTTTT TTGCGGCATT TTGCCTTCCT GTTTTTGCTC ACCCAGAAAC GCTGGTGAAA GTAAAAGATG CTGAAGATCA GTTGGGTGCA CGAGTGGGTT ACATCGAACT GGATCTCAAC AGCGGTAAGA TCCTTGAGAG TTTTCGCCCC GAAGAACGTT TTCCAATGAT GAGCACTTTT AAAGTTCTGC TATGTGGCGC GGTATTATCC CGTGTTGACG CCGGGCAAGA GCAACTCGGT CGCCGCATAC ACTATTCTCA GAATGACTTG GTTGAGTACT CACCAGTCAC AGAAAAGCAT CTTACGGATG GCATGACAGT AAGAGAATTA TGCAGTGCTG CCATAACCAT GAGTGATAAC ACTGCGGCCA ACTTACTTCT GACAACGATC GGAGGACCGA AGGAGCTAAC CGCTTTTTTG CACAACATGG GGGATCATGT AACTCGCCTT GATCGTTGGG AACCGGAGCT GAATGAAGCC ATACCAAACG ACGAGCGTGA CACCACGATG CCTGCAGCAA TGGCAACAAC GTTGCGCAAA CTATTAACTG GCGAACTACT TACTCTAGCT TCCCGGCAAC AATTAATAGA CTGGATGGAG GCGGATAAAG TTGCAGGACC ACTTCTGCGC TCGGCCCTTC CGGCTGGCTG GTTTATTGCT GATAAATCTG GAGCCGGTGA GCGTGGGTCT CGCGGTATCA TTGCAGCACT GGGGCCAGAT GGTAAGCCCT CCCGTATCGT AGTTATCTAC ACGACGGGGA GTCAGGCAAC TATGGATGAA CGAAATAGAC AGATCGCTGA GATAGGTGCC TCACTGATTA AGCATTGGTA A ','Ampicillin',31,'001433230877947','001433230877947','YES','FOWARD'),(201,'PART201','test14',7,NULL,NULL,41,'001433468117314','001433468117314','YES','FOWARD'),(211,'PART211','TRC',6,'atgcTGTTTGACAGCTTATCATCGATAAGCTTTAATGCGGTAGTTTATCACAGTTAAATTGCTAACGCAGTCAGGCAC\nCGTGTATGAAATCTAACAATGCGCTCATCGTCATCCTCGGCACCGTCACCCTGGATGCTGTAGGCATAGGCTTGGTTATG\nCCGGTACTGCCGGGCCTCTTGCGGGATATCCGGATATAGTTCCTCCTTTCAGCAAAAAACCCCTCAAGACCCGTTTAGAG\nGCCCCAAGGGGTTATGCTAGTTATTGCTCAGCGGTGGCAGCAGCCAACTCAGCTTCCTTTCGGGCTTTGTTAGCAGCCGG\nATCAAAGTGCCCCGGAGGATGAGATTTTCTTAAAGCGGTTGGCTGCTGAGACGGCTATGAAATTCTTTTTCCATCGTCGC\nTTGGTACCATTCCCACGTGGATACATGGATCTACCAAGTGATGCGCGTCTAAGGGATCCGACCCATTTGCTGTCCACCAG\nTCATGCTAGCCATGGTATATCTCCTTCTTAAAGTTAAACAAAATTATTTCTAGAGGGGAATTGTTATCCGCTCACAATTC\nCCCTATAGTGAGTCGTATTAATTTCGCGGGATCGAGATCTCGATCCTCTACGCCGGACGCATCGTGGCCGGCATCACCGG\nCGCCACAGGTGCGGTTGCTGGCGCCTATATCGCCGACATCACCGATGGGGAAGATCGGGCTCGCCACTTCGGGCTCATGA\nGCGCTTGTTTCGGCGTGGGTATGGTGGCAGGCCCCGTGGCCGGGGGACTGTTGGGCGCCATCTCCTTGCATGCACCATTC\nCTTGCGGCGGCGGTGCTCAACGGCCTCAACCTACTACTGGGCTGCTTCCTAATGCAGGAGTCGCATAAGGGAGAGCGTCG\nAGATCCCGGACACCATCGAATGGCGCAAAACCTTTCGCGGTATGGCATGATAGCGCCCGGAAGAGAGTCAATTCAGGGTG\nGTGAATGTGAAACCAGTAACGTTATACGATGTCGCAGAGTATGCCGGTGTCTCTTATCAGACCGTTTCCCGCGTGGTGAA\nCCAGGCCAGCCACGTTTCTGCGAAAACGCGGGAAAAAGTGGAAGCGGCGATGGCGGAGCTGAATTACATTCCCAACCGCG\nTGGCACAACAACTGGCGGGCAAACAGTCGTTGCTGATTGGCGTTGCCACCTCCAGTCTGGCCCTGCACGCGCCGTCGCAA\nATTGTCGCGGCGATTAAATCTCGCGCCGATCAACTGGGTGCCAGCGTGGTGGTGTCGATGGTAGAACGAAGCGGCGTCGA\nAGCCTGTAAAGCGGCGGTGCACAATCTTCTCGCGCAACGCGTCAGTGGGCTGATCATTAACTATCCGCTGGATGACCAGG\nATGCCATTGCTGTGGAAGCTGCCTGCACTAATGTTCCGGCGTTATTTCTTGATGTCTCTGACCAGACACCCATCAACAGT\nATTATTTTCTCCCATGAAGACGGTACGCGACTGGGCGTGGAGCATCTGGTCGCATTGGGTCACCAGCAAATCGCGCTGTT\nAGCGGGCCCATTAAGTTCTGTCTCGGCGCGTCTGCGTCTGGCTGGCTGGCATAAATATCTCACTCGCAATCAAATTCAGC\nCGATAGCGGAACGGGAAGGCGACTGGAGTGCCATGTCCGGTTTTCAACAAACCATGCAAATGCTGAATGAGGGCATCGTT\nCCCACTGCGATGCTGGTTGCCAACGATCAGATGGCGCTGGGCGCAATGCGCGCCATTACCGAGTCCGGGCTGCGCGTTGG\nTGCGGATATCTCGGTAGTGGGATACGACGATACCGAAGACAGCTCATGTTATATCCCGCCGTTAACCACCATCAAACAGG\nATTTTCGCCTGCTGGGGCAAACCAGCGTGGACCGCTTGCTGCAACTCTCTCAGGGCCAGGCGGTGAAGGGCAATCAGCTG\nTTGCCCGTCTCACTGGTGAAAAGAAAAACCACCCTGGCGCCCAATACGCAAACCGCCTCTCCCCGCGCGTTGGCCGATTC\nATTAATGCAGCTGGCACGACAGGTTTCCCGACTGGAAAGCGGGCAGTGAGCGCAACGCAATTAATGTAAGTTAGCTCACT\nCATTAGGCACCGGGATCTCGACCGATGCCCTTGAGAGCCTTCAACCCAGTCAGCTCCTTCCGGTGGGCGCGGGGCATGAC\nTATCGTCGCCGCACTTATGACTGTCTTCTTTATCATGCAACTCGTAGGACAGGTGCCGGCAGCGCTCTGGGTCATTTTCG\nGCGAGGACCGCTTTCGCTGGAGCGCGACGATGATCGGCCTGTCGCTTGCGGTATTCGGAATCTTGCACGCCCTCGCTCAA\nGCCTTCGTCACTGGTCCCGCCACCAAACGTTTCGGCGAGAAGCAGGCCATTATCGCCGGCATGGCGGCCGACGCGCTGGG\nCTACGTCTTGCTGGCGTTCGCGACGCGAGGCTGGATGGCCTTCCCCATTATGATTCTTCTCGCTTCCGGCGGCATCGGGA\nTGCCCGCGTTGCAGGCCATGCTGTCCAGGCAGGTAGATGACGACCATCAGGGACAGCTTCAAGGATCGCTCGCGGCTCTT\nACCAGCCTAACTTCGATCACTGGACCGCTGATCGTCACGGCGATTTATGCCGCCTCGGCGAGCACATGGAACGGGTTGGC\nATGGATTGTAGGCGCCGCCCTATACCTTGTCTGCCTCCCCGCGTTGCGTCGCGGTGCATGGAGCCGGGCCACCTCGACCT\nGAATGGAAGCCGGCGGCACCTCGCTAACGGATTCACCACTCCAAGAATTGGAGCCAATCAATTCTTGCGGAGAACTGTGA\nATGCGCAAACCAACCCTTGGCAGAACATATCCATCGCGTCCGCCATCTCCAGCAGCCGCACGCGGCGCATCTCGGGCAGC\nGTTGGGTCCTGGCCACGGGTGCGCATGATCGTGCTCCTGTCGTTGAGGACCCGGCTAGGCTGGCGGGGTTGCCTTACTGG\nTTAGCAGAATGAATCACCGATACGCGAGCGAACGTGAAGCGACTGCTGCTGCAAAACGTCTGCGACCTGAGCAACAACAT\nGAATGGTCTTCGGTTTCCGTGTTTCGTAAAGTCTGGAAACGCGGAAGTCAGCGCCCTGCACCATTATGTTCCGGATCTGC\nATCGCAGGATGCTGCTGGCTACCCTGTGGAACACCTACATCTGTATTAACGAAGCGCTGGCATTGACCCTGAGTGATTTT\nTCTCTGGTCCCGCCGCATCCATACCGCCAGTTGTTTACCCTCACAACGTTCCAGTAACCGGGCATGTTCATCATCAGTAA\nCCCGTATCGTGAGCATCCTCTCTCGTTTCATCGGTATCATTACCCCCATGAACAGAAATCCCCCTTACACGGAGGCATCA\nGTGACCAAACAGGAAAAAACCGCCCTTAACATGGCCCGCTTTATCAGAAGCCAGACATTAACGCTTCTGGAGAAACTCAA\nCGAGCTGGACGCGGATGAACAGGCAGACATCTGTGAATCGCTTCACGACCACGCTGATGAGCTTTACCGCAGCTGCCTCG\nCGCGTTTCGGTGATGACGGTGAAAACCTCTGACACATGCAGCTCCCGGAGACGGTCACAGCTTGTCTGTAAGCGGATGCC\nGGGAGCAGACAAGCCCGTCAGGGCGCGTCAGCGGGTGTTGGCGGGTGTCGGGGCGCAGCCATGACCCAGTCACGTAGCGA\nTAGCGGAGTGTATACTGGCTTAACTATGCGGCATCAGAGCAGATTGTACTGAGAGTGCACCATATATGCGGTGTGAAATA\nCCGCACAGATGCGTAAGGAGAAAATACCGCATCAGGCGCTCTTCCGCTTCCTCGCTCACTGACTCGCTGCGCTCGGTCGT\nTCGGCTGCGGCGAGCGGTATCAGCTCACTCAAAGGCGGTAATACGGTTATCCACAGAATCAGGGGATAACGCAGGAAAGA\nACATGTGAGCAAAAGGCCAGCAAAAGGCCAGGAACCGTAAAAAGGCCGCGTTGCTGGCGTTTTTCCATAGGCTCCGCCCC\nCCTGACGAGCATCACAAAAATCGACGCTCAAGTCAGAGGTGGCGAAACCCGACAGGACTATAAAGATACCAGGCGTTTCC\nCCCTGGAAGCTCCCTCGTGCGCTCTCCTGTTCCGACCCTGCCGCTTACCGGATACCTGTCCGCCTTTCTCCCTTCGGGAA\nGCGTGGCGCTTTCTCATAGCTCACGCTGTAGGTATCTCAGTTCGGTGTAGGTCGTTCGCTCCAAGCTGGGCTGTGTGCAC\nGAACCCCCCGTTCAGCCCGACCGCTGCGCCTTATCCGGTAACTATCGTCTTGAGTCCAACCCGGTAAGACACGACTTATC\nGCCACTGGCAGCAGCCACTGGTAACAGGATTAGCAGAGCGAGGTATGTAGGCGGTGCTACAGAGTTCTTGAAGTGGTGGC\nCTAACTACGGCTACACTAGAAGGACAGTATTTGGTATCTGCGCTCTGCTGAAGCCAGTTACCTTCGGAAAAAGAGTTGGT\nAGCTCTTGATCCGGCAAACAAACCACCGCTGGTAGCGGTGGTTTTTTTGTTTGCAAGCAGCAGATTACGCGCAGAAAAAA\nAGGATCTCAAGAAGATCCTTTGATCTTTTCTACGGGGTCTGACGCTCAGTGGAACGAAAACTCACGTTAAGGGATTTTGG\nTCATGAGATTATCAAAAAGGATCTTCACCTAGATCCTTTTAAATTAAAAATGAAGTTTTAAATCAATCTAAAGTATATAT\nGAGTAAACTTGGTCTGACAGTTACCAATGCTTAATCAGTGAGGCACCTATCTCAGCGATCTGTCTATTTCGTTCATCCAT\nAGTTGCCTGACTCCCCGTCGTGTAGATAACTACGATACGGGAGGGCTTACCATCTGGCCCCAGTGCTGCAATGATACCGC\nGAGACCCACGCTCACCGGCTCCAGATTTATCAGCAATAAACCAGCCAGCCGGAAGGGCCGAGCGCAGAAGTGGTCCTGCA\nACTTTATCCGCCTCCATCCAGTCTATTAATTGTTGCCGGGAAGCTAGAGTAAGTAGTTCGCCAGTTAATAGTTTGCGCAA\nCGTTGTTGCCATTGCTGCAGGCATCGTGGTGTCACGCTCGTCGTTTGGTATGGCTTCATTCAGCTCCGGTTCCCAACGAT\nCAAGGCGAGTTACATGATCCCCCATGTTGTGCAAAAAAGCGGTTAGCTCCTTCGGTCCTCCGATCGTTGTCAGAAGTAAG\nTTGGCCGCAGTGTTATCACTCATGGTTATGGCAGCACTGCATAATTCTCTTACTGTCATGCCATCCGTAAGATGCTTTTC\nTGTGACTGGTGAGTACTCAACCAAGTCATTCTGAGAATAGTGTATGCGGCGACCGAGTTGCTCTTGCCCGGCGTCAACAC\nGGGATAATACCGCGCCACATAGCAGAACTTTAAAAGTGCTCATCATTGGAAAACGTTCTTCGGGGCGAAAACTCTCAAGG\nATCTTACCGCTGTTGAGATCCAGTTCGATGTAACCCACTCGTGCACCCAACTGATCTTCAGCATCTTTTACTTTCACCAG\nCGTTTCTGGGTGAGCAAAAACAGGAAGGCAAAATGCCGCAAAAAAGGGAATAAGGGCGACACGGAAATGTTGAATACTCA\nTACTCTTCCTTTTTCAATATTATTGAAGCATTTATCAGGGTTATTGTCTCATGAGCGGATACATATTTGAATGTATTTAG\nAAAAATAAACAAATAGGGGTTCCGCGCACATTTCCCCGAAAAGTGCCACCTGACGTCTAAGAAACCATTATTATCATGAC\nATTAACCTATAAAAATAGGCGTATCACGAGGCCCTTTCGTCTTCAAGAAA\n','from pTRC99a vector\nw/ RBS\n',31,'001433741608083','001433741608083','YES','FOWARD'),(212,'PART212','HCE',6,'GATCTCTCCT TCACAGATTC CCAATCTCTT GTTAAATAAC GAAAAAGCAT CAATCAAAAC GGCGGCATGT CTTTCTATAT TCCAGCAATG TTTTATAGGG GACATATTGA TGAAGATGGG TATCACCTTA GTAAAAAAAG AATTGCTATA AGCTGCTCTT TTTTGTTCGT GATATACTGA TAATAAATTG AATTTTCACA CTTCTGGAAA AAGGAGATAT CA ',NULL,31,'001433741608375','001433741608375','YES','FOWARD'),(221,'PART221','dmpR(E135K)',61,'ATGCCGATCA AGTACAAGCC TGAAATCCAG CACTCCGATT TCAAGGACCT GACCAACCTG ATCCACTTCC AGAGCACGGA AGGCAAGATC TGGCTTGGCG AACAACGCAT GCTGTTGCTG CAGGTTTCAG CAATGGCCAG CTTTCGCCGG GAAATGGTCA ATACCCTGGG CATCGAACGC GCCAAGGGCT TCTTCCTGCG CCAGGGTTAC CAGTCCGGCC TGAAGGATGC CGAACTGGCC AGGAAGCTTA GACCGAATGC CAGCGAGTAC GACATGTTCC TCGCCGGCCC GCAGCTGCAT TCGCTCAAGG GTCTGGTCAA GGTCCGCCCC ACCGAGGTCG ATATCGACAA GGAATGCGGG CGCTTCTATG CCGAGATGGA GTGGATCGAC TCCTTCGAGG TGAAAATCTG CCAGACCGAC CTGGGGCAGA TGCAAGACCC GGTGTGCTGG ACTCTGCTCG GCTACGCCTG CGCCTATTCC TCGGCGTTCA TGGGCCGGGA AATCATCTTC AAGGAAGTCA GCTGCCGCGG CTGCGGCGGC GACAAGTGCC GGGTCATTGG CAAGCCGGCC GAAGAGTGGG ACGACGTTGC CAGCTTCAAA CAGTATTTCA AGAACGACCC CATCATCGAG GAACTCTACG AGTTGCAATC GCAACTGGTG TCGCTGCGTA CCAACCTCGA CAAACAGGAA GGCCAGTACT ACGGCATCGG TCAGACCCCG GCCTACCAGA CCGTGCGCAA TATGATGGAC AAGGCCGCAC AGGGCAAAGT CTCGGTGCTG CTGCTTGGCG AGACCGGGGT CGGCAAGGAG GTCATCGCGC GTAGCGTGCA CCTGCGCAGC AAACGCGCCG CCGAGCCCTT TGTCGCGGTG AACTGTGCGG CGATCCCGCC GGACCTGATC GAGTCCGAAT TGTTCGGCGT GGAAAAAGGC GCCTTCACCG GCGCCACCCA GTCACGCATG GGCCGCTTCG AGCGGGCCGA CAAGGGCACC ATCTTCCTTG ACGAGGTGAT CGAACTCAGC CCGCGCGCTC AGGCCAGTTT GCTGCGCGTG CTGCAAGAAG GCGAGCTGGA GCGAGTTGGC GACAACCGCA CGCGCAAGAT CGACGTAAGG GTTATCGCAG CCACCCACGA GGACCTGGCC GAAGCGGTCA AGGCCGGGCG TTTTCGCGCC GACCTGTACT ACCGGCTGAA CGTTTTCCCG GTGGCGATCC CGGCGTTGCG CGAACGCCGC GAGGACATTC CACTGCTGGT TGAGCACTTC CTTCAGCGCT TCCACCAGGA GTACGGCAAG AGAACCCTCG GCCTTTCAGA CAAAGCCCTG GAGGCCTGCC TGCATTACAG TTGGCCGGGC AATATCCGTG AGCTGGAGAA CGTCATCGAG CGCGGCATCA TCCTCACCGA TCCGAACGAA AGCATCAGCG TGCAGGCGCT GTTCCCACGG GCGCCGGAAG AGCCGCAGAC CGCCAGCGAG CGGGTGTCGT CGGACGGCGT GCTGATTCAG CCAGGCAATG GCCAGGGCAG TTGGATCAGC CAGTTGTTGA GCAGCGGCCT GAGCCTCGAC GAGATCGAGG AAAGCCTGAT GCGCAAAGCC ATGCAACAGG CCAACCAAAA CGTCTCCGGT GCCGCGCGCT TGCTCGGCCT AAGCCGACCG GCACTGGCCT ATCGGCTGAA GAAAATCGGC ATCGAAGGCT AG ','Source: Pseudomonas putida KCTC1452\nDifferent from shingler\'s dmpR.',31,'001433749508775','001433749508775','YES','FOWARD'),(231,'PART231','Tl3 terminator',11,'AATGGCGATG ACGCATCCTC ACGATAATAT CCGGGTAGGC GCAATCACTT TCGTCTACTC CGTTACAAAG CGAGGCTGGG TATTTCCCGG CCTTTCTGTT ATCCGAAATC CACTGAAAGC ACAGCGGCTG GCTGAGGAGA TAAATAATAA ACGAGGGGCT GTATGCACAA AGCATCTTCT GTTGAGTTAA GAACGAGTAT CGAGATGGCA CATAGCCTTG CTCAAATTGG AATCAGGTTT GTGCCAATAC CAGTAGAAAC AGACGAAGAA ','Tl3 terminator',41,'001433926327688','001433927353811','YES','FOWARD'),(241,'PART241','test111111',6,'aaaaaaaaaaaaaaaaaaaaaaaaaaa',NULL,31,'001441598425460','001441598425460','YES','FOWARD');
/*!40000 ALTER TABLE `Part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartDesigner`
--

DROP TABLE IF EXISTS `PartDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartDesigner` (
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `userID` bigint(20) NOT NULL COMMENT '부품개발자 ID',
  UNIQUE KEY `partID` (`partID`,`userID`),
  KEY `PARTDESIGNER_REFS_USERID` (`userID`),
  CONSTRAINT `PARTDESIGNER_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`),
  CONSTRAINT `PARTDESIGNER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartDesigner`
--

LOCK TABLES `PartDesigner` WRITE;
/*!40000 ALTER TABLE `PartDesigner` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartDesigner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartProp`
--

DROP TABLE IF EXISTS `PartProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartProp` (
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `propName` varchar(128) NOT NULL COMMENT '부품 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '부품 속성 값',
  UNIQUE KEY `partID` (`partID`,`propName`),
  KEY `PartProp_PName_IDX` (`propName`),
  KEY `PartProp_PValue_IDX` (`propValue`),
  CONSTRAINT `PARTPROP_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartProp`
--

LOCK TABLES `PartProp` WRITE;
/*!40000 ALTER TABLE `PartProp` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartProp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartReferences`
--

DROP TABLE IF EXISTS `PartReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartReferences` (
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `partID` (`partID`,`articleID`),
  KEY `PARTREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `PARTREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `PARTREFERENCES_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartReferences`
--

LOCK TABLES `PartReferences` WRITE;
/*!40000 ALTER TABLE `PartReferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartReferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Protocol`
--

DROP TABLE IF EXISTS `Protocol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Protocol` (
  `protocolID` bigint(20) NOT NULL COMMENT '실험 프로토콜 ID',
  `name` varchar(64) NOT NULL COMMENT '실험 프로토콜 이름, Device 이름으로 대체할 수 있음',
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `strainID` bigint(20) NOT NULL COMMENT 'Strain ID',
  `media` bigint(20) DEFAULT '1',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '실험 프로토콜 등록 사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`protocolID`),
  KEY `EP_REFS_DEVICEID` (`deviceID`),
  KEY `EP_REFS_STRAINID` (`strainID`),
  KEY `EP_REFS_MEDIA` (`media`),
  KEY `EP_REFS_USERID` (`userID`),
  KEY `Protocol_Name_IDX` (`name`),
  CONSTRAINT `EP_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`),
  CONSTRAINT `EP_REFS_MEDIA` FOREIGN KEY (`media`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `EP_REFS_STRAINID` FOREIGN KEY (`strainID`) REFERENCES `Strain` (`strainID`),
  CONSTRAINT `EP_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비에 의한 실험프로토콜 정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Protocol`
--

LOCK TABLES `Protocol` WRITE;
/*!40000 ALTER TABLE `Protocol` DISABLE KEYS */;
INSERT INTO `Protocol` VALUES (61,'phGESS',21,1,132,'Seed 1%\nCulture 1mL\n37oC\n',22,'001402358323248','001406687776333','YES'),(71,'aaa',21,1,133,'reteste',22,'001406701898179','001406701898179','YES'),(123,'phGESS',21,1,132,'Seed 1%\nCulture 1mL\n37oC\n',31,'001441783632244','001441783632244','NO'),(181,'ABCD',91,1,1,'sadfasdf',31,'001446082192780','001446082192780','YES'),(182,'TFCD',111,21,1,'wfsdf',31,'001446082215968','001446082215968','YES');
/*!40000 ALTER TABLE `Protocol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProtocolDesigner`
--

DROP TABLE IF EXISTS `ProtocolDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProtocolDesigner` (
  `protocolID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `userID` bigint(20) NOT NULL COMMENT '프로토콜 개발자 ID',
  UNIQUE KEY `protocolID` (`protocolID`,`userID`),
  KEY `EPD_REFS_USERID` (`userID`),
  CONSTRAINT `EPD_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`),
  CONSTRAINT `EPD_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 프로토콜 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProtocolDesigner`
--

LOCK TABLES `ProtocolDesigner` WRITE;
/*!40000 ALTER TABLE `ProtocolDesigner` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProtocolDesigner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProtocolProp`
--

DROP TABLE IF EXISTS `ProtocolProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProtocolProp` (
  `protocolID` bigint(20) NOT NULL COMMENT '실험결과 ID',
  `propName` varchar(64) NOT NULL COMMENT '실험결과 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '실험결과 속성 값',
  UNIQUE KEY `protocolID` (`protocolID`,`propName`),
  KEY `ProtocolProp_PID_IDX` (`protocolID`),
  KEY `ProtocolProp_PName_IDX` (`propName`),
  KEY `ProtocolProp_PValue_IDX` (`propValue`),
  CONSTRAINT `EPP_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 속성 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProtocolProp`
--

LOCK TABLES `ProtocolProp` WRITE;
/*!40000 ALTER TABLE `ProtocolProp` DISABLE KEYS */;
INSERT INTO `ProtocolProp` VALUES (71,'Culture time','1'),(61,'Culture time','10'),(61,'Temperature','10'),(123,'Culture time','10'),(123,'Temperature','10'),(71,'Temperature','32');
/*!40000 ALTER TABLE `ProtocolProp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProtocolReferences`
--

DROP TABLE IF EXISTS `ProtocolReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProtocolReferences` (
  `protocolID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `protocolID` (`protocolID`,`articleID`),
  KEY `EPR_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `EPR_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `EPR_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 프로토콜 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProtocolReferences`
--

LOCK TABLES `ProtocolReferences` WRITE;
/*!40000 ALTER TABLE `ProtocolReferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProtocolReferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequenceID`
--

DROP TABLE IF EXISTS `SequenceID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SequenceID` (
  `idType` int(11) NOT NULL COMMENT 'ID type',
  `id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'next id seqeunce'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ID 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequenceID`
--

LOCK TABLES `SequenceID` WRITE;
/*!40000 ALTER TABLE `SequenceID` DISABLE KEYS */;
INSERT INTO `SequenceID` VALUES (800,191),(701,241),(601,141),(602,71),(700,131),(500,251),(600,141),(400,81),(200,141),(900,1451),(100,861),(201,21),(901,51),(202,701),(203,1601),(820,31),(810,91),(1,201611);
/*!40000 ALTER TABLE `SequenceID` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Simulation`
--

DROP TABLE IF EXISTS `Simulation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Simulation` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `protocolID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `name` varchar(128) DEFAULT NULL COMMENT '실험결과 이름',
  `cultureTime` float DEFAULT '0' COMMENT '배양시간',
  `duration` float DEFAULT '0' COMMENT '확인 간격',
  `status` varchar(15) NOT NULL DEFAULT 'ready' COMMENT '상태정보 (ready, running, complete, error)',
  `startTime` varchar(15) DEFAULT NULL COMMENT '시뮬레이션 시작 시간',
  `endTime` varchar(15) DEFAULT NULL COMMENT '시뮬레이션 종료 시간',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일시',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일시',
  PRIMARY KEY (`simulationID`),
  KEY `SIMUL_REFS_PROTOCOLID` (`protocolID`),
  KEY `SIMUL_REFS_USERID` (`userID`),
  KEY `SIMUL_Name_IDX` (`name`),
  CONSTRAINT `SIMUL_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`),
  CONSTRAINT `SIMUL_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Simulation`
--

LOCK TABLES `Simulation` WRITE;
/*!40000 ALTER TABLE `Simulation` DISABLE KEYS */;
INSERT INTO `Simulation` VALUES (1,61,'test-simulation',10,10,'Ready',NULL,NULL,NULL,1,'001406481518602','001406481518602'),(21,61,'20140730',0,0,'Ready',NULL,NULL,NULL,1,'001406681497731','001406697684338');
/*!40000 ALTER TABLE `Simulation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimulationInput`
--

DROP TABLE IF EXISTS `SimulationInput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimulationInput` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `componentID` bigint(20) NOT NULL COMMENT 'DeviceComponent 테이블의 입력에 해당하는 부품 ID',
  `quantity` double DEFAULT '0' COMMENT '입력 수치',
  `unit` varchar(10) DEFAULT NULL COMMENT '수치 단위',
  UNIQUE KEY `simulationID` (`simulationID`,`componentID`),
  KEY `SIMULIN_REFS_COMPID` (`componentID`),
  CONSTRAINT `SIMULIN_REFS_COMPID` FOREIGN KEY (`componentID`) REFERENCES `DeviceComponent` (`componentID`),
  CONSTRAINT `SIMULIN_REFS_SIMULID` FOREIGN KEY (`simulationID`) REFERENCES `Simulation` (`simulationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 입력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimulationInput`
--

LOCK TABLES `SimulationInput` WRITE;
/*!40000 ALTER TABLE `SimulationInput` DISABLE KEYS */;
INSERT INTO `SimulationInput` VALUES (1,112,0,NULL),(21,112,1213213,NULL);
/*!40000 ALTER TABLE `SimulationInput` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimulationOutput`
--

DROP TABLE IF EXISTS `SimulationOutput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimulationOutput` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `name` varchar(128) NOT NULL COMMENT '출력정보 이름',
  `mean` double DEFAULT '0' COMMENT '평균 출력 값',
  `variance` double DEFAULT '0' COMMENT '분산',
  UNIQUE KEY `simulationID` (`simulationID`,`name`),
  KEY `SIMULOUT_Name_IDX` (`name`),
  CONSTRAINT `SIMULOUT_REFS_SIMULID` FOREIGN KEY (`simulationID`) REFERENCES `Simulation` (`simulationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 출력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimulationOutput`
--

LOCK TABLES `SimulationOutput` WRITE;
/*!40000 ALTER TABLE `SimulationOutput` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimulationOutput` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimulationParameters`
--

DROP TABLE IF EXISTS `SimulationParameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimulationParameters` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `parameters` varchar(140) NOT NULL COMMENT 'protocol + substrate + product 조합으로 된 파라미터 이름',
  `substrates` varchar(1000) NOT NULL COMMENT '콤마(,)로 연결된  input unit들의 이름',
  `products` varchar(1000) NOT NULL COMMENT '콤마(,)로 연결된 output unit들의 이름',
  `value` double NOT NULL DEFAULT '0' COMMENT '반응 비율 등의 반응수치',
  UNIQUE KEY `simulationID` (`simulationID`,`parameters`),
  CONSTRAINT `SIMULPARAM_REFS_SIMULID` FOREIGN KEY (`simulationID`) REFERENCES `Simulation` (`simulationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 입력 파라미터 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimulationParameters`
--

LOCK TABLES `SimulationParameters` WRITE;
/*!40000 ALTER TABLE `SimulationParameters` DISABLE KEYS */;
INSERT INTO `SimulationParameters` VALUES (1,'phGESS-RNA polymerase-Sensor module:Gene_sensor:dmpR(WT)','RNA polymerase','Sensor module:Gene_sensor:dmpR(WT)',0),(1,'phGESS-Sensor module:Gene_sensor:dmpR(WT)-Reporter module:Gene_reporter:EGFP','Sensor module:Gene_sensor:dmpR(WT)','Reporter module:Gene_reporter:EGFP',0),(21,'phGESS-RNA polymerase-Sensor module:Gene_sensor:dmpR(WT)','RNA polymerase','Sensor module:Gene_sensor:dmpR(WT)',1),(21,'phGESS-Sensor module:Gene_sensor:dmpR(WT)-RNA polymerase','Sensor module:Gene_sensor:dmpR(WT)','RNA polymerase',2);
/*!40000 ALTER TABLE `SimulationParameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Strain`
--

DROP TABLE IF EXISTS `Strain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Strain` (
  `strainID` bigint(20) NOT NULL COMMENT 'strain growth model ID',
  `name` varchar(64) NOT NULL COMMENT 'strain 이름',
  `meanTime` float DEFAULT '0' COMMENT '평균 배양 시간',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '생성일시',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일시',
  `shared` varchar(3) DEFAULT 'YES',
  PRIMARY KEY (`strainID`),
  KEY `SGM_REFS_USERID` (`userID`),
  KEY `Strain_Name_IDX` (`name`),
  CONSTRAINT `SGM_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Strain Growth Model 정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Strain`
--

LOCK TABLES `Strain` WRITE;
/*!40000 ALTER TABLE `Strain` DISABLE KEYS */;
INSERT INTO `Strain` VALUES (1,'sample-strain',10,'sample strain',1,'001406687676443','001406687676443','YES'),(11,'test',0,'test',31,'001431479664871','001431479664871','NO'),(21,'test[2]',0,'test[2]',31,'001432081048643','001432081048643','NO'),(22,'test[3]',0,NULL,31,'001432081058769','001432081058769','NO'),(23,'test[4]',0,NULL,31,'001432081068957','001432081068957','NO'),(24,'test[5]',0,NULL,31,'001432081077783','001432081077783','NO'),(25,'test[6]',0,NULL,31,'001432081088071','001432081088071','NO'),(26,'test[7]',0,NULL,31,'001432081102511','001432081102511','NO'),(27,'test[8]',0,NULL,31,'001432081111782','001432081111782','NO'),(28,'test[9]',0,NULL,31,'001432081121879','001432081121879','NO'),(29,'test[10]',0,NULL,31,'001432081132879','001432081132879','YES'),(31,'test[11]',0,NULL,31,'001432099654910','001432099654910',NULL),(32,'test[12]',0,NULL,31,'001432099724294','001432099724294',NULL),(41,'test[15]',0,NULL,31,'001432100010471','001432100010471',NULL),(42,'test[17]',0,NULL,31,'001432100097241','001432100097241',NULL),(51,'test[18]',0,NULL,31,'001432103766031','001432103766031',NULL),(52,'test[20]',0,NULL,31,'001432104204025','001432104204025','NO'),(53,'test[21]',0,NULL,31,'001432104216677','001432104216677','YES'),(61,'test[10]',0,NULL,41,'001433468202158','001433468202158','YES'),(81,'test134679',0,NULL,31,'001438676768304','001438676768304',NULL);
/*!40000 ALTER TABLE `Strain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StrainAttachment`
--

DROP TABLE IF EXISTS `StrainAttachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StrainAttachment` (
  `strainID` bigint(20) NOT NULL COMMENT 'strain growth model ID',
  `attachmentID` bigint(20) NOT NULL COMMENT '첨부파일 ID',
  `type` varchar(15) NOT NULL DEFAULT 'output' COMMENT '첨부파일 종류 (결과파일, 기타...)',
  UNIQUE KEY `strainID` (`strainID`,`attachmentID`),
  KEY `SGMA_REFS_ATTACHID` (`attachmentID`),
  CONSTRAINT `SGMA_REFS_ATTACHID` FOREIGN KEY (`attachmentID`) REFERENCES `Attachment` (`attachmentID`),
  CONSTRAINT `SGMA_REFS_STRAINID` FOREIGN KEY (`strainID`) REFERENCES `Strain` (`strainID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Strain Growth Model 첨부파일 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StrainAttachment`
--

LOCK TABLES `StrainAttachment` WRITE;
/*!40000 ALTER TABLE `StrainAttachment` DISABLE KEYS */;
INSERT INTO `StrainAttachment` VALUES (1,72,'input');
/*!40000 ALTER TABLE `StrainAttachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tecan`
--

DROP TABLE IF EXISTS `Tecan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tecan` (
  `tecanID` bigint(20) NOT NULL,
  `experimentID` bigint(20) NOT NULL,
  `timeName` varchar(12) DEFAULT NULL,
  `labelName` varchar(20) NOT NULL,
  `colName` varchar(5) NOT NULL,
  `rowName` varchar(5) NOT NULL,
  `condition1` varchar(15) DEFAULT NULL,
  `inputName1` varchar(15) DEFAULT NULL,
  `condition2` varchar(15) DEFAULT NULL,
  `inputName2` varchar(15) DEFAULT NULL,
  `condition3` varchar(15) DEFAULT NULL,
  `inputName3` varchar(15) DEFAULT NULL,
  `result` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`tecanID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tecan`
--

LOCK TABLES `Tecan` WRITE;
/*!40000 ALTER TABLE `Tecan` DISABLE KEYS */;
INSERT INTO `Tecan` VALUES (0,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,0,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Tecan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `userID` bigint(20) NOT NULL COMMENT '사용자 ID',
  `username` varchar(32) NOT NULL COMMENT '사용자 이름',
  `name` varchar(32) DEFAULT NULL COMMENT '사용자 이름',
  `passwordHash` varchar(32) NOT NULL COMMENT '사용자 비밀번호',
  `email` varchar(64) DEFAULT NULL COMMENT '전자우편 주소',
  `affiliation` varchar(256) DEFAULT NULL COMMENT '소속정보',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`),
  KEY `User_Username_IDX` (`email`),
  KEY `User_Name_IDX` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='사용자 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'admin','admin','c93ccd78b2076528346216b3b2f701e6','admin@kribb.re.kr','KRIBB','001377489221670','001377489221670'),(11,'yang4851','ì–‘ì„±ì§„','a3e1edccfe0a88ad9a6d0eabc25e718f','yang4851@hanmail.net','Insilicogen.inc','001377882090685','001377882090685'),(21,'test','test','81dc9bdb52d04dc20036dbd8313ed055',NULL,NULL,'001378076269909','001378076269909'),(22,'haseong','haseong','f9c6af02247195e5b23f7bee1ff7b8ae',NULL,NULL,'001378079093244','001378079093244'),(31,'aaaa','aaaa','594f803b380a41396ed63dca39503542','aaaa@naver.com','aaaa','001429756086216','001429756086216'),(41,'bbbb','bbbb','a21075a36eeddd084e17611a238c7101','bbbb@naver.com','bbbbb','001433228883881','001433228883881'),(51,'test2','test2','ad0234829205b9033196ba818f7a872b',NULL,NULL,'001441846688038','001441846688038'),(61,'qwer','qwer','962012d09b8170d912f0669f6d7d9d07',NULL,NULL,'001441847191091','001441847191091');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Victor`
--

DROP TABLE IF EXISTS `Victor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Victor` (
  `victorID` bigint(20) NOT NULL,
  `experimentID` bigint(20) NOT NULL,
  `labelName` varchar(20) NOT NULL,
  `colName` varchar(5) NOT NULL,
  `rowName` varchar(5) NOT NULL,
  `condition1` varchar(15) DEFAULT NULL,
  `inputName1` varchar(15) DEFAULT NULL,
  `condition2` varchar(15) DEFAULT NULL,
  `inputName2` varchar(15) DEFAULT NULL,
  `condition3` varchar(15) DEFAULT NULL,
  `inputName3` varchar(15) DEFAULT NULL,
  `result` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`victorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Victor`
--

LOCK TABLES `Victor` WRITE;
/*!40000 ALTER TABLE `Victor` DISABLE KEYS */;
INSERT INTO `Victor` VALUES (661,1371,'595nm_kk (A)','A','01','5','1',NULL,NULL,NULL,NULL,'0.604'),(662,1371,'595nm_kk (A)','A','02','5','1',NULL,NULL,NULL,NULL,'0.611'),(663,1371,'595nm_kk (A)','A','03','5','1',NULL,NULL,NULL,NULL,'0.606'),(664,1371,'595nm_kk (A)','A','04','5','1',NULL,NULL,NULL,NULL,'0.592'),(665,1371,'595nm_kk (A)','A','05','5','1',NULL,NULL,NULL,NULL,'0.595'),(666,1371,'595nm_kk (A)','A','06','5','1',NULL,NULL,NULL,NULL,'0.567'),(667,1371,'595nm_kk (A)','A','07','5','1',NULL,NULL,NULL,NULL,'0.586'),(668,1371,'595nm_kk (A)','A','08','5','1',NULL,NULL,NULL,NULL,'0.584'),(669,1371,'595nm_kk (A)','A','09','5','1',NULL,NULL,NULL,NULL,'0.587'),(670,1371,'595nm_kk (A)','A','10','5','1',NULL,NULL,NULL,NULL,'0.591'),(671,1371,'595nm_kk (A)','A','11','5','1',NULL,NULL,NULL,NULL,'-'),(672,1371,'595nm_kk (A)','A','12','5','1',NULL,NULL,NULL,NULL,'-'),(673,1371,'595nm_kk (A)','B','01','5','1',NULL,NULL,NULL,NULL,'0.617'),(674,1371,'595nm_kk (A)','B','02','5','1',NULL,NULL,NULL,NULL,'0.605'),(675,1371,'595nm_kk (A)','B','03','5','1',NULL,NULL,NULL,NULL,'0.600'),(676,1371,'595nm_kk (A)','B','04','5','1',NULL,NULL,NULL,NULL,'0.621'),(677,1371,'595nm_kk (A)','B','05','5','1',NULL,NULL,NULL,NULL,'0.604'),(678,1371,'595nm_kk (A)','B','06','5','1',NULL,NULL,NULL,NULL,'0.616'),(679,1371,'595nm_kk (A)','B','07','5','1',NULL,NULL,NULL,NULL,'0.579'),(680,1371,'595nm_kk (A)','B','08','5','1',NULL,NULL,NULL,NULL,'0.655'),(681,1371,'595nm_kk (A)','B','09','5','1',NULL,NULL,NULL,NULL,'0.612'),(682,1371,'595nm_kk (A)','B','10','5','1',NULL,NULL,NULL,NULL,'0.589'),(683,1371,'595nm_kk (A)','B','11','5','1',NULL,NULL,NULL,NULL,'-'),(684,1371,'595nm_kk (A)','B','12','5','1',NULL,NULL,NULL,NULL,'-'),(685,1371,'595nm_kk (A)','C','01','5','1',NULL,NULL,NULL,NULL,'0.632'),(686,1371,'595nm_kk (A)','C','02','5','1',NULL,NULL,NULL,NULL,'0.579'),(687,1371,'595nm_kk (A)','C','03','5','1',NULL,NULL,NULL,NULL,'0.585'),(688,1371,'595nm_kk (A)','C','04','5','1',NULL,NULL,NULL,NULL,'0.603'),(689,1371,'595nm_kk (A)','C','05','5','1',NULL,NULL,NULL,NULL,'0.625'),(690,1371,'595nm_kk (A)','C','06','5','1',NULL,NULL,NULL,NULL,'0.603'),(691,1371,'595nm_kk (A)','C','07','5','1',NULL,NULL,NULL,NULL,'0.614'),(692,1371,'595nm_kk (A)','C','08','5','1',NULL,NULL,NULL,NULL,'0.619'),(693,1371,'595nm_kk (A)','C','09','5','1',NULL,NULL,NULL,NULL,'0.617'),(694,1371,'595nm_kk (A)','C','10','5','1',NULL,NULL,NULL,NULL,'0.644'),(695,1371,'595nm_kk (A)','C','11','5','1',NULL,NULL,NULL,NULL,'-'),(696,1371,'595nm_kk (A)','C','12','5','1',NULL,NULL,NULL,NULL,'-'),(697,1371,'595nm_kk (A)','D','01','5','1',NULL,NULL,NULL,NULL,'0.631'),(698,1371,'595nm_kk (A)','D','02','5','1',NULL,NULL,NULL,NULL,'0.591'),(699,1371,'595nm_kk (A)','D','03','5','1',NULL,NULL,NULL,NULL,'0.624'),(700,1371,'595nm_kk (A)','D','04','5','1',NULL,NULL,NULL,NULL,'0.626'),(701,1371,'595nm_kk (A)','D','05','5','1',NULL,NULL,NULL,NULL,'0.616'),(702,1371,'595nm_kk (A)','D','06','5','1',NULL,NULL,NULL,NULL,'0.648'),(703,1371,'595nm_kk (A)','D','07','5','1',NULL,NULL,NULL,NULL,'0.597'),(704,1371,'595nm_kk (A)','D','08','5','1',NULL,NULL,NULL,NULL,'0.602'),(705,1371,'595nm_kk (A)','D','09','5','1',NULL,NULL,NULL,NULL,'0.635'),(706,1371,'595nm_kk (A)','D','10','5','1',NULL,NULL,NULL,NULL,'0.595'),(707,1371,'595nm_kk (A)','D','11','5','1',NULL,NULL,NULL,NULL,'-'),(708,1371,'595nm_kk (A)','D','12','5','1',NULL,NULL,NULL,NULL,'-'),(709,1371,'595nm_kk (A)','E','01','5','1',NULL,NULL,NULL,NULL,'0.641'),(710,1371,'595nm_kk (A)','E','02','5','1',NULL,NULL,NULL,NULL,'0.615'),(711,1371,'595nm_kk (A)','E','03','5','1',NULL,NULL,NULL,NULL,'0.582'),(712,1371,'595nm_kk (A)','E','04','5','1',NULL,NULL,NULL,NULL,'0.594'),(713,1371,'595nm_kk (A)','E','05','5','1',NULL,NULL,NULL,NULL,'0.612'),(714,1371,'595nm_kk (A)','E','06','5','1',NULL,NULL,NULL,NULL,'0.600'),(715,1371,'595nm_kk (A)','E','07','5','1',NULL,NULL,NULL,NULL,'0.604'),(716,1371,'595nm_kk (A)','E','08','5','1',NULL,NULL,NULL,NULL,'0.586'),(717,1371,'595nm_kk (A)','E','09','5','1',NULL,NULL,NULL,NULL,'0.582'),(718,1371,'595nm_kk (A)','E','10','5','1',NULL,NULL,NULL,NULL,'0.577'),(719,1371,'595nm_kk (A)','E','11','5','1',NULL,NULL,NULL,NULL,'-'),(720,1371,'595nm_kk (A)','E','12','5','1',NULL,NULL,NULL,NULL,'-'),(721,1371,'595nm_kk (A)','F','01','5','1',NULL,NULL,NULL,NULL,'0.636'),(722,1371,'595nm_kk (A)','F','02','5','1',NULL,NULL,NULL,NULL,'0.623'),(723,1371,'595nm_kk (A)','F','03','5','1',NULL,NULL,NULL,NULL,'0.637'),(724,1371,'595nm_kk (A)','F','04','5','1',NULL,NULL,NULL,NULL,'0.642'),(725,1371,'595nm_kk (A)','F','05','5','1',NULL,NULL,NULL,NULL,'0.542'),(726,1371,'595nm_kk (A)','F','06','5','1',NULL,NULL,NULL,NULL,'0.573'),(727,1371,'595nm_kk (A)','F','07','5','1',NULL,NULL,NULL,NULL,'0.548'),(728,1371,'595nm_kk (A)','F','08','5','1',NULL,NULL,NULL,NULL,'0.534'),(729,1371,'595nm_kk (A)','F','09','5','1',NULL,NULL,NULL,NULL,'0.536'),(730,1371,'595nm_kk (A)','F','10','5','1',NULL,NULL,NULL,NULL,'0.547'),(731,1371,'595nm_kk (A)','F','11','5','1',NULL,NULL,NULL,NULL,'-'),(732,1371,'595nm_kk (A)','F','12','5','1',NULL,NULL,NULL,NULL,'-'),(733,1371,'595nm_kk (A)','G','01','5','1',NULL,NULL,NULL,NULL,'0.651'),(734,1371,'595nm_kk (A)','G','02','5','1',NULL,NULL,NULL,NULL,'0.629'),(735,1371,'595nm_kk (A)','G','03','5','1',NULL,NULL,NULL,NULL,'0.647'),(736,1371,'595nm_kk (A)','G','04','5','1',NULL,NULL,NULL,NULL,'0.622'),(737,1371,'595nm_kk (A)','G','05','5','1',NULL,NULL,NULL,NULL,'0.610'),(738,1371,'595nm_kk (A)','G','06','5','1',NULL,NULL,NULL,NULL,'0.553'),(739,1371,'595nm_kk (A)','G','07','5','1',NULL,NULL,NULL,NULL,'0.597'),(740,1371,'595nm_kk (A)','G','08','5','1',NULL,NULL,NULL,NULL,'0.595'),(741,1371,'595nm_kk (A)','G','09','5','1',NULL,NULL,NULL,NULL,'0.586'),(742,1371,'595nm_kk (A)','G','10','5','1',NULL,NULL,NULL,NULL,'0.520'),(743,1371,'595nm_kk (A)','G','11','5','1',NULL,NULL,NULL,NULL,'-'),(744,1371,'595nm_kk (A)','G','12','5','1',NULL,NULL,NULL,NULL,'-'),(745,1371,'595nm_kk (A)','H','01','5','1',NULL,NULL,NULL,NULL,'0.654'),(746,1371,'595nm_kk (A)','H','02','5','1',NULL,NULL,NULL,NULL,'0.640'),(747,1371,'595nm_kk (A)','H','03','5','1',NULL,NULL,NULL,NULL,'0.631'),(748,1371,'595nm_kk (A)','H','04','5','1',NULL,NULL,NULL,NULL,'0.569'),(749,1371,'595nm_kk (A)','H','05','5','1',NULL,NULL,NULL,NULL,'0.622'),(750,1371,'595nm_kk (A)','H','06','5','1',NULL,NULL,NULL,NULL,'0.605'),(751,1371,'595nm_kk (A)','H','07','5','1',NULL,NULL,NULL,NULL,'0.554'),(752,1371,'595nm_kk (A)','H','08','5','1',NULL,NULL,NULL,NULL,'0.583'),(753,1371,'595nm_kk (A)','H','09','5','1',NULL,NULL,NULL,NULL,'0.635'),(754,1371,'595nm_kk (A)','H','10','5','1',NULL,NULL,NULL,NULL,'0.561'),(755,1371,'595nm_kk (A)','H','11','5','1',NULL,NULL,NULL,NULL,'-'),(756,1371,'595nm_kk (A)','H','12','5','1',NULL,NULL,NULL,NULL,'-'),(757,1371,'EGFP_sulim (Counts)','A','01','5','1',NULL,NULL,NULL,NULL,'220116'),(758,1371,'EGFP_sulim (Counts)','A','02','5','1',NULL,NULL,NULL,NULL,'210786'),(759,1371,'EGFP_sulim (Counts)','A','03','5','1',NULL,NULL,NULL,NULL,'213041'),(760,1371,'EGFP_sulim (Counts)','A','04','5','1',NULL,NULL,NULL,NULL,'195992'),(761,1371,'EGFP_sulim (Counts)','A','05','5','1',NULL,NULL,NULL,NULL,'205932'),(762,1371,'EGFP_sulim (Counts)','A','06','5','1',NULL,NULL,NULL,NULL,'203712'),(763,1371,'EGFP_sulim (Counts)','A','07','5','1',NULL,NULL,NULL,NULL,'224232'),(764,1371,'EGFP_sulim (Counts)','A','08','5','1',NULL,NULL,NULL,NULL,'310995'),(765,1371,'EGFP_sulim (Counts)','A','09','5','1',NULL,NULL,NULL,NULL,'393411'),(766,1371,'EGFP_sulim (Counts)','A','10','5','1',NULL,NULL,NULL,NULL,'565603'),(767,1371,'EGFP_sulim (Counts)','A','11','5','1',NULL,NULL,NULL,NULL,'-'),(768,1371,'EGFP_sulim (Counts)','A','12','5','1',NULL,NULL,NULL,NULL,'-'),(769,1371,'EGFP_sulim (Counts)','B','01','5','1',NULL,NULL,NULL,NULL,'265674'),(770,1371,'EGFP_sulim (Counts)','B','02','5','1',NULL,NULL,NULL,NULL,'261076'),(771,1371,'EGFP_sulim (Counts)','B','03','5','1',NULL,NULL,NULL,NULL,'207188'),(772,1371,'EGFP_sulim (Counts)','B','04','5','1',NULL,NULL,NULL,NULL,'218168'),(773,1371,'EGFP_sulim (Counts)','B','05','5','1',NULL,NULL,NULL,NULL,'208031'),(774,1371,'EGFP_sulim (Counts)','B','06','5','1',NULL,NULL,NULL,NULL,'228282'),(775,1371,'EGFP_sulim (Counts)','B','07','5','1',NULL,NULL,NULL,NULL,'244916'),(776,1371,'EGFP_sulim (Counts)','B','08','5','1',NULL,NULL,NULL,NULL,'341263'),(777,1371,'EGFP_sulim (Counts)','B','09','5','1',NULL,NULL,NULL,NULL,'420596'),(778,1371,'EGFP_sulim (Counts)','B','10','5','1',NULL,NULL,NULL,NULL,'556636'),(779,1371,'EGFP_sulim (Counts)','B','11','5','1',NULL,NULL,NULL,NULL,'-'),(780,1371,'EGFP_sulim (Counts)','B','12','5','1',NULL,NULL,NULL,NULL,'-'),(781,1371,'EGFP_sulim (Counts)','C','01','5','1',NULL,NULL,NULL,NULL,'275795'),(782,1371,'EGFP_sulim (Counts)','C','02','5','1',NULL,NULL,NULL,NULL,'231906'),(783,1371,'EGFP_sulim (Counts)','C','03','5','1',NULL,NULL,NULL,NULL,'222966'),(784,1371,'EGFP_sulim (Counts)','C','04','5','1',NULL,NULL,NULL,NULL,'220296'),(785,1371,'EGFP_sulim (Counts)','C','05','5','1',NULL,NULL,NULL,NULL,'220794'),(786,1371,'EGFP_sulim (Counts)','C','06','5','1',NULL,NULL,NULL,NULL,'245951'),(787,1371,'EGFP_sulim (Counts)','C','07','5','1',NULL,NULL,NULL,NULL,'226058'),(788,1371,'EGFP_sulim (Counts)','C','08','5','1',NULL,NULL,NULL,NULL,'330568'),(789,1371,'EGFP_sulim (Counts)','C','09','5','1',NULL,NULL,NULL,NULL,'431254'),(790,1371,'EGFP_sulim (Counts)','C','10','5','1',NULL,NULL,NULL,NULL,'717230'),(791,1371,'EGFP_sulim (Counts)','C','11','5','1',NULL,NULL,NULL,NULL,'-'),(792,1371,'EGFP_sulim (Counts)','C','12','5','1',NULL,NULL,NULL,NULL,'-'),(793,1371,'EGFP_sulim (Counts)','D','01','5','1',NULL,NULL,NULL,NULL,'237165'),(794,1371,'EGFP_sulim (Counts)','D','02','5','1',NULL,NULL,NULL,NULL,'243128'),(795,1371,'EGFP_sulim (Counts)','D','03','5','1',NULL,NULL,NULL,NULL,'217283'),(796,1371,'EGFP_sulim (Counts)','D','04','5','1',NULL,NULL,NULL,NULL,'218781'),(797,1371,'EGFP_sulim (Counts)','D','05','5','1',NULL,NULL,NULL,NULL,'224246'),(798,1371,'EGFP_sulim (Counts)','D','06','5','1',NULL,NULL,NULL,NULL,'226866'),(799,1371,'EGFP_sulim (Counts)','D','07','5','1',NULL,NULL,NULL,NULL,'263500'),(800,1371,'EGFP_sulim (Counts)','D','08','5','1',NULL,NULL,NULL,NULL,'341279'),(801,1371,'EGFP_sulim (Counts)','D','09','5','1',NULL,NULL,NULL,NULL,'439141'),(802,1371,'EGFP_sulim (Counts)','D','10','5','1',NULL,NULL,NULL,NULL,'568266'),(803,1371,'EGFP_sulim (Counts)','D','11','5','1',NULL,NULL,NULL,NULL,'-'),(804,1371,'EGFP_sulim (Counts)','D','12','5','1',NULL,NULL,NULL,NULL,'-'),(805,1371,'EGFP_sulim (Counts)','E','01','5','1',NULL,NULL,NULL,NULL,'306966'),(806,1371,'EGFP_sulim (Counts)','E','02','5','1',NULL,NULL,NULL,NULL,'281667'),(807,1371,'EGFP_sulim (Counts)','E','03','5','1',NULL,NULL,NULL,NULL,'248509'),(808,1371,'EGFP_sulim (Counts)','E','04','5','1',NULL,NULL,NULL,NULL,'251331'),(809,1371,'EGFP_sulim (Counts)','E','05','5','1',NULL,NULL,NULL,NULL,'291295'),(810,1371,'EGFP_sulim (Counts)','E','06','5','1',NULL,NULL,NULL,NULL,'278445'),(811,1371,'EGFP_sulim (Counts)','E','07','5','1',NULL,NULL,NULL,NULL,'299047'),(812,1371,'EGFP_sulim (Counts)','E','08','5','1',NULL,NULL,NULL,NULL,'386605'),(813,1371,'EGFP_sulim (Counts)','E','09','5','1',NULL,NULL,NULL,NULL,'476810'),(814,1371,'EGFP_sulim (Counts)','E','10','5','1',NULL,NULL,NULL,NULL,'602206'),(815,1371,'EGFP_sulim (Counts)','E','11','5','1',NULL,NULL,NULL,NULL,'-'),(816,1371,'EGFP_sulim (Counts)','E','12','5','1',NULL,NULL,NULL,NULL,'-'),(817,1371,'EGFP_sulim (Counts)','F','01','5','1',NULL,NULL,NULL,NULL,'299898'),(818,1371,'EGFP_sulim (Counts)','F','02','5','1',NULL,NULL,NULL,NULL,'291297'),(819,1371,'EGFP_sulim (Counts)','F','03','5','1',NULL,NULL,NULL,NULL,'292041'),(820,1371,'EGFP_sulim (Counts)','F','04','5','1',NULL,NULL,NULL,NULL,'307102'),(821,1371,'EGFP_sulim (Counts)','F','05','5','1',NULL,NULL,NULL,NULL,'232315'),(822,1371,'EGFP_sulim (Counts)','F','06','5','1',NULL,NULL,NULL,NULL,'231412'),(823,1371,'EGFP_sulim (Counts)','F','07','5','1',NULL,NULL,NULL,NULL,'245611'),(824,1371,'EGFP_sulim (Counts)','F','08','5','1',NULL,NULL,NULL,NULL,'321613'),(825,1371,'EGFP_sulim (Counts)','F','09','5','1',NULL,NULL,NULL,NULL,'396518'),(826,1371,'EGFP_sulim (Counts)','F','10','5','1',NULL,NULL,NULL,NULL,'554314'),(827,1371,'EGFP_sulim (Counts)','F','11','5','1',NULL,NULL,NULL,NULL,'-'),(828,1371,'EGFP_sulim (Counts)','F','12','5','1',NULL,NULL,NULL,NULL,'-'),(829,1371,'EGFP_sulim (Counts)','G','01','5','1',NULL,NULL,NULL,NULL,'254536'),(830,1371,'EGFP_sulim (Counts)','G','02','5','1',NULL,NULL,NULL,NULL,'283451'),(831,1371,'EGFP_sulim (Counts)','G','03','5','1',NULL,NULL,NULL,NULL,'284740'),(832,1371,'EGFP_sulim (Counts)','G','04','5','1',NULL,NULL,NULL,NULL,'277731'),(833,1371,'EGFP_sulim (Counts)','G','05','5','1',NULL,NULL,NULL,NULL,'245029'),(834,1371,'EGFP_sulim (Counts)','G','06','5','1',NULL,NULL,NULL,NULL,'230921'),(835,1371,'EGFP_sulim (Counts)','G','07','5','1',NULL,NULL,NULL,NULL,'248116'),(836,1371,'EGFP_sulim (Counts)','G','08','5','1',NULL,NULL,NULL,NULL,'395299'),(837,1371,'EGFP_sulim (Counts)','G','09','5','1',NULL,NULL,NULL,NULL,'472696'),(838,1371,'EGFP_sulim (Counts)','G','10','5','1',NULL,NULL,NULL,NULL,'496801'),(839,1371,'EGFP_sulim (Counts)','G','11','5','1',NULL,NULL,NULL,NULL,'-'),(840,1371,'EGFP_sulim (Counts)','G','12','5','1',NULL,NULL,NULL,NULL,'-'),(841,1371,'EGFP_sulim (Counts)','H','01','5','1',NULL,NULL,NULL,NULL,'283224'),(842,1371,'EGFP_sulim (Counts)','H','02','5','1',NULL,NULL,NULL,NULL,'290653'),(843,1371,'EGFP_sulim (Counts)','H','03','5','1',NULL,NULL,NULL,NULL,'281867'),(844,1371,'EGFP_sulim (Counts)','H','04','5','1',NULL,NULL,NULL,NULL,'224324'),(845,1371,'EGFP_sulim (Counts)','H','05','5','1',NULL,NULL,NULL,NULL,'256924'),(846,1371,'EGFP_sulim (Counts)','H','06','5','1',NULL,NULL,NULL,NULL,'267208'),(847,1371,'EGFP_sulim (Counts)','H','07','5','1',NULL,NULL,NULL,NULL,'211450'),(848,1371,'EGFP_sulim (Counts)','H','08','5','1',NULL,NULL,NULL,NULL,'362963'),(849,1371,'EGFP_sulim (Counts)','H','09','5','1',NULL,NULL,NULL,NULL,'576385'),(850,1371,'EGFP_sulim (Counts)','H','10','5','1',NULL,NULL,NULL,NULL,'578629'),(851,1371,'EGFP_sulim (Counts)','H','11','5','1',NULL,NULL,NULL,NULL,'-'),(852,1371,'EGFP_sulim (Counts)','H','12','5','1',NULL,NULL,NULL,NULL,'-'),(932,931,'EGFP_sulim (Counts)','A','01','26.4','1',NULL,NULL,NULL,NULL,'220116'),(933,931,'EGFP_sulim (Counts)','A','02','26.4','1',NULL,NULL,NULL,NULL,'210786'),(934,931,'EGFP_sulim (Counts)','A','03','26.4','1',NULL,NULL,NULL,NULL,'213041'),(935,931,'EGFP_sulim (Counts)','A','04','26.4','1',NULL,NULL,NULL,NULL,'195992'),(936,931,'EGFP_sulim (Counts)','A','05','26.4','1',NULL,NULL,NULL,NULL,'205932'),(937,931,'EGFP_sulim (Counts)','A','06','26.4','1',NULL,NULL,NULL,NULL,'203712'),(938,931,'EGFP_sulim (Counts)','A','07','26.4','1',NULL,NULL,NULL,NULL,'224232'),(939,931,'EGFP_sulim (Counts)','A','08','26.4','1',NULL,NULL,NULL,NULL,'310995'),(940,931,'EGFP_sulim (Counts)','A','09','26.4','1',NULL,NULL,NULL,NULL,'393411'),(941,931,'EGFP_sulim (Counts)','A','10','26.4','1',NULL,NULL,NULL,NULL,'565603'),(942,931,'EGFP_sulim (Counts)','A','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(943,931,'EGFP_sulim (Counts)','A','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(944,931,'EGFP_sulim (Counts)','B','01','26.4','1',NULL,NULL,NULL,NULL,'265674'),(945,931,'EGFP_sulim (Counts)','B','02','26.4','1',NULL,NULL,NULL,NULL,'261076'),(946,931,'EGFP_sulim (Counts)','B','03','26.4','1',NULL,NULL,NULL,NULL,'207188'),(947,931,'EGFP_sulim (Counts)','B','04','26.4','1',NULL,NULL,NULL,NULL,'218168'),(948,931,'EGFP_sulim (Counts)','B','05','26.4','1',NULL,NULL,NULL,NULL,'208031'),(949,931,'EGFP_sulim (Counts)','B','06','26.4','1',NULL,NULL,NULL,NULL,'228282'),(950,931,'EGFP_sulim (Counts)','B','07','26.4','1',NULL,NULL,NULL,NULL,'244916'),(951,931,'EGFP_sulim (Counts)','B','08','26.4','1',NULL,NULL,NULL,NULL,'341263'),(952,931,'EGFP_sulim (Counts)','B','09','26.4','1',NULL,NULL,NULL,NULL,'420596'),(953,931,'EGFP_sulim (Counts)','B','10','26.4','1',NULL,NULL,NULL,NULL,'556636'),(954,931,'EGFP_sulim (Counts)','B','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(955,931,'EGFP_sulim (Counts)','B','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(956,931,'EGFP_sulim (Counts)','C','01','26.4','1',NULL,NULL,NULL,NULL,'275795'),(957,931,'EGFP_sulim (Counts)','C','02','26.4','1',NULL,NULL,NULL,NULL,'231906'),(958,931,'EGFP_sulim (Counts)','C','03','26.4','1',NULL,NULL,NULL,NULL,'222966'),(959,931,'EGFP_sulim (Counts)','C','04','26.4','1',NULL,NULL,NULL,NULL,'220296'),(960,931,'EGFP_sulim (Counts)','C','05','26.4','1',NULL,NULL,NULL,NULL,'220794'),(961,931,'EGFP_sulim (Counts)','C','06','26.4','1',NULL,NULL,NULL,NULL,'245951'),(962,931,'EGFP_sulim (Counts)','C','07','26.4','1',NULL,NULL,NULL,NULL,'226058'),(963,931,'EGFP_sulim (Counts)','C','08','26.4','1',NULL,NULL,NULL,NULL,'330568'),(964,931,'EGFP_sulim (Counts)','C','09','26.4','1',NULL,NULL,NULL,NULL,'431254'),(965,931,'EGFP_sulim (Counts)','C','10','26.4','1',NULL,NULL,NULL,NULL,'717230'),(966,931,'EGFP_sulim (Counts)','C','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(967,931,'EGFP_sulim (Counts)','C','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(968,931,'EGFP_sulim (Counts)','D','01','26.4','1',NULL,NULL,NULL,NULL,'237165'),(969,931,'EGFP_sulim (Counts)','D','02','26.4','1',NULL,NULL,NULL,NULL,'243128'),(970,931,'EGFP_sulim (Counts)','D','03','26.4','1',NULL,NULL,NULL,NULL,'217283'),(971,931,'EGFP_sulim (Counts)','D','04','26.4','1',NULL,NULL,NULL,NULL,'218781'),(972,931,'EGFP_sulim (Counts)','D','05','26.4','1',NULL,NULL,NULL,NULL,'224246'),(973,931,'EGFP_sulim (Counts)','D','06','26.4','1',NULL,NULL,NULL,NULL,'226866'),(974,931,'EGFP_sulim (Counts)','D','07','26.4','1',NULL,NULL,NULL,NULL,'263500'),(975,931,'EGFP_sulim (Counts)','D','08','26.4','1',NULL,NULL,NULL,NULL,'341279'),(976,931,'EGFP_sulim (Counts)','D','09','26.4','1',NULL,NULL,NULL,NULL,'439141'),(977,931,'EGFP_sulim (Counts)','D','10','26.4','1',NULL,NULL,NULL,NULL,'568266'),(978,931,'EGFP_sulim (Counts)','D','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(979,931,'EGFP_sulim (Counts)','D','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(980,931,'EGFP_sulim (Counts)','E','01','26.4','1',NULL,NULL,NULL,NULL,'306966'),(981,931,'EGFP_sulim (Counts)','E','02','26.4','1',NULL,NULL,NULL,NULL,'281667'),(982,931,'EGFP_sulim (Counts)','E','03','26.4','1',NULL,NULL,NULL,NULL,'248509'),(983,931,'EGFP_sulim (Counts)','E','04','26.4','1',NULL,NULL,NULL,NULL,'251331'),(984,931,'EGFP_sulim (Counts)','E','05','26.4','1',NULL,NULL,NULL,NULL,'291295'),(985,931,'EGFP_sulim (Counts)','E','06','26.4','1',NULL,NULL,NULL,NULL,'278445'),(986,931,'EGFP_sulim (Counts)','E','07','26.4','1',NULL,NULL,NULL,NULL,'299047'),(987,931,'EGFP_sulim (Counts)','E','08','26.4','1',NULL,NULL,NULL,NULL,'386605'),(988,931,'EGFP_sulim (Counts)','E','09','26.4','1',NULL,NULL,NULL,NULL,'476810'),(989,931,'EGFP_sulim (Counts)','E','10','26.4','1',NULL,NULL,NULL,NULL,'602206'),(990,931,'EGFP_sulim (Counts)','E','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(991,931,'EGFP_sulim (Counts)','E','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(992,931,'EGFP_sulim (Counts)','F','01','26.4','1',NULL,NULL,NULL,NULL,'299898'),(993,931,'EGFP_sulim (Counts)','F','02','26.4','1',NULL,NULL,NULL,NULL,'291297'),(994,931,'EGFP_sulim (Counts)','F','03','26.4','1',NULL,NULL,NULL,NULL,'292041'),(995,931,'EGFP_sulim (Counts)','F','04','26.4','1',NULL,NULL,NULL,NULL,'307102'),(996,931,'EGFP_sulim (Counts)','F','05','26.4','1',NULL,NULL,NULL,NULL,'232315'),(997,931,'EGFP_sulim (Counts)','F','06','26.4','1',NULL,NULL,NULL,NULL,'231412'),(998,931,'EGFP_sulim (Counts)','F','07','26.4','1',NULL,NULL,NULL,NULL,'245611'),(999,931,'EGFP_sulim (Counts)','F','08','26.4','1',NULL,NULL,NULL,NULL,'321613'),(1000,931,'EGFP_sulim (Counts)','F','09','26.4','1',NULL,NULL,NULL,NULL,'396518'),(1001,931,'EGFP_sulim (Counts)','F','10','26.4','1',NULL,NULL,NULL,NULL,'554314'),(1002,931,'EGFP_sulim (Counts)','F','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1003,931,'EGFP_sulim (Counts)','F','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1004,931,'EGFP_sulim (Counts)','G','01','26.4','1',NULL,NULL,NULL,NULL,'254536'),(1005,931,'EGFP_sulim (Counts)','G','02','26.4','1',NULL,NULL,NULL,NULL,'283451'),(1006,931,'EGFP_sulim (Counts)','G','03','26.4','1',NULL,NULL,NULL,NULL,'284740'),(1007,931,'EGFP_sulim (Counts)','G','04','26.4','1',NULL,NULL,NULL,NULL,'277731'),(1008,931,'EGFP_sulim (Counts)','G','05','26.4','1',NULL,NULL,NULL,NULL,'245029'),(1009,931,'EGFP_sulim (Counts)','G','06','26.4','1',NULL,NULL,NULL,NULL,'230921'),(1010,931,'EGFP_sulim (Counts)','G','07','26.4','1',NULL,NULL,NULL,NULL,'248116'),(1011,931,'EGFP_sulim (Counts)','G','08','26.4','1',NULL,NULL,NULL,NULL,'395299'),(1012,931,'EGFP_sulim (Counts)','G','09','26.4','1',NULL,NULL,NULL,NULL,'472696'),(1013,931,'EGFP_sulim (Counts)','G','10','26.4','1',NULL,NULL,NULL,NULL,'496801'),(1014,931,'EGFP_sulim (Counts)','G','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1015,931,'EGFP_sulim (Counts)','G','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1016,931,'EGFP_sulim (Counts)','H','01','26.4','1',NULL,NULL,NULL,NULL,'283224'),(1017,931,'EGFP_sulim (Counts)','H','02','26.4','1',NULL,NULL,NULL,NULL,'290653'),(1018,931,'EGFP_sulim (Counts)','H','03','26.4','1',NULL,NULL,NULL,NULL,'281867'),(1019,931,'EGFP_sulim (Counts)','H','04','26.4','1',NULL,NULL,NULL,NULL,'224324'),(1020,931,'EGFP_sulim (Counts)','H','05','26.4','1',NULL,NULL,NULL,NULL,'256924'),(1021,931,'EGFP_sulim (Counts)','H','06','26.4','1',NULL,NULL,NULL,NULL,'267208'),(1022,931,'EGFP_sulim (Counts)','H','07','26.4','1',NULL,NULL,NULL,NULL,'211450'),(1023,931,'EGFP_sulim (Counts)','H','08','26.4','1',NULL,NULL,NULL,NULL,'362963'),(1024,931,'EGFP_sulim (Counts)','H','09','26.4','1',NULL,NULL,NULL,NULL,'576385'),(1025,931,'EGFP_sulim (Counts)','H','10','26.4','1',NULL,NULL,NULL,NULL,'578629'),(1026,931,'EGFP_sulim (Counts)','H','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1027,931,'EGFP_sulim (Counts)','H','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1032,1031,'595nm_kk (A)','A','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.604'),(1033,1031,'595nm_kk (A)','A','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.611'),(1034,1031,'595nm_kk (A)','A','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.606'),(1035,1031,'595nm_kk (A)','A','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.592'),(1036,1031,'595nm_kk (A)','A','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.595'),(1037,1031,'595nm_kk (A)','A','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.567'),(1038,1031,'595nm_kk (A)','A','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.586'),(1039,1031,'595nm_kk (A)','A','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.584'),(1040,1031,'595nm_kk (A)','A','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.587'),(1041,1031,'595nm_kk (A)','A','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.591'),(1042,1031,'595nm_kk (A)','A','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1043,1031,'595nm_kk (A)','A','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1044,1031,'595nm_kk (A)','B','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.617'),(1045,1031,'595nm_kk (A)','B','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.605'),(1046,1031,'595nm_kk (A)','B','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.600'),(1047,1031,'595nm_kk (A)','B','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.621'),(1048,1031,'595nm_kk (A)','B','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.604'),(1049,1031,'595nm_kk (A)','B','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.616'),(1050,1031,'595nm_kk (A)','B','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.579'),(1051,1031,'595nm_kk (A)','B','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.655'),(1052,1031,'595nm_kk (A)','B','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.612'),(1053,1031,'595nm_kk (A)','B','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.589'),(1054,1031,'595nm_kk (A)','B','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1055,1031,'595nm_kk (A)','B','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1056,1031,'595nm_kk (A)','C','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.632'),(1057,1031,'595nm_kk (A)','C','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.579'),(1058,1031,'595nm_kk (A)','C','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.585'),(1059,1031,'595nm_kk (A)','C','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.603'),(1060,1031,'595nm_kk (A)','C','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.625'),(1061,1031,'595nm_kk (A)','C','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.603'),(1062,1031,'595nm_kk (A)','C','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.614'),(1063,1031,'595nm_kk (A)','C','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.619'),(1064,1031,'595nm_kk (A)','C','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.617'),(1065,1031,'595nm_kk (A)','C','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.644'),(1066,1031,'595nm_kk (A)','C','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1067,1031,'595nm_kk (A)','C','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1068,1031,'595nm_kk (A)','D','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.631'),(1069,1031,'595nm_kk (A)','D','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.591'),(1070,1031,'595nm_kk (A)','D','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.624'),(1071,1031,'595nm_kk (A)','D','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.626'),(1072,1031,'595nm_kk (A)','D','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.616'),(1073,1031,'595nm_kk (A)','D','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.648'),(1074,1031,'595nm_kk (A)','D','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.597'),(1075,1031,'595nm_kk (A)','D','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.602'),(1076,1031,'595nm_kk (A)','D','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.635'),(1077,1031,'595nm_kk (A)','D','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.595'),(1078,1031,'595nm_kk (A)','D','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1079,1031,'595nm_kk (A)','D','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1080,1031,'595nm_kk (A)','E','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.641'),(1081,1031,'595nm_kk (A)','E','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.615'),(1082,1031,'595nm_kk (A)','E','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.582'),(1083,1031,'595nm_kk (A)','E','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.594'),(1084,1031,'595nm_kk (A)','E','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.612'),(1085,1031,'595nm_kk (A)','E','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.600'),(1086,1031,'595nm_kk (A)','E','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.604'),(1087,1031,'595nm_kk (A)','E','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.586'),(1088,1031,'595nm_kk (A)','E','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.582'),(1089,1031,'595nm_kk (A)','E','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.577'),(1090,1031,'595nm_kk (A)','E','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1091,1031,'595nm_kk (A)','E','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1092,1031,'595nm_kk (A)','F','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.636'),(1093,1031,'595nm_kk (A)','F','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.623'),(1094,1031,'595nm_kk (A)','F','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.637'),(1095,1031,'595nm_kk (A)','F','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.642'),(1096,1031,'595nm_kk (A)','F','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.542'),(1097,1031,'595nm_kk (A)','F','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.573'),(1098,1031,'595nm_kk (A)','F','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.548'),(1099,1031,'595nm_kk (A)','F','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.534'),(1100,1031,'595nm_kk (A)','F','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.536'),(1101,1031,'595nm_kk (A)','F','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.547'),(1102,1031,'595nm_kk (A)','F','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1103,1031,'595nm_kk (A)','F','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1104,1031,'595nm_kk (A)','G','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.651'),(1105,1031,'595nm_kk (A)','G','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.629'),(1106,1031,'595nm_kk (A)','G','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.647'),(1107,1031,'595nm_kk (A)','G','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.622'),(1108,1031,'595nm_kk (A)','G','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.610'),(1109,1031,'595nm_kk (A)','G','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.553'),(1110,1031,'595nm_kk (A)','G','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.597'),(1111,1031,'595nm_kk (A)','G','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.595'),(1112,1031,'595nm_kk (A)','G','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.586'),(1113,1031,'595nm_kk (A)','G','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.520'),(1114,1031,'595nm_kk (A)','G','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1115,1031,'595nm_kk (A)','G','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1116,1031,'595nm_kk (A)','H','01',NULL,NULL,NULL,NULL,NULL,NULL,'0.654'),(1117,1031,'595nm_kk (A)','H','02',NULL,NULL,NULL,NULL,NULL,NULL,'0.640'),(1118,1031,'595nm_kk (A)','H','03',NULL,NULL,NULL,NULL,NULL,NULL,'0.631'),(1119,1031,'595nm_kk (A)','H','04',NULL,NULL,NULL,NULL,NULL,NULL,'0.569'),(1120,1031,'595nm_kk (A)','H','05',NULL,NULL,NULL,NULL,NULL,NULL,'0.622'),(1121,1031,'595nm_kk (A)','H','06',NULL,NULL,NULL,NULL,NULL,NULL,'0.605'),(1122,1031,'595nm_kk (A)','H','07',NULL,NULL,NULL,NULL,NULL,NULL,'0.554'),(1123,1031,'595nm_kk (A)','H','08',NULL,NULL,NULL,NULL,NULL,NULL,'0.583'),(1124,1031,'595nm_kk (A)','H','09',NULL,NULL,NULL,NULL,NULL,NULL,'0.635'),(1125,1031,'595nm_kk (A)','H','10',NULL,NULL,NULL,NULL,NULL,NULL,'0.561'),(1126,1031,'595nm_kk (A)','H','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1127,1031,'595nm_kk (A)','H','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1128,1031,'EGFP_sulim (Counts)','A','01',NULL,NULL,NULL,NULL,NULL,NULL,'220116'),(1129,1031,'EGFP_sulim (Counts)','A','02',NULL,NULL,NULL,NULL,NULL,NULL,'210786'),(1130,1031,'EGFP_sulim (Counts)','A','03',NULL,NULL,NULL,NULL,NULL,NULL,'213041'),(1131,1031,'EGFP_sulim (Counts)','A','04',NULL,NULL,NULL,NULL,NULL,NULL,'195992'),(1132,1031,'EGFP_sulim (Counts)','A','05',NULL,NULL,NULL,NULL,NULL,NULL,'205932'),(1133,1031,'EGFP_sulim (Counts)','A','06',NULL,NULL,NULL,NULL,NULL,NULL,'203712'),(1134,1031,'EGFP_sulim (Counts)','A','07',NULL,NULL,NULL,NULL,NULL,NULL,'224232'),(1135,1031,'EGFP_sulim (Counts)','A','08',NULL,NULL,NULL,NULL,NULL,NULL,'310995'),(1136,1031,'EGFP_sulim (Counts)','A','09',NULL,NULL,NULL,NULL,NULL,NULL,'393411'),(1137,1031,'EGFP_sulim (Counts)','A','10',NULL,NULL,NULL,NULL,NULL,NULL,'565603'),(1138,1031,'EGFP_sulim (Counts)','A','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1139,1031,'EGFP_sulim (Counts)','A','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1140,1031,'EGFP_sulim (Counts)','B','01',NULL,NULL,NULL,NULL,NULL,NULL,'265674'),(1141,1031,'EGFP_sulim (Counts)','B','02',NULL,NULL,NULL,NULL,NULL,NULL,'261076'),(1142,1031,'EGFP_sulim (Counts)','B','03',NULL,NULL,NULL,NULL,NULL,NULL,'207188'),(1143,1031,'EGFP_sulim (Counts)','B','04',NULL,NULL,NULL,NULL,NULL,NULL,'218168'),(1144,1031,'EGFP_sulim (Counts)','B','05',NULL,NULL,NULL,NULL,NULL,NULL,'208031'),(1145,1031,'EGFP_sulim (Counts)','B','06',NULL,NULL,NULL,NULL,NULL,NULL,'228282'),(1146,1031,'EGFP_sulim (Counts)','B','07',NULL,NULL,NULL,NULL,NULL,NULL,'244916'),(1147,1031,'EGFP_sulim (Counts)','B','08',NULL,NULL,NULL,NULL,NULL,NULL,'341263'),(1148,1031,'EGFP_sulim (Counts)','B','09',NULL,NULL,NULL,NULL,NULL,NULL,'420596'),(1149,1031,'EGFP_sulim (Counts)','B','10',NULL,NULL,NULL,NULL,NULL,NULL,'556636'),(1150,1031,'EGFP_sulim (Counts)','B','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1151,1031,'EGFP_sulim (Counts)','B','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1152,1031,'EGFP_sulim (Counts)','C','01',NULL,NULL,NULL,NULL,NULL,NULL,'275795'),(1153,1031,'EGFP_sulim (Counts)','C','02',NULL,NULL,NULL,NULL,NULL,NULL,'231906'),(1154,1031,'EGFP_sulim (Counts)','C','03',NULL,NULL,NULL,NULL,NULL,NULL,'222966'),(1155,1031,'EGFP_sulim (Counts)','C','04',NULL,NULL,NULL,NULL,NULL,NULL,'220296'),(1156,1031,'EGFP_sulim (Counts)','C','05',NULL,NULL,NULL,NULL,NULL,NULL,'220794'),(1157,1031,'EGFP_sulim (Counts)','C','06',NULL,NULL,NULL,NULL,NULL,NULL,'245951'),(1158,1031,'EGFP_sulim (Counts)','C','07',NULL,NULL,NULL,NULL,NULL,NULL,'226058'),(1159,1031,'EGFP_sulim (Counts)','C','08',NULL,NULL,NULL,NULL,NULL,NULL,'330568'),(1160,1031,'EGFP_sulim (Counts)','C','09',NULL,NULL,NULL,NULL,NULL,NULL,'431254'),(1161,1031,'EGFP_sulim (Counts)','C','10',NULL,NULL,NULL,NULL,NULL,NULL,'717230'),(1162,1031,'EGFP_sulim (Counts)','C','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1163,1031,'EGFP_sulim (Counts)','C','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1164,1031,'EGFP_sulim (Counts)','D','01',NULL,NULL,NULL,NULL,NULL,NULL,'237165'),(1165,1031,'EGFP_sulim (Counts)','D','02',NULL,NULL,NULL,NULL,NULL,NULL,'243128'),(1166,1031,'EGFP_sulim (Counts)','D','03',NULL,NULL,NULL,NULL,NULL,NULL,'217283'),(1167,1031,'EGFP_sulim (Counts)','D','04',NULL,NULL,NULL,NULL,NULL,NULL,'218781'),(1168,1031,'EGFP_sulim (Counts)','D','05',NULL,NULL,NULL,NULL,NULL,NULL,'224246'),(1169,1031,'EGFP_sulim (Counts)','D','06',NULL,NULL,NULL,NULL,NULL,NULL,'226866'),(1170,1031,'EGFP_sulim (Counts)','D','07',NULL,NULL,NULL,NULL,NULL,NULL,'263500'),(1171,1031,'EGFP_sulim (Counts)','D','08',NULL,NULL,NULL,NULL,NULL,NULL,'341279'),(1172,1031,'EGFP_sulim (Counts)','D','09',NULL,NULL,NULL,NULL,NULL,NULL,'439141'),(1173,1031,'EGFP_sulim (Counts)','D','10',NULL,NULL,NULL,NULL,NULL,NULL,'568266'),(1174,1031,'EGFP_sulim (Counts)','D','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1175,1031,'EGFP_sulim (Counts)','D','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1176,1031,'EGFP_sulim (Counts)','E','01',NULL,NULL,NULL,NULL,NULL,NULL,'306966'),(1177,1031,'EGFP_sulim (Counts)','E','02',NULL,NULL,NULL,NULL,NULL,NULL,'281667'),(1178,1031,'EGFP_sulim (Counts)','E','03',NULL,NULL,NULL,NULL,NULL,NULL,'248509'),(1179,1031,'EGFP_sulim (Counts)','E','04',NULL,NULL,NULL,NULL,NULL,NULL,'251331'),(1180,1031,'EGFP_sulim (Counts)','E','05',NULL,NULL,NULL,NULL,NULL,NULL,'291295'),(1181,1031,'EGFP_sulim (Counts)','E','06',NULL,NULL,NULL,NULL,NULL,NULL,'278445'),(1182,1031,'EGFP_sulim (Counts)','E','07',NULL,NULL,NULL,NULL,NULL,NULL,'299047'),(1183,1031,'EGFP_sulim (Counts)','E','08',NULL,NULL,NULL,NULL,NULL,NULL,'386605'),(1184,1031,'EGFP_sulim (Counts)','E','09',NULL,NULL,NULL,NULL,NULL,NULL,'476810'),(1185,1031,'EGFP_sulim (Counts)','E','10',NULL,NULL,NULL,NULL,NULL,NULL,'602206'),(1186,1031,'EGFP_sulim (Counts)','E','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1187,1031,'EGFP_sulim (Counts)','E','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1188,1031,'EGFP_sulim (Counts)','F','01',NULL,NULL,NULL,NULL,NULL,NULL,'299898'),(1189,1031,'EGFP_sulim (Counts)','F','02',NULL,NULL,NULL,NULL,NULL,NULL,'291297'),(1190,1031,'EGFP_sulim (Counts)','F','03',NULL,NULL,NULL,NULL,NULL,NULL,'292041'),(1191,1031,'EGFP_sulim (Counts)','F','04',NULL,NULL,NULL,NULL,NULL,NULL,'307102'),(1192,1031,'EGFP_sulim (Counts)','F','05',NULL,NULL,NULL,NULL,NULL,NULL,'232315'),(1193,1031,'EGFP_sulim (Counts)','F','06',NULL,NULL,NULL,NULL,NULL,NULL,'231412'),(1194,1031,'EGFP_sulim (Counts)','F','07',NULL,NULL,NULL,NULL,NULL,NULL,'245611'),(1195,1031,'EGFP_sulim (Counts)','F','08',NULL,NULL,NULL,NULL,NULL,NULL,'321613'),(1196,1031,'EGFP_sulim (Counts)','F','09',NULL,NULL,NULL,NULL,NULL,NULL,'396518'),(1197,1031,'EGFP_sulim (Counts)','F','10',NULL,NULL,NULL,NULL,NULL,NULL,'554314'),(1198,1031,'EGFP_sulim (Counts)','F','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1199,1031,'EGFP_sulim (Counts)','F','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1200,1031,'EGFP_sulim (Counts)','G','01',NULL,NULL,NULL,NULL,NULL,NULL,'254536'),(1201,1031,'EGFP_sulim (Counts)','G','02',NULL,NULL,NULL,NULL,NULL,NULL,'283451'),(1202,1031,'EGFP_sulim (Counts)','G','03',NULL,NULL,NULL,NULL,NULL,NULL,'284740'),(1203,1031,'EGFP_sulim (Counts)','G','04',NULL,NULL,NULL,NULL,NULL,NULL,'277731'),(1204,1031,'EGFP_sulim (Counts)','G','05',NULL,NULL,NULL,NULL,NULL,NULL,'245029'),(1205,1031,'EGFP_sulim (Counts)','G','06',NULL,NULL,NULL,NULL,NULL,NULL,'230921'),(1206,1031,'EGFP_sulim (Counts)','G','07',NULL,NULL,NULL,NULL,NULL,NULL,'248116'),(1207,1031,'EGFP_sulim (Counts)','G','08',NULL,NULL,NULL,NULL,NULL,NULL,'395299'),(1208,1031,'EGFP_sulim (Counts)','G','09',NULL,NULL,NULL,NULL,NULL,NULL,'472696'),(1209,1031,'EGFP_sulim (Counts)','G','10',NULL,NULL,NULL,NULL,NULL,NULL,'496801'),(1210,1031,'EGFP_sulim (Counts)','G','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1211,1031,'EGFP_sulim (Counts)','G','12',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1212,1031,'EGFP_sulim (Counts)','H','01',NULL,NULL,NULL,NULL,NULL,NULL,'283224'),(1213,1031,'EGFP_sulim (Counts)','H','02',NULL,NULL,NULL,NULL,NULL,NULL,'290653'),(1214,1031,'EGFP_sulim (Counts)','H','03',NULL,NULL,NULL,NULL,NULL,NULL,'281867'),(1215,1031,'EGFP_sulim (Counts)','H','04',NULL,NULL,NULL,NULL,NULL,NULL,'224324'),(1216,1031,'EGFP_sulim (Counts)','H','05',NULL,NULL,NULL,NULL,NULL,NULL,'256924'),(1217,1031,'EGFP_sulim (Counts)','H','06',NULL,NULL,NULL,NULL,NULL,NULL,'267208'),(1218,1031,'EGFP_sulim (Counts)','H','07',NULL,NULL,NULL,NULL,NULL,NULL,'211450'),(1219,1031,'EGFP_sulim (Counts)','H','08',NULL,NULL,NULL,NULL,NULL,NULL,'362963'),(1220,1031,'EGFP_sulim (Counts)','H','09',NULL,NULL,NULL,NULL,NULL,NULL,'576385'),(1221,1031,'EGFP_sulim (Counts)','H','10',NULL,NULL,NULL,NULL,NULL,NULL,'578629'),(1222,1031,'EGFP_sulim (Counts)','H','11',NULL,NULL,NULL,NULL,NULL,NULL,'-'),(1223,1031,'EGFP_sulim (Counts)','H','12',NULL,NULL,NULL,NULL,NULL,NULL,'-');
/*!40000 ALTER TABLE `Victor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-04 12:57:55
