from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import url

from . import views

app_name = 'faq'
urlpatterns = [
	url(r'^', views.index, name='index'),
] 

###+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
