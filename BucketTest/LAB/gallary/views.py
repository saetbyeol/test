# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import Gallary
# Create your views here.
def index(request):
	print 'gallary/index'
	gallaryList = Gallary.objects.all()
        context = {'gallaryList':gallaryList}
        return render(request,'gallary/index.html',context)
