-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: PartBank
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES euckr */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Attachment`
--

DROP TABLE IF EXISTS `Attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Attachment` (
  `attachmentID` bigint(20) NOT NULL COMMENT '첨부 파일 ID',
  `name` varchar(100) NOT NULL COMMENT '첨부파일 이름',
  `extension` varchar(16) DEFAULT NULL COMMENT '첨부파일 확장자 이름',
  `contentType` varchar(20) DEFAULT NULL COMMENT '첨부파일 유형 (image, file, text, xml...)',
  `fileSize` bigint(100) NOT NULL COMMENT '첨부파일 크기',
  `path` varchar(100) DEFAULT NULL COMMENT '파일경로',
  `userID` bigint(100) NOT NULL COMMENT '첨부파일 등록 사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '생성일자',
  PRIMARY KEY (`attachmentID`),
  KEY `ERA_REFS_USERID` (`userID`),
  CONSTRAINT `ERA_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 결과 이미지 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CitaterArticle`
--

DROP TABLE IF EXISTS `CitaterArticle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CitaterArticle` (
  `articleID` bigint(20) NOT NULL COMMENT '참고논문 ID',
  `alias` varchar(128) NOT NULL COMMENT '참고논문 약어이름',
  `title` varchar(128) NOT NULL COMMENT '참고논문 제목',
  `authors` varchar(1000) DEFAULT NULL COMMENT '저자명',
  `year` int(11) DEFAULT NULL COMMENT '저서 등록 년도',
  `comment` text COMMENT '저서에 대한 논평',
  `journal` varchar(128) DEFAULT NULL COMMENT '저널 이름',
  `publishDate` varchar(50) DEFAULT NULL COMMENT '출판일자',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  PRIMARY KEY (`articleID`),
  KEY `Article_Alias_IDX` (`alias`),
  KEY `Article_Title_IDX` (`title`),
  KEY `Article_Journal_IDX` (`journal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Device`
--

DROP TABLE IF EXISTS `Device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Device` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `serial` varchar(16) NOT NULL COMMENT '고유번호',
  `name` varchar(64) NOT NULL COMMENT '이름이 없는 경우 고유번호 입력',
  `deviceType` bigint(20) NOT NULL COMMENT '장비 유형 ID',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`deviceID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `DEVICE_REFS_TYPE` (`deviceType`),
  KEY `DEVICE_REFS_USERID` (`userID`),
  KEY `Device_Serial_IDX` (`serial`),
  KEY `Device_Name_IDX` (`name`),
  CONSTRAINT `DEVICE_REFS_TYPE` FOREIGN KEY (`deviceType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `DEVICE_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비 관리테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DeviceComponent`
--

DROP TABLE IF EXISTS `DeviceComponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceComponent` (
  `componentID` bigint(20) NOT NULL COMMENT '장비 내 부품 ID',
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `sourceID` bigint(20) NOT NULL COMMENT '부품의 원본 객체 ID (Part, IOUnit, Module, ModulePart, ModuleIOUnit, Logic)',
  `sourceType` varchar(20) NOT NULL DEFAULT 'DeviceModule' COMMENT '부품의 원본 객체의 종류',
  `positionX` double NOT NULL DEFAULT '0' COMMENT '부품의 x 좌표',
  `positionY` double NOT NULL DEFAULT '0' COMMENT '부품의 y 좌표',
  `width` double NOT NULL DEFAULT '30' COMMENT '부품의 넓이',
  `height` double NOT NULL DEFAULT '30' COMMENT '부품의 높이',
  `inputDirections` varchar(45) DEFAULT NULL COMMENT '입력 Anchor 방향, comma 로 구분(UP,DOWN,LEFT,RIGHT)',
  `outputDirections` varchar(45) DEFAULT NULL COMMENT '출력 Anchor 방향, comma 로 구분(UP,DOWN,LEFT,RIGHT)',
  `isInputOverlap` tinyint(1) DEFAULT '0' COMMENT '입력되는 연결선의 중복여부 (기본 중복 불허)',
  `isOutputOverlap` tinyint(1) DEFAULT '1' COMMENT '입력되는 연결선의 중복여부 (기본 중복 불허)',
  `rotationDegrees` double DEFAULT '0' COMMENT '회전 각도 (0 ~ 360)',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '부품의 등록 순서',
  PRIMARY KEY (`componentID`),
  KEY `DCOMP_REFS_DEVICEID` (`deviceID`),
  CONSTRAINT `DCOMP_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비의 부품관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DeviceDesigner`
--

DROP TABLE IF EXISTS `DeviceDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceDesigner` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `userID` bigint(20) NOT NULL COMMENT '장비개발자 ID',
  UNIQUE KEY `deviceID` (`deviceID`,`userID`),
  KEY `DEVICEDESIGNER_REFS_USERID` (`userID`),
  CONSTRAINT `DEVICEDESIGNER_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`),
  CONSTRAINT `DEVICEDESIGNER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DeviceLink`
--

DROP TABLE IF EXISTS `DeviceLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceLink` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `sourceID` bigint(20) NOT NULL COMMENT '시작하는 장비의 부품 ID',
  `targetID` bigint(20) NOT NULL COMMENT '종료하는 장비의 부품 ID',
  `borderWidth` int(11) NOT NULL DEFAULT '1' COMMENT '연결선의 두께',
  `borderColor` varchar(15) NOT NULL DEFAULT '000000' COMMENT '연결선의 색상',
  `isStartArrow` tinyint(1) DEFAULT '0' COMMENT '시작부분 화살표 유무',
  `isEndArrow` tinyint(1) DEFAULT '0' COMMENT '끝부분 화살표 유무',
  `inputIndex` int(11) NOT NULL DEFAULT '0' COMMENT 'Component의 출력에 연결된 Link의 순서, ex) IOUnit에서 2개의 선이 나와 두 개의 프로모터로 각각 연결되면 각 연결선의 outputIndex 는 0과 1이 됨',
  `outputIndex` int(11) NOT NULL DEFAULT '0' COMMENT 'Component의 입력에 연결된 Link의 순서, ex) 두 개의 I/O unit에서 선이 하나씩 나와 하나의 로직으로 연결되는 경우 각 연결선의 inputIndex는 0과 1이 됨',
  UNIQUE KEY `deviceID` (`deviceID`,`sourceID`,`targetID`),
  KEY `DLINK_REF_SOURCEID` (`sourceID`),
  KEY `DLINK_REF_TARGETID` (`targetID`),
  CONSTRAINT `DLINK_REF_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`),
  CONSTRAINT `DLINK_REF_SOURCEID` FOREIGN KEY (`sourceID`) REFERENCES `DeviceComponent` (`componentID`),
  CONSTRAINT `DLINK_REF_TARGETID` FOREIGN KEY (`targetID`) REFERENCES `DeviceComponent` (`componentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비의 부품 연결선 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DeviceReferences`
--

DROP TABLE IF EXISTS `DeviceReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeviceReferences` (
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `deviceID` (`deviceID`,`articleID`),
  KEY `DEVICEREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `DEVICEREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `DEVICEREFERENCES_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Dictionary`
--

DROP TABLE IF EXISTS `Dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dictionary` (
  `dictionaryID` bigint(20) NOT NULL COMMENT '항목 ID',
  `name` varchar(128) NOT NULL COMMENT '항목이름',
  `parentID` bigint(20) DEFAULT NULL COMMENT '상위 항목 ID',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `description` text COMMENT '항목 설명',
  `nodeType` enum('Folder','Text') NOT NULL DEFAULT 'Folder' COMMENT '폴더인지 Leaf 노드인지 설정',
  `sourceType` varchar(16) NOT NULL DEFAULT 'Unknown' COMMENT 'Dictionary 종류',
  `removable` char(1) DEFAULT '1' COMMENT '삭제 가능성(0: 삭제 불가능, 1:삭제 가능)',
  `hasIcon` char(1) DEFAULT '0' COMMENT '아이콘 이미지를 갖고 있는지 여부(0: 이미지 존재 안 함, 1: 이미지 존재)',
  `creationDate` varchar(15) NOT NULL COMMENT '생성일자',
  PRIMARY KEY (`dictionaryID`),
  KEY `DICT_REFS_PARENTID` (`parentID`),
  KEY `DICT_REFS_USERID` (`userID`),
  KEY `Dictionary_Name_IDX` (`name`),
  CONSTRAINT `DICT_REFS_PARENTID` FOREIGN KEY (`parentID`) REFERENCES `Dictionary` (`dictionaryID`) ON DELETE CASCADE,
  CONSTRAINT `DICT_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시스템 사용항목 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DictionaryProp`
--

DROP TABLE IF EXISTS `DictionaryProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DictionaryProp` (
  `dictionaryID` bigint(20) NOT NULL COMMENT '사용 항목 ID',
  `propName` varchar(128) NOT NULL COMMENT '사용항목 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '사용항목 속성 값',
  UNIQUE KEY `dictionaryID` (`dictionaryID`,`propName`),
  KEY `DictionaryProp_Name_IDX` (`propName`),
  KEY `DictionaryProp_Value_IDX` (`propValue`),
  CONSTRAINT `DICTPROP_REFS_DICTID` FOREIGN KEY (`dictionaryID`) REFERENCES `Dictionary` (`dictionaryID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시스템 사용항목 기타속성 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventHistory`
--

DROP TABLE IF EXISTS `EventHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventHistory` (
  `eventID` bigint(20) NOT NULL COMMENT '이벤트 ID',
  `eventType` varchar(16) NOT NULL COMMENT '이벤트 유형(Create, Change, Delete)',
  `message` varchar(256) NOT NULL COMMENT '메시지',
  `sourceID` bigint(20) NOT NULL COMMENT '이벤트 대상 ID',
  `sourceName` varchar(256) NOT NULL COMMENT '이벤트 대상 이름',
  `sourceType` varchar(32) NOT NULL COMMENT 'User, Dictionary, IOUnit, Part, Module, Device, Strain, Protocol, Experiment, Simulation',
  `userID` bigint(20) NOT NULL COMMENT '이벤트 실행 사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '이벤트 발생 시간',
  PRIMARY KEY (`eventID`),
  KEY `EVTHISTORY_REFS_USERID` (`userID`),
  KEY `Evtistory_SourceType_IDX` (`sourceType`),
  KEY `EvtHistory_CDate_IDX` (`creationDate`),
  CONSTRAINT `EVTHISTORY_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='이벤트 생성, 수정, 삭제 기록 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Experiment`
--

DROP TABLE IF EXISTS `Experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Experiment` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험 결과 ID',
  `protocolID` bigint(20) NOT NULL COMMENT '실험 프로토콜 ID',
  `name` varchar(128) DEFAULT NULL COMMENT '실험결과 이름',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '실험 생성자 ID',
  `experimentDate` varchar(15) DEFAULT NULL COMMENT '실험날짜',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  `p_description` text,
  `outputList` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`experimentID`),
  KEY `ER_REFS_PROTOCOLID` (`protocolID`),
  KEY `ER_REFS_USERID` (`userID`),
  KEY `Exper_Name_IDX` (`name`),
  CONSTRAINT `ER_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`),
  CONSTRAINT `ER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExperimentAttachment`
--

DROP TABLE IF EXISTS `ExperimentAttachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentAttachment` (
  `experimentID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `attachmentID` bigint(20) NOT NULL COMMENT '첨부파일 ID',
  `type` varchar(15) NOT NULL DEFAULT 'output' COMMENT '첨부파일 종류 (결과파일, 기타...)',
  UNIQUE KEY `experimentID` (`experimentID`,`attachmentID`),
  KEY `ERA_REFS_ATTACHID` (`attachmentID`),
  CONSTRAINT `ERA_REFS_ATTACHID` FOREIGN KEY (`attachmentID`) REFERENCES `Attachment` (`attachmentID`),
  CONSTRAINT `ERA_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 결과 이미지 첨부파일 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExperimentDesigner`
--

DROP TABLE IF EXISTS `ExperimentDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentDesigner` (
  `experimentID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `userID` bigint(20) NOT NULL COMMENT '프로토콜 개발자 ID',
  UNIQUE KEY `experimentID` (`experimentID`,`userID`),
  KEY `ERD_REFS_USERID` (`userID`),
  CONSTRAINT `ERD_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`),
  CONSTRAINT `ERD_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExperimentInput`
--

DROP TABLE IF EXISTS `ExperimentInput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentInput` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험 결과 ID',
  `componentID` bigint(20) NOT NULL COMMENT 'DeviceComponent 테이블의 입력에 해당하는 부품 ID',
  `quantity` double DEFAULT '0' COMMENT '입력 수치',
  `unit` varchar(10) DEFAULT NULL COMMENT '수치 단위',
  UNIQUE KEY `experimentID` (`experimentID`,`componentID`),
  KEY `EXPRIN_REFS_COMPID` (`componentID`),
  CONSTRAINT `EXPRIN_REFS_COMPID` FOREIGN KEY (`componentID`) REFERENCES `DeviceComponent` (`componentID`),
  CONSTRAINT `EXPRIN_REFS_SIMULID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 입력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExperimentOutput`
--

DROP TABLE IF EXISTS `ExperimentOutput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentOutput` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험 결과 ID',
  `name` varchar(128) NOT NULL COMMENT '출력정보 이름',
  `mean` double DEFAULT '0' COMMENT '평균 출력 값',
  `variance` double DEFAULT '0' COMMENT '분산',
  UNIQUE KEY `experimentID` (`experimentID`,`name`),
  KEY `EXPROut_NAME_IDX` (`name`),
  CONSTRAINT `EXPROUT_REFS_SIMULID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 출력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExperimentProp`
--

DROP TABLE IF EXISTS `ExperimentProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentProp` (
  `experimentID` bigint(20) NOT NULL COMMENT '실험결과 ID',
  `propName` varchar(64) NOT NULL COMMENT '실험결과 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '실험결과 속성 값',
  UNIQUE KEY `experimentID` (`experimentID`,`propName`),
  KEY `EXPRProp_PName_IDX` (`propName`),
  KEY `EXPRProp_PValue_IDX` (`propValue`),
  CONSTRAINT `ERP_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 속성 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExperimentReferences`
--

DROP TABLE IF EXISTS `ExperimentReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExperimentReferences` (
  `experimentID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `experimentID` (`experimentID`,`articleID`),
  KEY `ERR_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `ERR_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `ERR_REFS_EXPERIMENTID` FOREIGN KEY (`experimentID`) REFERENCES `Experiment` (`experimentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 결과 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Experiment_Input`
--

DROP TABLE IF EXISTS `Experiment_Input`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Experiment_Input` (
  `experimentID` bigint(20) NOT NULL COMMENT 'experimentID`',
  `no` varchar(5) NOT NULL COMMENT 'no',
  `name` varchar(15) DEFAULT NULL,
  `typeName` varchar(20) DEFAULT NULL,
  `value` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IOUnit`
--

DROP TABLE IF EXISTS `IOUnit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOUnit` (
  `unitID` bigint(20) NOT NULL COMMENT '분자 ID',
  `serial` varchar(16) NOT NULL COMMENT '고유번호',
  `name` varchar(32) NOT NULL COMMENT '분자이름',
  `unitType` bigint(20) NOT NULL COMMENT '분자유형 ID',
  `unitValue` varchar(1000) DEFAULT NULL COMMENT '분자 량 혹은 서열',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) DEFAULT 'YES',
  PRIMARY KEY (`unitID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `IOUNIT_REFS_TYPE` (`unitType`),
  KEY `IOUNIT_REFS_USERID` (`userID`),
  KEY `IOUnit_Serial_IDX` (`serial`),
  KEY `IOUnit_Name_IDX` (`name`),
  CONSTRAINT `IOUNIT_REFS_TYPE` FOREIGN KEY (`unitType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `IOUNIT_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='분자 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IOUnitReferences`
--

DROP TABLE IF EXISTS `IOUnitReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOUnitReferences` (
  `unitID` bigint(20) NOT NULL COMMENT '분자 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `unitID` (`unitID`,`articleID`),
  KEY `IOUNITREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `IOUNITREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `IOUNITREFERENCES_REFS_IOUNITID` FOREIGN KEY (`unitID`) REFERENCES `IOUnit` (`unitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='분자의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Link`
--

DROP TABLE IF EXISTS `Link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Link` (
  `linkID` bigint(20) NOT NULL COMMENT '링크 ID',
  `experimentID` bigint(20) NOT NULL COMMENT 'experimentID`',
  `loginID` bigint(20) NOT NULL COMMENT 'link를 수행한 ID',
  PRIMARY KEY (`linkID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Module`
--

DROP TABLE IF EXISTS `Module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Module` (
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `serial` varchar(16) NOT NULL COMMENT '고유번호',
  `name` varchar(64) NOT NULL COMMENT '이름이 없는 경우 고유번호 사용',
  `moduleType` bigint(20) NOT NULL COMMENT '모듈 유형 ID',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`moduleID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `MODULE_REFS_TYPE` (`moduleType`),
  KEY `MODULE_REFS_USERID` (`userID`),
  KEY `Module_Serial_IDX` (`serial`),
  KEY `Module_Name_IDX` (`name`),
  CONSTRAINT `MODULE_REFS_TYPE` FOREIGN KEY (`moduleType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `MODULE_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module(=Unit) 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ModuleDesigner`
--

DROP TABLE IF EXISTS `ModuleDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleDesigner` (
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `userID` bigint(20) NOT NULL COMMENT '모듈개발자 ID',
  UNIQUE KEY `moduleID` (`moduleID`,`userID`),
  KEY `MODULEDESIGNER_REFS_USERID` (`userID`),
  CONSTRAINT `MODULEDESIGNER_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`),
  CONSTRAINT `MODULEDESIGNER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ModuleIOUnit`
--

DROP TABLE IF EXISTS `ModuleIOUnit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleIOUnit` (
  `moduleUnitID` bigint(20) NOT NULL COMMENT '모듈의 input/output IOUnit ID',
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `unitID` bigint(20) NOT NULL COMMENT 'IOUnit ID',
  `modulePartID` bigint(20) NOT NULL COMMENT 'IOUnit 의 대상이 되는 ModulePart ID',
  `ioType` enum('Input','Output','Both') NOT NULL DEFAULT 'Both' COMMENT 'input/output 유형',
  `positionX` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 X 좌표',
  `positionY` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 Y 좌표',
  `width` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 넓이',
  `height` double NOT NULL DEFAULT '0' COMMENT '모듈 입/출력 아이콘의 높이',
  PRIMARY KEY (`moduleUnitID`),
  KEY `MODULEUNIT_REFS_MODULEID` (`moduleID`),
  KEY `MODULEUNIT_REFS_IOUNITID` (`unitID`),
  KEY `MODULEUNIT_REFS_MODULEPARTID` (`modulePartID`),
  CONSTRAINT `MODULEUNIT_REFS_IOUNITID` FOREIGN KEY (`unitID`) REFERENCES `IOUnit` (`unitID`),
  CONSTRAINT `MODULEUNIT_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`),
  CONSTRAINT `MODULEUNIT_REFS_MODULEPARTID` FOREIGN KEY (`modulePartID`) REFERENCES `ModulePart` (`modulePartID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈의 input/output 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ModulePart`
--

DROP TABLE IF EXISTS `ModulePart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModulePart` (
  `modulePartID` bigint(20) NOT NULL COMMENT '모듈의 파트 ID',
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `seq` int(11) NOT NULL DEFAULT '1' COMMENT '부품의 순서',
  PRIMARY KEY (`modulePartID`),
  UNIQUE KEY `moduleID` (`moduleID`,`partID`,`seq`),
  KEY `MP_REFS_PARTID` (`partID`),
  CONSTRAINT `MP_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`),
  CONSTRAINT `MP_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈의 파트목록 관리테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ModuleReferences`
--

DROP TABLE IF EXISTS `ModuleReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleReferences` (
  `moduleID` bigint(20) NOT NULL COMMENT '모듈 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `moduleID` (`moduleID`,`articleID`),
  KEY `MODULEREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `MODULEREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `MODULEREFERENCES_REFS_MODULEID` FOREIGN KEY (`moduleID`) REFERENCES `Module` (`moduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='모듈의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Part`
--

DROP TABLE IF EXISTS `Part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Part` (
  `partID` bigint(20) NOT NULL COMMENT '부품ID',
  `serial` varchar(16) NOT NULL COMMENT '부품 고유번호',
  `name` varchar(64) NOT NULL COMMENT '부붐 이름',
  `partType` bigint(20) NOT NULL COMMENT '부품 타입 ID',
  `sequence` text COMMENT '서열정보',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '생성자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) DEFAULT 'YES',
  `direction` varchar(10) NOT NULL DEFAULT 'FOWARD',
  PRIMARY KEY (`partID`),
  UNIQUE KEY `serial` (`serial`),
  KEY `PART_REFS_TYPE` (`partType`),
  KEY `PART_REFS_USERID` (`userID`),
  KEY `Part_Serial_IDX` (`serial`),
  KEY `Part_Name_IDX` (`name`),
  CONSTRAINT `PART_REFS_TYPE` FOREIGN KEY (`partType`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `PART_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PartDesigner`
--

DROP TABLE IF EXISTS `PartDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartDesigner` (
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `userID` bigint(20) NOT NULL COMMENT '부품개발자 ID',
  UNIQUE KEY `partID` (`partID`,`userID`),
  KEY `PARTDESIGNER_REFS_USERID` (`userID`),
  CONSTRAINT `PARTDESIGNER_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`),
  CONSTRAINT `PARTDESIGNER_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PartProp`
--

DROP TABLE IF EXISTS `PartProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartProp` (
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `propName` varchar(128) NOT NULL COMMENT '부품 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '부품 속성 값',
  UNIQUE KEY `partID` (`partID`,`propName`),
  KEY `PartProp_PName_IDX` (`propName`),
  KEY `PartProp_PValue_IDX` (`propValue`),
  CONSTRAINT `PARTPROP_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PartReferences`
--

DROP TABLE IF EXISTS `PartReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartReferences` (
  `partID` bigint(20) NOT NULL COMMENT '부품 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `partID` (`partID`,`articleID`),
  KEY `PARTREFERENCES_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `PARTREFERENCES_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `PARTREFERENCES_REFS_PARTID` FOREIGN KEY (`partID`) REFERENCES `Part` (`partID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='부품의 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Protocol`
--

DROP TABLE IF EXISTS `Protocol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Protocol` (
  `protocolID` bigint(20) NOT NULL COMMENT '실험 프로토콜 ID',
  `name` varchar(64) NOT NULL COMMENT '실험 프로토콜 이름, Device 이름으로 대체할 수 있음',
  `deviceID` bigint(20) NOT NULL COMMENT '장비 ID',
  `strainID` bigint(20) NOT NULL COMMENT 'Strain ID',
  `media` bigint(20) NOT NULL COMMENT 'Dictionary의 media ID',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '실험 프로토콜 등록 사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  `shared` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`protocolID`),
  KEY `EP_REFS_DEVICEID` (`deviceID`),
  KEY `EP_REFS_STRAINID` (`strainID`),
  KEY `EP_REFS_MEDIA` (`media`),
  KEY `EP_REFS_USERID` (`userID`),
  KEY `Protocol_Name_IDX` (`name`),
  CONSTRAINT `EP_REFS_DEVICEID` FOREIGN KEY (`deviceID`) REFERENCES `Device` (`deviceID`),
  CONSTRAINT `EP_REFS_MEDIA` FOREIGN KEY (`media`) REFERENCES `Dictionary` (`dictionaryID`),
  CONSTRAINT `EP_REFS_STRAINID` FOREIGN KEY (`strainID`) REFERENCES `Strain` (`strainID`),
  CONSTRAINT `EP_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='장비에 의한 실험프로토콜 정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProtocolDesigner`
--

DROP TABLE IF EXISTS `ProtocolDesigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProtocolDesigner` (
  `protocolID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `userID` bigint(20) NOT NULL COMMENT '프로토콜 개발자 ID',
  UNIQUE KEY `protocolID` (`protocolID`,`userID`),
  KEY `EPD_REFS_USERID` (`userID`),
  CONSTRAINT `EPD_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`),
  CONSTRAINT `EPD_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 프로토콜 등록자 또는 개발자 목록';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProtocolProp`
--

DROP TABLE IF EXISTS `ProtocolProp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProtocolProp` (
  `protocolID` bigint(20) NOT NULL COMMENT '실험결과 ID',
  `propName` varchar(64) NOT NULL COMMENT '실험결과 속성 이름',
  `propValue` varchar(128) NOT NULL COMMENT '실험결과 속성 값',
  UNIQUE KEY `protocolID` (`protocolID`,`propName`),
  KEY `ProtocolProp_PID_IDX` (`protocolID`),
  KEY `ProtocolProp_PName_IDX` (`propName`),
  KEY `ProtocolProp_PValue_IDX` (`propValue`),
  CONSTRAINT `EPP_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험결과 속성 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProtocolReferences`
--

DROP TABLE IF EXISTS `ProtocolReferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProtocolReferences` (
  `protocolID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `articleID` bigint(20) NOT NULL COMMENT '저서 ID',
  UNIQUE KEY `protocolID` (`protocolID`,`articleID`),
  KEY `EPR_REFS_ARTICLEID` (`articleID`),
  CONSTRAINT `EPR_REFS_ARTICLEID` FOREIGN KEY (`articleID`) REFERENCES `CitaterArticle` (`articleID`),
  CONSTRAINT `EPR_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='실험 프로토콜 참고논문 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SequenceID`
--

DROP TABLE IF EXISTS `SequenceID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SequenceID` (
  `idType` int(11) NOT NULL COMMENT 'ID type',
  `id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'next id seqeunce'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ID 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Simulation`
--

DROP TABLE IF EXISTS `Simulation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Simulation` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `protocolID` bigint(20) NOT NULL COMMENT '프로토콜 ID',
  `name` varchar(128) DEFAULT NULL COMMENT '실험결과 이름',
  `cultureTime` float DEFAULT '0' COMMENT '배양시간',
  `duration` float DEFAULT '0' COMMENT '확인 간격',
  `status` varchar(15) NOT NULL DEFAULT 'ready' COMMENT '상태정보 (ready, running, complete, error)',
  `startTime` varchar(15) DEFAULT NULL COMMENT '시뮬레이션 시작 시간',
  `endTime` varchar(15) DEFAULT NULL COMMENT '시뮬레이션 종료 시간',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일시',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일시',
  PRIMARY KEY (`simulationID`),
  KEY `SIMUL_REFS_PROTOCOLID` (`protocolID`),
  KEY `SIMUL_REFS_USERID` (`userID`),
  KEY `SIMUL_Name_IDX` (`name`),
  CONSTRAINT `SIMUL_REFS_PROTOCOLID` FOREIGN KEY (`protocolID`) REFERENCES `Protocol` (`protocolID`),
  CONSTRAINT `SIMUL_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SimulationInput`
--

DROP TABLE IF EXISTS `SimulationInput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimulationInput` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `componentID` bigint(20) NOT NULL COMMENT 'DeviceComponent 테이블의 입력에 해당하는 부품 ID',
  `quantity` double DEFAULT '0' COMMENT '입력 수치',
  `unit` varchar(10) DEFAULT NULL COMMENT '수치 단위',
  UNIQUE KEY `simulationID` (`simulationID`,`componentID`),
  KEY `SIMULIN_REFS_COMPID` (`componentID`),
  CONSTRAINT `SIMULIN_REFS_COMPID` FOREIGN KEY (`componentID`) REFERENCES `DeviceComponent` (`componentID`),
  CONSTRAINT `SIMULIN_REFS_SIMULID` FOREIGN KEY (`simulationID`) REFERENCES `Simulation` (`simulationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 입력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SimulationOutput`
--

DROP TABLE IF EXISTS `SimulationOutput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimulationOutput` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `name` varchar(128) NOT NULL COMMENT '출력정보 이름',
  `mean` double DEFAULT '0' COMMENT '평균 출력 값',
  `variance` double DEFAULT '0' COMMENT '분산',
  UNIQUE KEY `simulationID` (`simulationID`,`name`),
  KEY `SIMULOUT_Name_IDX` (`name`),
  CONSTRAINT `SIMULOUT_REFS_SIMULID` FOREIGN KEY (`simulationID`) REFERENCES `Simulation` (`simulationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 출력정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SimulationParameters`
--

DROP TABLE IF EXISTS `SimulationParameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimulationParameters` (
  `simulationID` bigint(20) NOT NULL COMMENT '시뮬레이션 ID',
  `parameters` varchar(140) NOT NULL COMMENT 'protocol + substrate + product 조합으로 된 파라미터 이름',
  `substrates` varchar(1000) NOT NULL COMMENT '콤마(,)로 연결된  input unit들의 이름',
  `products` varchar(1000) NOT NULL COMMENT '콤마(,)로 연결된 output unit들의 이름',
  `value` double NOT NULL DEFAULT '0' COMMENT '반응 비율 등의 반응수치',
  UNIQUE KEY `simulationID` (`simulationID`,`parameters`),
  CONSTRAINT `SIMULPARAM_REFS_SIMULID` FOREIGN KEY (`simulationID`) REFERENCES `Simulation` (`simulationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시뮬레이션 입력 파라미터 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Strain`
--

DROP TABLE IF EXISTS `Strain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Strain` (
  `strainID` bigint(20) NOT NULL COMMENT 'strain growth model ID',
  `name` varchar(64) NOT NULL COMMENT 'strain 이름',
  `meanTime` float DEFAULT '0' COMMENT '평균 배양 시간',
  `description` text COMMENT '요약설명',
  `userID` bigint(20) NOT NULL COMMENT '사용자 ID',
  `creationDate` varchar(15) NOT NULL COMMENT '생성일시',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일시',
  `shared` varchar(3) DEFAULT 'YES',
  PRIMARY KEY (`strainID`),
  KEY `SGM_REFS_USERID` (`userID`),
  KEY `Strain_Name_IDX` (`name`),
  CONSTRAINT `SGM_REFS_USERID` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Strain Growth Model 정보 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StrainAttachment`
--

DROP TABLE IF EXISTS `StrainAttachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StrainAttachment` (
  `strainID` bigint(20) NOT NULL COMMENT 'strain growth model ID',
  `attachmentID` bigint(20) NOT NULL COMMENT '첨부파일 ID',
  `type` varchar(15) NOT NULL DEFAULT 'output' COMMENT '첨부파일 종류 (결과파일, 기타...)',
  UNIQUE KEY `strainID` (`strainID`,`attachmentID`),
  KEY `SGMA_REFS_ATTACHID` (`attachmentID`),
  CONSTRAINT `SGMA_REFS_ATTACHID` FOREIGN KEY (`attachmentID`) REFERENCES `Attachment` (`attachmentID`),
  CONSTRAINT `SGMA_REFS_STRAINID` FOREIGN KEY (`strainID`) REFERENCES `Strain` (`strainID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Strain Growth Model 첨부파일 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `userID` bigint(20) NOT NULL COMMENT '사용자 ID',
  `username` varchar(32) NOT NULL COMMENT '사용자 이름',
  `name` varchar(32) DEFAULT NULL COMMENT '사용자 이름',
  `passwordHash` varchar(32) NOT NULL COMMENT '사용자 비밀번호',
  `email` varchar(64) DEFAULT NULL COMMENT '전자우편 주소',
  `affiliation` varchar(256) DEFAULT NULL COMMENT '소속정보',
  `creationDate` varchar(15) NOT NULL COMMENT '등록일자',
  `modificationDate` varchar(15) NOT NULL COMMENT '변경일자',
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`),
  KEY `User_Username_IDX` (`email`),
  KEY `User_Name_IDX` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='사용자 관리 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Victor_Condition`
--

DROP TABLE IF EXISTS `Victor_Condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Victor_Condition` (
  `experimentID` bigint(20) NOT NULL COMMENT 'experimentID`',
  `colName` varchar(5) NOT NULL COMMENT 'column Name',
  `condition1` varchar(15) DEFAULT NULL COMMENT 'result 01 value ',
  `inputName1` varchar(15) DEFAULT NULL,
  `condition2` varchar(15) DEFAULT NULL COMMENT 'result 02 value ',
  `inputName2` varchar(15) DEFAULT NULL,
  `condition3` varchar(15) DEFAULT NULL COMMENT 'result 03 value ',
  `inputName3` varchar(15) DEFAULT NULL,
  `rowName` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Victor_Values`
--

DROP TABLE IF EXISTS `Victor_Values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Victor_Values` (
  `victorID` bigint(20) NOT NULL COMMENT 'Victor ID',
  `experimentID` bigint(20) NOT NULL COMMENT 'experimentID`',
  `labelName` varchar(20) NOT NULL COMMENT 'label Name',
  `colName` varchar(5) NOT NULL COMMENT 'column Name',
  `result1` varchar(10) DEFAULT NULL,
  `result2` varchar(10) DEFAULT NULL,
  `result3` varchar(10) DEFAULT NULL,
  `result4` varchar(10) DEFAULT NULL,
  `result5` varchar(10) DEFAULT NULL,
  `result6` varchar(10) DEFAULT NULL,
  `result7` varchar(10) DEFAULT NULL,
  `result8` varchar(10) DEFAULT NULL,
  `result9` varchar(10) DEFAULT NULL,
  `result10` varchar(10) DEFAULT NULL,
  `result11` varchar(10) DEFAULT NULL,
  `result12` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`victorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-31 11:55:35
