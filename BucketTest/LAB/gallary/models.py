from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Gallary(models.Model):
	Title = models.CharField(max_length=50, null=False)
	Gallarydate = models.DateTimeField()
	Path = models.CharField(max_length=100, null=False)

