from django.shortcuts import render
from .models import Paper
# Create your views here.
def index(request):
	paperList = Paper.objects.all()
	print paperList[0]
	yearList = []
	for index in range(0,len(paperList)):
		yearList.append(str(paperList[index].Pu_year))
	print 'paperList = ' , len(paperList)
	yearList = sorted(list(set(yearList)),reverse=True)
	htmlstr = 'test'
	context = {'htmlstr':htmlstr,'paperList':paperList,'yearList':yearList}
	return render(request,'publication/index.html',context)


def year(request,year):
	paperList = Paper.objects.all()
	print 'year / paperList = ' , len(paperList)
	htmlstr = 'test'
	context = {'htmlstr':htmlstr,'paperList':paperList,'year':year}
	return render(request,'publication/pu_year.html',context)

