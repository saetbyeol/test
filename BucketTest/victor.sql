CREATE TABLE Victor (
  victorID bigint(20) not null primary key,
  experimentID bigint(20) NOT NULL,
  labelName varchar(20) NOT NULL,
  colName varchar(5) NOT NULL ,
  rowName varchar(5) NOT NULL,
  condition1 varchar(15) DEFAULT NULL,
  inputName1 varchar(15) DEFAULT NULL,
  condition2 varchar(15) DEFAULT NULL,
  inputName2 varchar(15) DEFAULT NULL,
  condition3 varchar(15) DEFAULT NULL,
  inputName3 varchar(15) DEFAULT NULL,
  result varchar(10) NULL
  
) ;
