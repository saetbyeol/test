# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-01 05:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publication', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paper',
            name='Doi',
            field=models.CharField(max_length=120),
        ),
    ]
