from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Paper(models.Model):
	Paper_title=models.CharField(max_length=220, null=False)
	Journal=models.CharField(max_length=40, null=False)
	First_author=models.CharField(max_length=15, null=False)
	Cor_author=models.CharField(max_length=150, null=False)
	Co_author=models.CharField(max_length=50, null=False)
	link = models.CharField(max_length = 230, null=False)
	Species=models.CharField(max_length = 10, null=False)
	Pu_year=models.CharField(max_length = 4, null=False)
	Doi=models.CharField(max_length = 120, null=False)

